/*
SQLyog Ultimate v11.11 (64 bit)
MySQL - 5.7.21 : Database - relay-job
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`relay-job` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `relay-job`;

/*Table structure for table `sys_dict` */

DROP TABLE IF EXISTS `sys_dict`;

CREATE TABLE `sys_dict` (
  `id` varchar(50) NOT NULL COMMENT '主键ID',
  `title` varchar(255) DEFAULT NULL COMMENT '字典名称',
  `name` varchar(255) DEFAULT NULL COMMENT '字典键名',
  `type` tinyint(4) DEFAULT NULL COMMENT '字典类型',
  `value` text COMMENT '字典键值',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注',
  `CREATE_TIME` datetime DEFAULT NULL COMMENT '创建时间',
  `CREATE_BY` varchar(50) DEFAULT NULL COMMENT '创建人',
  `UPDATE_TIME` datetime DEFAULT NULL COMMENT '更新时间',
  `UPDATE_BY` varchar(50) DEFAULT NULL COMMENT '更新人',
  `DEL_FLAG` varchar(1) DEFAULT NULL COMMENT '状态, 0-有效, 1-无效',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `sys_dict` */

insert  into `sys_dict`(`id`,`title`,`name`,`type`,`value`,`remark`,`CREATE_TIME`,`CREATE_BY`,`UPDATE_TIME`,`UPDATE_BY`,`DEL_FLAG`) values ('1','数据状态','DATA_STATUS',2,'1:正常,2:冻结,3:删除','','2018-10-05 16:03:11','1','2018-10-05 16:11:41','1','0'),('2','字典类型','DICT_TYPE',2,'2:键值对','','2018-10-05 20:08:55','1','2019-01-17 23:39:23','1','0'),('3','用户性别','USER_SEX',2,'1:男,2:女','','2018-10-05 20:12:32','1','2018-10-05 20:12:32','1','0'),('347df243b0b94508827f93ee29c5c4f5','任务执行状态','EXECUTE_STATUS',2,'0000:成功,1001:未找到执行器,1002:执行中,1003:任务执行错误,2003:任务未找到,2004:部分成功,2005:执行超时,2006:人工取消,3001:任务分片错误,4001:不支持任务结果收集处理,4002:任务结果收集处理错误,9999:任务执行异常','','2019-07-27 09:22:44','1','2019-07-27 09:22:44','1','0'),('3a6753897e5c4f25ba2863d9a253fead','集群任务处理状态','COLLECT_STATUS',2,'SUCCESS:处理成功,FAILED:处理失败','','2019-08-10 21:22:46','1','2019-08-10 21:22:46','1','0'),('4','菜单类型','MENU_TYPE',2,'1:一级菜单,2:子级菜单,3:不是菜单','','2018-10-05 20:24:57','1','2018-10-13 13:56:05','1','0'),('5','搜索栏状态','SEARCH_STATUS',2,'1:正常,2:冻结','','2018-10-05 20:25:45','1','2019-02-26 00:34:34','1','0'),('6','日志类型','LOG_TYPE',2,'1:业务,2:登录,3:系统','','2018-10-05 20:28:47','1','2019-02-26 00:31:43','1','0'),('6edd503a1a92460cb30e04ba569997bd','分区执行状态','PARTITION_STATUS',2,'EXECUTING:执行中,SUCCESS:执行成功,FAILED:执行失败','','2019-08-10 21:21:40','1','2019-08-10 21:21:40','1','0'),('7','应用状态','APP_STATUS',2,'NORMAL:正常,PAUSE:暂停',NULL,'2019-06-26 23:08:57','1','2019-06-26 23:09:01','1','0'),('8','授权类型','AUTH_TYPE',2,'TOKEN:token,IP:指定ip,NONE:无需认证',NULL,'2019-06-26 23:08:52','1','2019-06-26 23:09:04','1','0'),('9','任务状态','JOB_STATUS',2,'NORMAL:正常,PAUSE:暂停,ABANDONED:废弃',NULL,'2019-06-26 23:53:42','1','2019-06-26 23:53:47','1','0'),('90314979daa44b9da8a7e4674fe3a867','网络协议','NET_PROTOCOL',2,'HTTP:HTTP','','2019-07-01 22:55:27','20f770ad4dad41b4b8792eb65a3de9a0','2019-07-01 22:55:27','20f770ad4dad41b4b8792eb65a3de9a0','0'),('eb5265bf088a42949e53212a7a5cc6d6','任务触发结果','TRIGGER_RESULT',2,'T-0001:任务触发中,T-0000:触发成功,T-1000:触发请求被拦截,T-2000:任务结果处理异常,T-9999:任务触发异常','','2020-05-09 15:28:07','1','2020-05-09 15:28:07','1','0');

/*Table structure for table `sys_menu` */

DROP TABLE IF EXISTS `sys_menu`;

CREATE TABLE `sys_menu` (
  `ID` varchar(32) NOT NULL COMMENT '主键ID',
  `title` varchar(255) DEFAULT NULL COMMENT '菜单名称',
  `pid` varchar(32) DEFAULT NULL COMMENT '父级编号',
  `pids` varchar(255) DEFAULT NULL COMMENT '所有父级编号',
  `url` varchar(255) DEFAULT NULL COMMENT 'URL地址',
  `icon` varchar(255) DEFAULT NULL COMMENT '图标',
  `type` tinyint(4) DEFAULT NULL COMMENT '类型（1:一级菜单,2:子级菜单,3:不是菜单）',
  `sort` int(11) DEFAULT NULL COMMENT '排序',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注',
  `CREATE_TIME` datetime DEFAULT NULL COMMENT '创建时间',
  `CREATE_BY` varchar(50) DEFAULT NULL COMMENT '创建人',
  `UPDATE_TIME` datetime DEFAULT NULL COMMENT '更新时间',
  `UPDATE_BY` varchar(50) DEFAULT NULL COMMENT '更新人',
  `DEL_FLAG` varchar(1) DEFAULT NULL COMMENT '状态, 0-有效, 1-无效',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `sys_menu` */

insert  into `sys_menu`(`ID`,`title`,`pid`,`pids`,`url`,`icon`,`type`,`sort`,`remark`,`CREATE_TIME`,`CREATE_BY`,`UPDATE_TIME`,`UPDATE_BY`,`DEL_FLAG`) values ('093b8e49256c429fb7274ff89d3d80e3','查询触发日志','135','[0],[30],[135]','/admin/quartz/job/trigger','',3,1,'','2019-06-29 07:51:38',NULL,'2019-06-29 07:51:38',NULL,'0'),('0b45324d63bc417da2c5aafa497091bb','执行日志','30','[0],[30]','/admin/quartz/job/execute','',2,4,'','2019-07-03 20:59:54','20f770ad4dad41b4b8792eb65a3de9a0','2019-07-03 20:59:54','20f770ad4dad41b4b8792eb65a3de9a0','0'),('1','菜单管理','2','[0],[2]','/system/menu/index','',2,3,'','2019-06-29 20:35:03','1','2019-06-29 20:35:03','1','0'),('10','修改状态','1','[0],[2],[1]','/system/menu/status','',3,4,'','2019-06-29 20:35:03','1','2019-06-29 20:35:03','1','0'),('125','任务管理','30','[0],[30]','/admin/quartz/job','',2,2,'','2019-06-29 20:35:03','1','2019-07-03 20:59:54','20f770ad4dad41b4b8792eb65a3de9a0','0'),('135','触发日志','30','[0],[30]','/admin/quartz/job/trigger','',2,3,'','2019-07-03 20:59:13','1','2019-07-03 20:59:54','20f770ad4dad41b4b8792eb65a3de9a0','0'),('15','编辑','7','[0],[2],[7]','/system/user/edit','',3,2,'','2019-06-29 20:35:03','1','2019-06-29 20:35:03','1','0'),('16','添加','7','[0],[2],[7]','/system/user/add','',3,1,'','2019-06-29 20:35:03','1','2019-06-29 20:35:03','1','0'),('17','修改密码','7','[0],[2],[7]','/system/user/pwd','',3,3,'','2019-06-29 20:35:03','1','2019-06-29 20:35:03','1','0'),('19','详细','7','[0],[2],[7]','/system/user/detail','',3,5,'','2019-06-29 20:35:03','1','2019-06-29 20:35:03','1','0'),('2','系统管理','0','[0]','#','fa fa-cog',1,2,'','2019-06-29 20:35:03','1','2019-06-29 20:35:03','1','0'),('20','修改状态','7','[0],[2],[7]','/system/user/status','',3,6,'','2019-06-29 20:35:03','1','2019-06-29 20:35:03','1','0'),('21','字典管理','2','[0],[2]','/system/dict/index','',2,5,'','2019-06-29 20:35:03','1','2019-06-29 20:35:03','1','0'),('22','字典添加','21','[0],[2],[21]','/system/dict/add','',3,1,'','2019-06-29 20:35:03','1','2019-06-29 20:35:03','1','0'),('23','字典编辑','21','[0],[2],[21]','/system/dict/edit','',3,2,'','2019-06-29 20:35:03','1','2019-06-29 20:35:03','1','0'),('24','字典详细','21','[0],[2],[21]','/system/dict/detail','',3,3,'','2019-06-29 20:35:03','1','2019-06-29 20:35:03','1','0'),('25','修改状态','21','[0],[2],[21]','/system/dict/status','',3,4,'','2019-06-29 20:35:03','1','2019-06-29 20:35:03','1','0'),('3','添加','1','[0],[2],[1]','/system/menu/add','',3,1,'','2019-06-29 20:35:03','1','2019-06-29 20:35:03','1','0'),('30','定时任务管理','0','[0]','#','fa fa-clock-o',1,3,'','2019-06-29 20:35:03','1','2019-06-29 20:35:03','1','0'),('31','应用管理','30','[0],[30]','/admin/quartz/job/app','',2,1,'','2019-06-29 20:35:03','1','2019-07-03 20:59:54','20f770ad4dad41b4b8792eb65a3de9a0','0'),('40','API接口','0','[0]','#','fa fa-laptop',1,4,NULL,'2019-07-03 20:44:43','1','2019-07-03 20:44:47','1','0'),('5fa9df9b14804440b1bc7c21a020687f','添加任务','125','[0],[30],[125]','/admin/quartz/job/add','',3,1,'','2019-06-29 07:50:44',NULL,'2019-06-29 07:50:44',NULL,'0'),('6','主页','0','[0]','/index','layui-icon layui-icon-home',1,1,'','2019-06-29 20:35:03','1','2019-06-29 20:35:03','1','0'),('61594e4b034f455eb9d0c4d2acce5114','添加应用','31','[0],[30],[31]','/admin/quartz/job/app/save','',3,1,'','2019-06-29 07:49:58',NULL,'2019-06-29 07:49:58',NULL,'0'),('7','用户管理','2','[0],[2]','/system/user/index','',2,1,'','2019-06-29 20:35:03','1','2019-06-29 20:35:03','1','0'),('8','编辑','1','[0],[2],[1]','/system/menu/edit','',3,2,'','2019-06-29 20:35:03','1','2019-06-29 20:35:03','1','0'),('9','详细','1','[0],[2],[1]','/system/menu/detail','',3,3,'','2019-06-29 20:35:03','1','2019-06-29 20:35:03','1','0'),('b565c211d02a44df94070884e1842d07','详情','0b45324d63bc417da2c5aafa497091bb','[0],[30],[0b45324d63bc417da2c5aafa497091bb]','/admin/quartz/job/execute/info','',3,1,'','2019-07-03 21:06:03','20f770ad4dad41b4b8792eb65a3de9a0','2019-07-03 21:06:03','20f770ad4dad41b4b8792eb65a3de9a0','0'),('c44b81234d5f4c09a126340d399a3653','swagger接口','40','[0],[40]','/swagger-ui.html','',2,1,'','2019-07-03 07:45:43','20f770ad4dad41b4b8792eb65a3de9a0','2019-07-03 07:45:43','20f770ad4dad41b4b8792eb65a3de9a0','0');

/*Table structure for table `sys_user` */

DROP TABLE IF EXISTS `sys_user`;

CREATE TABLE `sys_user` (
  `ID` varchar(32) NOT NULL COMMENT '主键',
  `username` varchar(30) DEFAULT NULL COMMENT '用户名',
  `password` varchar(50) DEFAULT NULL COMMENT '密码',
  `salt` varchar(50) DEFAULT NULL COMMENT '密码盐',
  `email` varchar(30) DEFAULT NULL COMMENT '邮箱',
  `phone` varchar(20) DEFAULT NULL COMMENT '电话号码',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注',
  `CREATE_TIME` datetime DEFAULT NULL COMMENT '创建时间',
  `CREATE_BY` varchar(50) DEFAULT NULL COMMENT '创建人',
  `UPDATE_TIME` datetime DEFAULT NULL COMMENT '更新时间',
  `UPDATE_BY` varchar(50) DEFAULT NULL COMMENT '更新人',
  `DEL_FLAG` varchar(1) DEFAULT NULL COMMENT '状态, 0-有效, 1-无效',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `sys_user` */

insert  into `sys_user`(`ID`,`username`,`password`,`salt`,`email`,`phone`,`remark`,`CREATE_TIME`,`CREATE_BY`,`UPDATE_TIME`,`UPDATE_BY`,`DEL_FLAG`) values ('1','admin','2edf86256067ed46886d77d9aed82a','a3713257974949b','1137661952@qq.com','18200000000','超级管理员','2019-06-26 16:10:48',NULL,'2019-06-29 07:26:47',NULL,'0'),('12e27e89874d4d9cb995cf61c77466c8','test','fdf88610dff2028e978501e3edbb7d','865779ad7f704b4','','111','','2019-09-05 10:47:31','1','2019-09-05 10:53:03','1','1'),('20f770ad4dad41b4b8792eb65a3de9a0','zhouwd','477aa1910857273ea53d6703ce6abe','74eb2fce9faf449','1137661952@qq.com','18200316344','路漫漫其修远兮','2019-09-05 10:25:20','1','2019-09-05 10:25:41','1','0');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

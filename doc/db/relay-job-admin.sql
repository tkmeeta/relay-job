/*
SQLyog Ultimate v11.11 (64 bit)
MySQL - 5.7.21 : Database - relay-job
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`relay-job` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `relay-job`;

/*Table structure for table `relay_job_apps` */

DROP TABLE IF EXISTS `relay_job_apps`;

CREATE TABLE `relay_job_apps` (
  `ID` varchar(32) NOT NULL COMMENT '主键',
  `NAME` varchar(50) DEFAULT NULL COMMENT '名称',
  `APP_NAME` varchar(200) NOT NULL COMMENT '应用名称',
  `DESCRIPTION` varchar(300) DEFAULT NULL COMMENT '描述',
  `AUTH_TYPE` varchar(5) NOT NULL DEFAULT '1' COMMENT '认证方式, token-token认证, ip-指定IP认证',
  `AUTH_IPS` varchar(500) DEFAULT NULL COMMENT '授权IP列表, 使用英文逗号分隔',
  `STATUS` varchar(10) NOT NULL COMMENT '状态',
  `CREATE_TIME` datetime DEFAULT NULL COMMENT '创建时间',
  `CREATE_BY` varchar(50) DEFAULT NULL COMMENT '创建人',
  `UPDATE_TIME` datetime DEFAULT NULL COMMENT '更新时间',
  `UPDATE_BY` varchar(50) DEFAULT NULL COMMENT '更新人',
  `DEL_FLAG` varchar(1) DEFAULT NULL COMMENT '状态, 0-有效, 1-无效',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `relay_job_collect_log` */

DROP TABLE IF EXISTS `relay_job_collect_log`;

CREATE TABLE `relay_job_collect_log` (
  `ID` varchar(32) NOT NULL COMMENT '主键',
  `TRIGGER_LOG_ID` varchar(32) NOT NULL COMMENT '触发任务的ID',
  `ENDPOINT_IP` varchar(20) DEFAULT NULL COMMENT '执行端点IP',
  `ENDPOINT_PORT` int(6) DEFAULT NULL COMMENT '执行端点端口',
  `NET_PROTOCOL` varchar(20) DEFAULT NULL COMMENT '网络协议',
  `AUTH_TOKEN` varchar(50) DEFAULT NULL COMMENT '授权token',
  `COLLECT_STATUS` varchar(10) NOT NULL COMMENT '任务执行状态',
  `COLLECT_PARAM` longtext COMMENT '执行参数',
  `RESULT` longtext COMMENT '执行结果',
  `ERROR_MESSAGE` longtext COMMENT '执行的错误信息',
  `BEGIN_TIME` datetime DEFAULT NULL COMMENT '执行起始时间',
  `END_TIME` datetime DEFAULT NULL COMMENT '执行结束时间',
  `CREATE_TIME` datetime DEFAULT NULL,
  `CREATE_BY` varchar(50) DEFAULT NULL,
  `UPDATE_TIME` datetime DEFAULT NULL,
  `UPDATE_BY` varchar(50) DEFAULT NULL,
  `DEL_FLAG` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `relay_job_details` */

DROP TABLE IF EXISTS `relay_job_details`;

CREATE TABLE `relay_job_details` (
  `ID` varchar(32) NOT NULL COMMENT '主键',
  `APP_ID` varchar(200) NOT NULL COMMENT '所属应用',
  `JOB_NAME` varchar(50) DEFAULT NULL COMMENT '任务名称',
  `HANDLER_NAME` varchar(200) NOT NULL COMMENT '任务处理器名称',
  `TRIGGER_CLASS_NAME` varchar(250) NOT NULL COMMENT '任务触发器类名',
  `CRON_EXPRESSION` varchar(20) DEFAULT NULL COMMENT 'cron表达式',
  `EXECUTE_MODEL` varchar(50) NOT NULL COMMENT '执行模式',
  `ROUTE_STRATEGY` varchar(250) NOT NULL COMMENT '执行路由策略',
  `BLOCK_STRATEGY` varchar(10) DEFAULT NULL COMMENT '阻塞策略',
  `DESCRIPTION` varchar(250) DEFAULT NULL COMMENT '描述',
  `TRIGGER_PARAM` longtext COMMENT '执行参数',
  `EXECUTE_TIMEOUT` int(11) DEFAULT NULL COMMENT '任务执行超时时间, 单位:秒',
  `FAIL_RETRY_TIMES` int(2) DEFAULT NULL COMMENT '失败重试次数',
  `STATUS` varchar(10) DEFAULT NULL COMMENT '状态',
  `CREATE_TIME` datetime DEFAULT NULL COMMENT '创建时间',
  `CREATE_BY` varchar(50) DEFAULT NULL COMMENT '创建人',
  `UPDATE_TIME` datetime DEFAULT NULL COMMENT '更新时间',
  `UPDATE_BY` varchar(50) DEFAULT NULL COMMENT '更新人',
  `DEL_FLAG` varchar(1) DEFAULT NULL COMMENT '状态, 0-有效, 1-无效',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `relay_job_execute_log` */

DROP TABLE IF EXISTS `relay_job_execute_log`;

CREATE TABLE `relay_job_execute_log` (
  `ID` varchar(32) NOT NULL COMMENT '主键',
  `TRIGGER_LOG_ID` varchar(32) NOT NULL COMMENT '触发任务的ID',
  `ENDPOINT_IP` varchar(20) DEFAULT NULL COMMENT '执行端点IP',
  `ENDPOINT_PORT` int(6) DEFAULT NULL COMMENT '执行端点端口',
  `NET_PROTOCOL` varchar(20) DEFAULT NULL COMMENT '网络协议',
  `AUTH_TOKEN` varchar(50) DEFAULT NULL COMMENT '授权token',
  `EXECUTE_STATUS` varchar(10) NOT NULL COMMENT '任务执行状态',
  `EXECUTE_PARAM` longtext COMMENT '执行参数',
  `EXECUTE_RESULT` longtext COMMENT '执行结果',
  `EXECUTE_ERROR_MESSAGE` longtext COMMENT '执行的错误信息',
  `RETRY_TIMES` int(11) DEFAULT NULL COMMENT '重试次数',
  `BEGIN_TIME` datetime DEFAULT NULL COMMENT '执行起始时间',
  `END_TIME` datetime DEFAULT NULL COMMENT '执行结束时间',
  `CREATE_TIME` datetime DEFAULT NULL,
  `CREATE_BY` varchar(50) DEFAULT NULL,
  `UPDATE_TIME` datetime DEFAULT NULL,
  `UPDATE_BY` varchar(50) DEFAULT NULL,
  `DEL_FLAG` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `relay_job_partition_log` */

DROP TABLE IF EXISTS `relay_job_partition_log`;

CREATE TABLE `relay_job_partition_log` (
  `ID` varchar(32) NOT NULL COMMENT '主键',
  `TRIGGER_LOG_ID` varchar(32) DEFAULT NULL COMMENT '任务触发日志id',
  `ENDPOINT_IP` varchar(20) DEFAULT NULL COMMENT '执行端点IP',
  `ENDPOINT_PORT` int(6) DEFAULT NULL COMMENT '执行端点端口',
  `NET_PROTOCOL` varchar(20) DEFAULT NULL COMMENT '网络协议',
  `AUTH_TOKEN` varchar(50) DEFAULT NULL COMMENT '授权token',
  `STATUS` varchar(10) DEFAULT NULL COMMENT '状态',
  `ENDPOINT_LIST` longtext COMMENT '可用端点列表',
  `PARTITION_PARAM` longtext COMMENT '分片参数',
  `PARTITION_RESULT` longtext COMMENT '分片结果',
  `ERROR_MESSAGE` longtext COMMENT '错误信息',
  `BEGIN_TIME` datetime DEFAULT NULL COMMENT '执行起始时间',
  `END_TIME` datetime DEFAULT NULL COMMENT '执行结束时间',
  `CREATE_TIME` datetime DEFAULT NULL,
  `CREATE_BY` varchar(50) DEFAULT NULL,
  `UPDATE_TIME` datetime DEFAULT NULL,
  `UPDATE_BY` varchar(50) DEFAULT NULL,
  `DEL_FLAG` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `relay_job_relation` */

DROP TABLE IF EXISTS `relay_job_relation`;

CREATE TABLE `relay_job_relation` (
  `PARENT_JOB_ID` varchar(50) DEFAULT NULL COMMENT '父任务Id',
  `CHILD_JOB_ID` varchar(50) DEFAULT NULL COMMENT '子任务Id'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `relay_job_trigger_block_queue` */

DROP TABLE IF EXISTS `relay_job_trigger_block_queue`;

CREATE TABLE `relay_job_trigger_block_queue` (
  `ID` varchar(32) NOT NULL,
  `TRIGGER_ID` varchar(32) NOT NULL COMMENT '触发id',
  `STATUS` varchar(10) NOT NULL COMMENT '阻塞状态',
  `PRIORITY` int(3) DEFAULT '0' COMMENT '执行优先级',
  `SEAT` int(10) DEFAULT NULL COMMENT '执行队列排序号',
  `BLOCK_KEY` longtext COMMENT '阻塞KEY',
  `CREATE_TIME` datetime DEFAULT NULL COMMENT '创建时间',
  `CREATE_BY` varchar(50) DEFAULT NULL COMMENT '创建人',
  `UPDATE_TIME` datetime DEFAULT NULL COMMENT '更新时间',
  `UPDATE_BY` varchar(50) DEFAULT NULL COMMENT '更新人',
  `DEL_FLAG` varchar(1) DEFAULT NULL COMMENT '状态, 0-有效, 1-无效',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `relay_job_trigger_log` */

DROP TABLE IF EXISTS `relay_job_trigger_log`;

CREATE TABLE `relay_job_trigger_log` (
  `ID` varchar(32) NOT NULL COMMENT '主键',
  `EXECUTE_LOG_ID` varchar(32) DEFAULT NULL COMMENT '触发该任务的任务Id',
  `JOB_ID` varchar(32) NOT NULL COMMENT '触发的任务Id',
  `TRIGGER_TIME` datetime DEFAULT NULL COMMENT '触发时间',
  `TRIGGER_PARAM` longtext COMMENT '触发任务执行参数',
  `TRIGGER_RESULT_CODE` varchar(10) DEFAULT NULL COMMENT '触发任务结果',
  `TRIGGER_RESULT_MESSAGE` longtext COMMENT '触发任务返回结果信息',
  `TRIGGER_END_TIME` datetime DEFAULT NULL COMMENT '触发结束时间',
  `CREATE_TIME` datetime DEFAULT NULL,
  `CREATE_BY` varchar(50) DEFAULT NULL,
  `UPDATE_TIME` datetime DEFAULT NULL,
  `UPDATE_BY` varchar(50) DEFAULT NULL,
  `DEL_FLAG` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

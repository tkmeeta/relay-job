package com.yxsk.relay.job.admin.web.common.config;

import com.yxsk.relay.job.admin.web.common.security.LoginCheckInterceptor;
import com.yxsk.relay.job.component.common.constant.UriConstant;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * @Author 11376
 * @CreaTime 2019/6/25 17:54
 * @Description
 */
@Configuration
public class AdminWebConfig implements WebMvcConfigurer {

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/**").addResourceLocations("classpath:/static/");
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(loginCheckInterceptor());
    }

    @Bean
    public LoginCheckInterceptor loginCheckInterceptor() {
        return new LoginCheckInterceptor("/css/**",
                "/images/**",
                "/js/**",
                "/lib/**",
                "/favicon.ico",
                "/login/**",
                "/logout/**",
                UriConstant.RENEWAL_URI,
                UriConstant.TERMINATE_URI,
                UriConstant.TASK_ASYNC_EXECUTE_RESULT_CALLBACK_URI);
    }

}

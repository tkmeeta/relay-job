package com.yxsk.relay.job.admin.web.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.yxsk.relay.job.admin.data.entity.BaseEntity;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.util.HashMap;
import java.util.Map;

/**
 * @author 小懒虫
 * @date 2018/8/14
 */
@Getter
@Setter
@Entity
@Table(name = "sys_menu")
public class Menu extends BaseEntity {
    @Id
    private String id;
    private String pid;
    private String pids;
    private String title;
    private String url;
    private String icon;
    private Byte type;
    private Integer sort;
    private String remark;

    @Transient
    @JsonIgnore
    private Map<String, Menu> children = new HashMap<>();

}

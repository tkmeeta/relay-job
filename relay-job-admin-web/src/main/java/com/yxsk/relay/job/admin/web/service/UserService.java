package com.yxsk.relay.job.admin.web.service;

import com.yxsk.relay.job.admin.web.entity.User;
import com.yxsk.relay.job.admin.web.repository.UserRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.util.List;

/**
 * @Author 11376
 * @CreaTime 2019/6/25 16:56
 * @Description
 */
@Service
@Transactional
public class UserService extends AdminAbstractService<User, UserRepository> {

    @Transactional(propagation = Propagation.SUPPORTS)
    public User findByUsername(String username) {
        List<User> user = this.repository.findByUsername(username);
        return CollectionUtils.isEmpty(user) ? null : user.get(0);
    }

    public Boolean repeatByUsername(User user) {
        return !CollectionUtils.isEmpty(repository.findByUsernameAndIdNot(user.getUsername(), user.getId() != null ? user.getId() : ""));
    }

}

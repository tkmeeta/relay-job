package com.yxsk.relay.job.admin.web.common.aop;

import com.yxsk.relay.job.admin.web.common.exception.RelayAdminWebRestfulRuntimeException;
import com.yxsk.relay.job.admin.web.common.exception.RelayAdminWebRuntimeException;
import com.yxsk.relay.job.component.common.protocol.message.base.ResultResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @Author 11376
 * @CreaTime 2019/8/1 10:52
 * @Description
 */
@Slf4j
@ControllerAdvice
public class AdminWebExceptionHandler {

    @ExceptionHandler(RelayAdminWebRestfulRuntimeException.class)
    @ResponseBody
    public ResultResponse handleRestful(RelayAdminWebRestfulRuntimeException e) {
        log.error("服务异常", e);
        return ResultResponse.fail(e.getResponse().getCode(), e.getResponse().getMessage());
    }

    @ExceptionHandler(RelayAdminWebRuntimeException.class)
    public String forward(RelayAdminWebRuntimeException e, Model model) {
        log.error("服务请求异常", e);

        model.addAttribute("statusCode", e.getResponse().getCode());
        model.addAttribute("msg", e.getMessage());

        return "/system/error/error";
    }

}

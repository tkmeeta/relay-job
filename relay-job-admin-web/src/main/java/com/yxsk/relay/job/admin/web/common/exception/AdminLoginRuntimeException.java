package com.yxsk.relay.job.admin.web.common.exception;

import com.yxsk.relay.job.admin.web.common.enums.ResultEnum;

/**
 * @Author 11376
 * @CreaTime 2019/6/25 16:14
 * @Description
 */
public class AdminLoginRuntimeException extends RelayAdminWebRuntimeException {

    public AdminLoginRuntimeException(ResultEnum resultEnum) {
        super(resultEnum);
    }

    public AdminLoginRuntimeException(String cause) {
        super(cause);
    }

    public AdminLoginRuntimeException(Throwable e) {
        super(e);
    }

    public AdminLoginRuntimeException(String cause, Throwable e) {
        super(cause, e);
    }
}

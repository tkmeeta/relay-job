package com.yxsk.relay.job.admin.web.service;

import com.yxsk.relay.job.admin.data.entity.BaseEntity;
import com.yxsk.relay.job.admin.data.service.AbstractService;
import com.yxsk.relay.job.admin.web.common.data.PageSort;
import com.yxsk.relay.job.admin.web.entity.Dict;
import com.yxsk.relay.job.admin.web.repository.DictRepository;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.util.List;

/**
 * @author 小懒虫
 * @date 2018/8/14
 */
@Service
public class DictService extends AbstractService<Dict, DictRepository> {

    /**
     * 根据字典标识获取字典数据
     *
     * @param name 字典标识
     */
    public Dict getByNameOk(String name) {
        return repository.findByNameAndDelFlag(name, BaseEntity.DelFlagEnum.NORMAL.getFlag());
    }

    /**
     * 获取分页列表数据
     *
     * @param example 查询实例
     * @return 返回分页数据
     */
    public Page<Dict> getPageList(Example<Dict> example) {
        // 创建分页对象
        PageRequest page = PageSort.pageRequest();
        return repository.findAll(example, page);
    }

    /**
     * 字典标识是否重复
     *
     * @param dict 字典实体类
     */
    public boolean repeatByName(Dict dict) {
        Dict dict1 = repository.findByName(dict.getName());
        return dict1 != null && !dict1.getId().equals(dict.getId());
    }

    /**
     * 状态(启用，冻结，删除)/批量状态处理
     */
    @Transactional
    public void delete(List<String> idList) {
        if (!CollectionUtils.isEmpty(idList)) {
            idList.stream().forEach(id -> repository.deleteById(id));
        }
    }

}

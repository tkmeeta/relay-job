package com.yxsk.relay.job.admin.web.controller.dto;

import com.yxsk.relay.job.admin.data.entity.JobDetails;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.List;
import java.util.Map;

/**
 * @Author 11376
 * @CreaTime 2019/7/8 16:06
 * @Description
 */
@Getter
@Setter
@ToString
public class JobRelationQueryDto {

    // 被查询的任务信息
    private List<JobDetails> jobs;

    // 任务关系
    private Map<String, List<String>> relation;

}

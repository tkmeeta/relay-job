package com.yxsk.relay.job.admin.web.service;

import com.yxsk.relay.job.admin.data.entity.BaseEntity;
import com.yxsk.relay.job.admin.web.entity.Menu;
import com.yxsk.relay.job.admin.web.repository.MenuRepository;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @Author 11376
 * @CreaTime 2019/6/26 17:54
 * @Description
 */
@Service
@Transactional(propagation = Propagation.SUPPORTS)
public class MenuService extends AdminAbstractService<Menu, MenuRepository> {

    public List<Menu> getListBySortOk() {
        Sort sort = new Sort(Sort.Direction.ASC, "type", "sort");
        return repository.findAllByDelFlag(sort, BaseEntity.DelFlagEnum.NORMAL.getFlag());
    }

    public List<Menu> getListByExample(Example<Menu> example, Sort sort) {
        return repository.findAll(example, sort);
    }

    public List<Menu> getListByPid(String pid, String notId){
        Sort sort = new Sort(Sort.Direction.ASC, "sort");
        return repository.findByPidAndIdNot(sort, pid, notId);
    }

    public Integer getSortMax(String pid){
        return repository.findSortMax(pid);
    }
}

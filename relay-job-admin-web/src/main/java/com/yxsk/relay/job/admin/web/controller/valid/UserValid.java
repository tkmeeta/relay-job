package com.yxsk.relay.job.admin.web.controller.valid;

import lombok.Data;

import javax.validation.constraints.NotEmpty;
import java.io.Serializable;

/**
 * @author 小懒虫
 * @date 2018/8/14
 */
@Data
public class UserValid implements Serializable {
    @NotEmpty(message = "用户名不能为空")
    private String username;
//    @NotEmpty(message = "密码不能为空")
    private String confirm;
}

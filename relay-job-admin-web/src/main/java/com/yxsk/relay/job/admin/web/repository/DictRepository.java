package com.yxsk.relay.job.admin.web.repository;

import com.yxsk.relay.job.admin.data.repository.BaseRepository;
import com.yxsk.relay.job.admin.web.entity.Dict;

/**
 * @author 小懒虫
 * @date 2018/8/14
 */
public interface DictRepository extends BaseRepository<Dict> {

    /**
     * 根据字典标识查询
     *
     * @param name   字典标识
     * @param delFlag 状态
     */
    Dict findByNameAndDelFlag(String name, String delFlag);

    /**
     * 根据标识查询字典数据,且排查指定ID的字典
     *
     * @param name 字典标识
     * @param id   字典ID
     */
    Dict findByNameAndIdNot(String name, String id);

    Dict findByName(String name);
}

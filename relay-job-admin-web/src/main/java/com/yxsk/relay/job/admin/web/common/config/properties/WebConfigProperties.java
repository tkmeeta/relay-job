package com.yxsk.relay.job.admin.web.common.config.properties;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @Author 11376
 * @CreaTime 2019/6/25 17:09
 * @Description
 */
@Getter
@Setter
@ToString
@Component
@ConfigurationProperties(prefix = "relay.job.admin.web")
public class WebConfigProperties {

    // 开启登录验证码
    private boolean captchaOpen = false;

    // 登录有效天数
    private Integer loginEffectiveDays = 7;

}

package com.yxsk.relay.job.admin.web.repository;

import com.yxsk.relay.job.admin.data.repository.BaseRepository;
import com.yxsk.relay.job.admin.web.entity.User;

import java.util.List;

/**
 * @Author 11376
 * @CreaTime 2019/6/25 16:57
 * @Description
 */
public interface UserRepository extends BaseRepository<User> {

    List<User> findByUsername(String username);

    List<User> findByUsernameAndIdNot(String username, String s);
}

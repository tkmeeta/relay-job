package com.yxsk.relay.job.admin.web.common.context;

import com.yxsk.relay.job.admin.web.common.utils.HttpServletUtil;
import com.yxsk.relay.job.admin.web.common.utils.JwtUtil;
import com.yxsk.relay.job.admin.web.entity.User;
import com.yxsk.relay.job.admin.web.service.UserService;
import com.yxsk.relay.job.component.admin.utils.SpringBeanUtils;
import lombok.experimental.UtilityClass;

/**
 * @Author 11376
 * @CreaTime 2019/6/26 11:24
 * @Description
 */
@UtilityClass
public class HttpContextUtils {

    public User getRequestUser() {
        String token = JwtUtil.getRequestToken(HttpServletUtil.getRequest());
        String userId = JwtUtil.getUserIdFromToken(token);
        return SpringBeanUtils.getBean(UserService.class).findById(userId);
    }

}

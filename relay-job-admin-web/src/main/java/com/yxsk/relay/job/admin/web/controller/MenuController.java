package com.yxsk.relay.job.admin.web.controller;

import com.yxsk.relay.job.admin.utils.IdUtils;
import com.yxsk.relay.job.admin.web.common.timo.utility.DictUtil;
import com.yxsk.relay.job.admin.web.common.utils.EntityBeanUtil;
import com.yxsk.relay.job.admin.web.common.utils.HttpServletUtil;
import com.yxsk.relay.job.admin.web.controller.valid.MenuValid;
import com.yxsk.relay.job.admin.web.entity.Menu;
import com.yxsk.relay.job.admin.web.service.MenuService;
import com.yxsk.relay.job.component.common.protocol.message.base.ResultResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * @author 小懒虫
 * @date 2018/8/14
 */
@Controller
@RequestMapping("/system/menu")
public class MenuController {

    @Autowired
    private MenuService menuService;

    /**
     * 跳转到列表页面
     */
    @GetMapping("/index")
    public String index(Model model) {
        String search = HttpServletUtil.getRequest().getQueryString();
        model.addAttribute("search", search);
        return "/system/menu/index";
    }

    /**
     * 菜单数据列表
     */
    @GetMapping("/list")
    @ResponseBody
    public ResultResponse list(Menu menu) {
        // 创建匹配器，进行动态查询匹配
        ExampleMatcher matcher = ExampleMatcher.matching().
                withMatcher("title", match -> match.contains());

        // 获取菜单列表
        Example<Menu> example = Example.of(menu, matcher);
        Sort sort = new Sort(Sort.Direction.ASC, "sort");
        List<Menu> list = menuService.getListByExample(example, sort);

        list.forEach(editMenu -> {
            String type = String.valueOf(editMenu.getType());
            editMenu.setRemark(DictUtil.keyValue("MENU_TYPE", type));
        });
        return ResultResponse.ok(list);
    }

    /**
     * 获取排序菜单列表
     */
    @GetMapping("/sortList/{pid}/{notId}")
    @ResponseBody
    public Map<Integer, String> sortList(
            @PathVariable(value = "pid", required = false) String pid,
            @PathVariable(value = "notId", required = false) String notId) {
        // 本级排序菜单列表
        List<Menu> levelMenu = menuService.getListByPid(pid, notId != null ? notId : "");
        Map<Integer, String> sortMap = new TreeMap<>();
        for (int i = 1; i <= levelMenu.size(); i++) {
            sortMap.put(i, levelMenu.get(i - 1).getTitle());
        }
        return sortMap;
    }

    /**
     * 跳转到添加页面
     */
    @GetMapping({"/add", "/add/{pid}"})
    public String toAdd(@PathVariable(value = "pid", required = false) Menu pMenu, Model model) {
        model.addAttribute("pMenu", pMenu);
        return "/system/menu/add";
    }

    /**
     * 跳转到编辑页面
     */
    @GetMapping("/edit/{id}")
    public String toEdit(@PathVariable("id") Menu menu, Model model) {
        Menu pMenu = menuService.findById(menu.getPid());
        model.addAttribute("menu", menu);
        model.addAttribute("pMenu", pMenu);
        return "/system/menu/add";
    }

    /**
     * 保存添加/修改的数据
     *
     * @param valid 验证对象
     * @param menu  实体对象
     */
    @PostMapping("/save")
    @ResponseBody
    public ResultResponse save(@Validated MenuValid valid, Menu menu) {
        if (menu.getId() == null) {
            // 排序为空时，添加到最后
            if (menu.getSort() == null) {
                Integer sortMax = menuService.getSortMax(menu.getPid());
                menu.setSort(sortMax != null ? sortMax - 1 : 0);
            }

            // 添加全部上级序号
            Menu pMenu = menuService.findById(menu.getPid());
            menu.setPids(pMenu.getPids() + ",[" + menu.getPid() + "]");
        }

        // 复制保留无需修改的数据
        if (menu.getId() != null) {
            Menu beMenu = menuService.findById(menu.getId());
            EntityBeanUtil.copyProperties(beMenu, menu, "pids");
        }

        // 排序功能
        Integer sort = menu.getSort();
        List<Menu> levelMenu = menuService.getListByPid(menu.getPid(), menu.getId() != null ? menu.getId() : "");
        levelMenu.add(sort, menu);
        for (int i = 1; i <= levelMenu.size(); i++) {
            levelMenu.get(i - 1).setSort(i);
        }

        if (!StringUtils.hasLength(menu.getId())) {
            menu.setId(IdUtils.nextId());
        }

        // 保存数据
        menuService.addEntities(levelMenu);
        return ResultResponse.ok("成功");
    }

    /**
     * 跳转到详细页面
     */
    @GetMapping("/detail/{id}")
    public String toDetail(@PathVariable("id") Menu menu, Model model) {
        model.addAttribute("menu", menu);
        return "/system/menu/detail";
    }

}

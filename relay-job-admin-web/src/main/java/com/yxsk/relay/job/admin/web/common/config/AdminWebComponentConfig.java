package com.yxsk.relay.job.admin.web.common.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * @Author 11376
 * @CreaTime 2019/6/25 11:46
 * @Description
 */
@Configuration
@ComponentScan({"com.yxsk.relay.job.admin.config", "com.yxsk.relay.job.admin.web"})
public class AdminWebComponentConfig {
}

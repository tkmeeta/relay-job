package com.yxsk.relay.job.admin.web.common.exception;

import com.yxsk.relay.job.component.common.exception.RelayJobException;
import com.yxsk.relay.job.component.common.exception.RelayJobRuntimeException;
import com.yxsk.relay.job.component.common.protocol.message.base.Response;
import com.yxsk.relay.job.component.common.protocol.message.base.ResponseCodes;
import lombok.Getter;

/**
 * @Author 11376
 * @CreaTime 2019/6/25 16:13
 * @Description
 */
@Getter
public class RelayAdminWebRestfulRuntimeException extends RuntimeException {

    protected Response response;

    public RelayAdminWebRestfulRuntimeException(String errorCode, String message) {
        super(message);
        this.response = new Response(errorCode, message);
    }

    public RelayAdminWebRestfulRuntimeException(String cause) {
        super(cause);
        this.response = new Response(ResponseCodes.SERVICE_INVOKE_ERROR.getCode(), cause);
    }

    public RelayAdminWebRestfulRuntimeException(RelayJobException e) {
        super(e);
        this.response = new Response(ResponseCodes.SERVICE_INVOKE_ERROR.getCode(), e.getMessage());
    }

    public RelayAdminWebRestfulRuntimeException(RelayJobRuntimeException e) {
        super(e);
        this.response = new Response(ResponseCodes.SERVICE_INVOKE_ERROR.getCode(), e.getMessage());
    }

    public RelayAdminWebRestfulRuntimeException(RelayAdminWebRuntimeException e) {
        super(e);
        this.response = new Response(ResponseCodes.SERVICE_INVOKE_ERROR.getCode(), e.getMessage());
    }

    public RelayAdminWebRestfulRuntimeException(Throwable e) {
        super(e);
        this.response = ResponseCodes.SERVICE_INVOKE_ERROR;
    }

    public RelayAdminWebRestfulRuntimeException(String cause, Throwable e) {
        super(cause, e);
        this.response = new Response(ResponseCodes.SERVICE_INVOKE_ERROR.getCode(), cause);
    }
}

package com.yxsk.relay.job.admin.web.common.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 后台返回结果集枚举
 *
 * @author 小懒虫
 * @date 2018/8/14
 */
@Getter
@AllArgsConstructor
public enum ResultEnum {

    /**
     * 账户问题
     */
    USER_EXIST(101, "该用户名已经存在"),
    USER_PWD_NULL(102, "密码不能为空"),
    USER_INEQUALITY(103, "两次密码不一致"),
    USER_OLD_PWD_ERROR(104, "原来密码不正确"),
    USER_NAME_PWD_NULL(105, "用户名和密码不能为空"),
    USER_CAPTCHA_ERROR(106, "验证码错误"),
    USER_NOT_FOUND(107, "用户不存在"),
    PASSWORD_ERROR(108, "密码错误"),
    NO_ADMIN_AUTH(109, "不允许操作超级管理员"),

    /**
     * 字典问题
     */
    DICT_EXIST(201, "该字典标识已经存在，不允许重复！"),

    /**
     * 非法操作
     */
    STATUS_ERROR(301, "非法操作：状态有误"),

    /**
     * 权限问题
     */
    NO_PERMISSIONS(401, "权限不足！"),
    TOKEN_ERROR(402, "token错误");

    private Integer code;

    private String message;

}

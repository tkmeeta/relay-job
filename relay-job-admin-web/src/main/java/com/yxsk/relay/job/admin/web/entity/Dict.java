package com.yxsk.relay.job.admin.web.entity;

import com.yxsk.relay.job.admin.data.entity.BaseEntity;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author 小懒虫
 * @date 2018/8/14
 */
@Getter
@Setter
@ToString
@Entity
@Table(name = "sys_dict")
public class Dict extends BaseEntity {
    @Id
    private String id;
    private String name;
    private String title;
    private Byte type;
    @Column(columnDefinition = "TEXT")
    private String value;
    private String remark;
}

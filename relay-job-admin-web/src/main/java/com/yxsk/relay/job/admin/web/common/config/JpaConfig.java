package com.yxsk.relay.job.admin.web.common.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

/**
 * @Author 11376
 * @CreaTime 2019/6/25 17:40
 * @Description
 */
@Configuration
@EnableJpaRepositories("com.yxsk.relay.job.admin.web.repository")
public class JpaConfig {
}

package com.yxsk.relay.job.admin.web.service;

import com.yxsk.relay.job.admin.data.entity.BaseEntity;
import com.yxsk.relay.job.admin.data.repository.BaseRepository;
import com.yxsk.relay.job.admin.data.service.AbstractService;
import com.yxsk.relay.job.admin.web.common.data.PageSort;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;

/**
 * @Author 11376
 * @CreaTime 2019/6/26 20:24
 * @Description
 */
public abstract class AdminAbstractService<T extends BaseEntity, R extends BaseRepository> extends AbstractService<T, R> {

    public Page<T> getPageList(Example<T> example) {
        // 创建分页对象
        PageRequest page = PageSort.pageRequest(Sort.Direction.ASC);
        return repository.findAll(example, page);
    }

}

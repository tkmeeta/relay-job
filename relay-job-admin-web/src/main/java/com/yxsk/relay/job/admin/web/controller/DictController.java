package com.yxsk.relay.job.admin.web.controller;

import com.yxsk.relay.job.admin.utils.IdUtils;
import com.yxsk.relay.job.admin.web.common.enums.ResultEnum;
import com.yxsk.relay.job.admin.web.common.exception.RelayAdminWebRuntimeException;
import com.yxsk.relay.job.admin.web.common.utils.EntityBeanUtil;
import com.yxsk.relay.job.admin.web.controller.valid.DictValid;
import com.yxsk.relay.job.admin.web.entity.Dict;
import com.yxsk.relay.job.admin.web.service.DictService;
import com.yxsk.relay.job.component.common.protocol.message.base.ResultResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author 小懒虫
 * @date 2018/8/14
 */
@Controller
@RequestMapping("/system/dict")
public class DictController {

    @Autowired
    private DictService dictService;

    /**
     * 列表页面
     */
    @GetMapping("/index")
    public String index(Model model, Dict dict) {

        // 创建匹配器，进行动态查询匹配
        ExampleMatcher matcher = ExampleMatcher.matching().
                withMatcher("title", match -> match.contains());

        // 获取字典列表
        Example<Dict> example = Example.of(dict, matcher);
        Page<Dict> list = dictService.getPageList(example);

        // 封装数据
        model.addAttribute("list", list.getContent());
        model.addAttribute("page", list);
        return "/system/dict/index";
    }

    /**
     * 跳转到添加页面
     */
    @GetMapping("/add")
    public String toAdd() {
        return "/system/dict/add";
    }

    /**
     * 跳转到编辑页面
     */
    @GetMapping("/edit/{id}")
    public String toEdit(@PathVariable("id") Dict dict, Model model) {
        model.addAttribute("dict", dict);
        return "/system/dict/add";
    }

    /**
     * 保存添加/修改的数据
     *
     * @param valid 验证对象
     */
    @PostMapping({"/add", "/edit"})
    @ResponseBody
    public ResultResponse save(@Validated DictValid valid, Dict dict) {
        // 清除字典值两边空格
        dict.setValue(dict.getValue().trim());

        // 判断角色标识是否重复
        if (dictService.repeatByName(dict)) {
            throw new RelayAdminWebRuntimeException(ResultEnum.DICT_EXIST);
        }

        // 复制保留无需修改的数据
        if (dict.getId() != null) {
            Dict beDict = dictService.findById(dict.getId());
            EntityBeanUtil.copyProperties(beDict, dict);
        } else {
            dict.setId(IdUtils.nextId());
        }

        // 保存数据
        dictService.addEntity(dict);
        return ResultResponse.ok("操作成功");
    }

    /**
     * 跳转到详细页面
     */
    @GetMapping("/detail/{id}")
    public String toDetail(@PathVariable("id") Dict dict, Model model) {
        model.addAttribute("dict", dict);
        return "/system/dict/detail";
    }

    /**
     * 设置一条或者多条数据的状态
     */
    @RequestMapping("/delete")
    @ResponseBody
    public ResultResponse delete(@RequestParam(value = "ids", required = false) List<String> ids) {
        this.dictService.delete(ids);
        return ResultResponse.ok("删除成功");
    }
}

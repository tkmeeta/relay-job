package com.yxsk.relay.job.admin.web.entity;

import com.yxsk.relay.job.admin.data.entity.BaseEntity;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @Author 11376
 * @CreaTime 2019/6/25 16:57
 * @Description
 */
@Getter
@Setter
@ToString
@Table(name = "SYS_USER")
@Entity
public class User extends BaseEntity {

    private String username;

    private String password;

    private String salt;

    private String email;

    private String phone;

    private String remark;

}

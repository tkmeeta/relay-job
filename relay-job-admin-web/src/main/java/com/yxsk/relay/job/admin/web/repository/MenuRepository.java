package com.yxsk.relay.job.admin.web.repository;

import com.yxsk.relay.job.admin.data.repository.BaseRepository;
import com.yxsk.relay.job.admin.web.entity.Menu;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

/**
 * @Author 11376
 * @CreaTime 2019/6/26 17:55
 * @Description
 */
public interface MenuRepository extends BaseRepository<Menu> {

    List<Menu> findAllByDelFlag(Sort sort, String flag);

    List<Menu> findByPidAndIdNot(Sort sort, String pid, String notId);

    @Query(value = "select max(sort) from Menu m where m.pid = ?1 and m.delFlag <> '1'")
    Integer findSortMax(String pid);
}

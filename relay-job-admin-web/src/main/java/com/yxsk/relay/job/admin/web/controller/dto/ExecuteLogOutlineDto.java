package com.yxsk.relay.job.admin.web.controller.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.util.List;

/**
 * @Author 11376
 * @CreaTime 2019/7/2 17:45
 * @Description
 */
@Getter
@Setter
@ToString
@NoArgsConstructor
public class ExecuteLogOutlineDto implements Serializable {

    // 时间
    private List<String> time;

    // 总次数
    private List<Integer> totalTimes;

    // 失败次数
    private List<Integer> failedTimes;

}

package com.yxsk.relay.job.admin.web.controller.dto;

import lombok.*;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.io.Serializable;

/**
 * @Author 11376
 * @CreaTime 2019/7/2 17:45
 * @Description
 */
@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class ExecuteLogOutlineRequestDto implements Serializable {

    private String startTime;

    private String endTime;

    private String appId;

    private String jobId;

    // 查询粒度, 即按照时间段均分为多少份
    @NotNull(message = "查询粒度不能为空")
    private Integer granularity;

    @NotEmpty(message = "指标模型不能为空")
    @Pattern(regexp = "trigger|execute", message = "指标模型值错误, 允许值[trigger:任务触发;execute:任务执行]")
    private String model;

    @AllArgsConstructor
    @Getter
    public enum DataModel {

        TRIGGER("trigger"),
        EXECUTE("execute");

        private String model;
    }

}

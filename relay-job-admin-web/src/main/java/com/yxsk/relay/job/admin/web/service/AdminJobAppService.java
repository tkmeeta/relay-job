package com.yxsk.relay.job.admin.web.service;

import com.yxsk.relay.job.admin.data.entity.JobApps;
import com.yxsk.relay.job.admin.data.repository.JobAppsRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * @Author 11376
 * @CreaTime 2019/6/26 20:31
 * @Description
 */
@Service
@Transactional(propagation = Propagation.SUPPORTS)
public class AdminJobAppService extends AdminAbstractService<JobApps, JobAppsRepository> {


}

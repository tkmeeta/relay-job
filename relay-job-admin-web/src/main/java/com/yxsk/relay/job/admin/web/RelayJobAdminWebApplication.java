package com.yxsk.relay.job.admin.web;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @Author 11376
 * @CreaTime 2019/6/25 11:44
 * @Description
 */
@SpringBootApplication
public class RelayJobAdminWebApplication {

    public static void main(String[] args) {
        SpringApplication.run(RelayJobAdminWebApplication.class, args);
    }

}

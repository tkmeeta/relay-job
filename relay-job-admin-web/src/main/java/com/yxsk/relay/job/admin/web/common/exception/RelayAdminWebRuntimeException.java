package com.yxsk.relay.job.admin.web.common.exception;

import com.yxsk.relay.job.admin.web.common.enums.ResultEnum;
import com.yxsk.relay.job.component.common.exception.RelayJobException;
import com.yxsk.relay.job.component.common.exception.RelayJobRuntimeException;
import com.yxsk.relay.job.component.common.protocol.message.base.Response;
import com.yxsk.relay.job.component.common.protocol.message.base.ResponseCodes;
import lombok.Getter;

/**
 * @Author 11376
 * @CreaTime 2019/6/25 16:13
 * @Description
 */
@Getter
public class RelayAdminWebRuntimeException extends RuntimeException {

    protected Response response;

    public RelayAdminWebRuntimeException(ResultEnum resultEnum) {
        super(resultEnum.getMessage());
        this.response = new Response(resultEnum.getCode().toString(), resultEnum.getMessage());
    }

    public RelayAdminWebRuntimeException(String cause) {
        super(cause);
        this.response = new Response(ResponseCodes.SERVICE_INVOKE_ERROR.getCode(), cause);
    }

    public RelayAdminWebRuntimeException(RelayJobException e) {
        super(e);
        this.response = new Response(ResponseCodes.SERVICE_INVOKE_ERROR.getCode(), e.getMessage());
    }

    public RelayAdminWebRuntimeException(RelayJobRuntimeException e) {
        super(e);
        this.response = new Response(ResponseCodes.SERVICE_INVOKE_ERROR.getCode(), e.getMessage());
    }

    public RelayAdminWebRuntimeException(Throwable e) {
        super(e);
        this.response = ResponseCodes.SERVICE_INVOKE_ERROR;
    }

    public RelayAdminWebRuntimeException(String cause, Throwable e) {
        super(cause, e);
        this.response = new Response(ResponseCodes.SERVICE_INVOKE_ERROR.getCode(), cause);
    }
}

layui.use(['laydate', 'element', 'form', 'layer'], function () {
    var $ = layui.jquery;
    var element = layui.element; //加载element模块
    var form = layui.form; //加载form模块
    var layer = layui.layer; //加载layer模块
    var laydate = layui.laydate;

    //初始化laydate实例
    laydate.render({
        elem: '#laydate1E',
        type: 'datetime'
    });
    laydate.render({
        elem: '#laydate2E',
        type: 'datetime'
    });
    laydate.render({
        elem: '#laydate1T',
        type: 'datetime'
    });
    laydate.render({
        elem: '#laydate2T',
        type: 'datetime'
    });

    $(document).on("click", ".ajax-echarts", function (e) {
        e.preventDefault();
        var form = $(this).parents("form");
        var url = form.attr("action");
        var serializeArray = form.serializeArray();

        $.post(url, serializeArray, function (result) {
            if (result.code != '0000') {
                $.fn.Messager(result);
            } else {
                // 取 class为 "echarts"
                var echarts = form.children(".echarts");
                var data = result.data;
                doubleColumn(echarts[0], data.time, data.totalTimes, data.failedTimes, echarts[0].getAttribute("echartsHead").split(','), echarts[0].getAttribute("echartsColor").split(','));
            }
        });
    });

    var Format = function(date) {
        var year = date.getFullYear();
        var month =(date.getMonth() + 1).toString();
        var day = (date.getDate()).toString();
        var hour = (date.getHours()).toString();
        var minute = (date.getMinutes()).toString();
        var second = (date.getSeconds()).toString();
        if (month.length == 1) {
            month = "0" + month;
        }
        if (day.length == 1) {
            day = "0" + day;
        }
        if (hour.length == 1) {
            hour = "0" + hour;
        }
        if (minute.length == 1) {
            minute = "0" + minute;
        }
        if (second.length == 1) {
            second = "0" + second;
        }
        var dateTime = year + "-" + month + "-" + day +" "+ hour +":"+minute+":"+second;
        return dateTime;
    }

    $(document).ready(function(){
        var e = $(".ajax-echarts");
        $.each(e,function(index, value){
            // 查询触发起始时间、执行时间为24小时之前
            var now = new Date();
            var nowDateTime = Format(now);
            $("#laydate2T").val(nowDateTime);
            $("#laydate2E").val(nowDateTime);

            now.setDate(now.getDate()-1);//设置天数 -1 天
            var oneDayAgo = Format(now);
            $("#laydate1T").val(oneDayAgo);
            $("#laydate1E").val(oneDayAgo);
            value.click();
        });
    });

});
if(window.top!==window.self){window.top.location=window.location};
layui.use(['element'], function () {
    var $ = layui.jquery;
    $(document).on('click', '.captcha-img', function () {
        var src = this.src.split("?")[0];
        this.src = src + "?" + Math.random();
    });
    $(document).on('click', '.ajax-login', function (e) {
        e.preventDefault();
        var form = $(this).parents("form");
        var url = form.attr("action");
        var serializeArray = form.serializeArray();
        $.post(url, serializeArray, function (result) {
            if(result.code != '0000'){
                $('.captcha-img').click();
            } else {
                // 保存 token
                var d = new Date();
                d.setDate(d.getDate() + result.data.effectiveDays);
                document.cookie = result.data.tokenHeaderName +"="+ result.data.token + ";expires=" + d.toGMTString();
            }
            $.fn.Messager(result);
        });
    })
});
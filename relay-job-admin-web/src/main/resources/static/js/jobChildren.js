$(document).ready(function(){
    var jobId = $("#jobId").val();
    var e = document.getElementById('echarts');
    var myChart = echarts.init(e);
    myChart.showLoading();
    $.get('/admin/quartz/job/getJobChildren/' + jobId, function (data) {
        myChart.hideLoading();
        myChart.setOption(option = {
            tooltip: {
                trigger: 'item',
                triggerOn: 'mousemove'
            },
            series:[
                {
                    type: 'tree',
                    data: [data.data],
                    left: '2%',
                    right: '2%',
                    top: '8%',
                    bottom: '8%',
                    symbol: 'emptyCircle',
                    orient: 'vertical',
                    expandAndCollapse: true,
                    initialTreeDepth: -1,
                    label: {
                        normal: {
                            position: 'top',
                            rotate: 0,
                            verticalAlign: 'middle',
                            align: 'left',
                            fontSize: 14
                        }
                    },
                    leaves: {
                        label: {
                            normal: {
                                position: 'bottom',
                                rotate: 0,
                                verticalAlign: 'middle',
                                align: 'left'
                            }
                        }
                    },
                    animationDurationUpdate: 750
                }
            ]
        });
    });
});
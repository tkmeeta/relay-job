# relay-job

#### 演示地址
http://106.54.121.190
账号：admin
密码：111111

#### 介绍
RELAY-JOB任务调度平台

#### 软件架构
系统以组件为基础开发，具体执行流程可参考任务执行流程图

#### 演示工程部署
1. 安装jdk、maven环境，安装docker运行环境，安装docker-compose工具  
2. git clone代码      
3. mvn clean install编译工程
4. 打包admin镜像：切换至relay-job-admin-web目录，mvn clean package docker:build打包admin-web镜像  
5. 打包demo镜像：切换relay-job-endpoint-demo/relay-job-endpoint-demo-springboot，mvn clean package docker:build打包endpoint-springboot镜像  
6. 部署服务：切换doc/deploy目录，docker-compose up -d部署服务
7. 访问地址：http://ip

#### 安装教程

1. 安装Java运行环境, Jre1.8以上  
2. 执行doc文件夹下MySQL数据库脚本，修改工程application.yml数据库配置
3. java -jar relay-job-admin-web.jar 启动管理Web端，登录账户admin，密码111111
4. 客户端自行集成工程，eg：relay-job-endpoint-demo

#### 使用说明
1. 处理器名称： 为任务执行处理器名称，@JobHandler.value。任务处理器需实现JobExecutor接口
2. 定时任务自动执行：任务添加corn表达式
3. 触发器说明：  
    · 分片触发器：该触发器为异步任务执行任务执行之前会调用客户端Partitionable接口，如果客户端处理失败会继续在下一个客户端执行，直到执行成功或无可用客户端  
    · 同步触发器：触发任务与执行任务同步执行  
    · 异步触发器：触发任务与执行任务异步执行，任务执行结果由客户端回调，回调失败时管理端有补偿机制
4. 执行模式：  
    · 经典模式：每个任务执行单元执行完成后触发子任务，子任务执行参数使用其子任务配置的执行参数  
    · 流水线模式：每个任务执行单元执行完成后触发子任务，子任务执行参数使用该任务执行结果返回的参数  
    · 集群模式：触发任务后等待该触发任务所有的执行完成后，收集执行结果进行计算，任务需实现Collectable接口，如果计算出错继续下一个客户端计算，直到计算成功或无可用执行端点  
5. 阻塞策略：  
    · 串行策略：该任务在某一时间段只会有一个在途执行任务
    · 无阻塞： 该任务执行无阻塞  
6. 执行参数：触发该任务的执行参数，优先级最低  
7. 执行超时时间：该配置仅对异步执行任务生效
8. 失败重试次数：每个执行单元执行失败后重试的次数

#### 参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request

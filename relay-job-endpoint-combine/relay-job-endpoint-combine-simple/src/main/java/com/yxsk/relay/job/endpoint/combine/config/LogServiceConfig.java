package com.yxsk.relay.job.endpoint.combine.config;

import com.yxsk.relay.job.component.endpoint.config.RelayJobSlaveConfig;
import com.yxsk.relay.job.component.endpoint.log.logger.RelayJobLoggerFactory;
import com.yxsk.relay.job.component.endpoint.log.stream.LocalDiskLogStream;
import com.yxsk.relay.job.component.endpoint.log.stream.LogStream;
import org.apache.commons.lang3.StringUtils;

/**
 * @Description
 * @Author 11376
 * @CreateTime 2019/9/11 21:44
 */
public class LogServiceConfig extends EndpointCombineAutoConfig {

    private LogStream logStream;

    private RelayJobSlaveConfig slaveConfig;

    public LogServiceConfig(RelayJobSlaveConfig slaveConfig) {
        this.slaveConfig = slaveConfig;
    }

    public synchronized LogStream logStream() {
        if (logStream != null) {
            return this.logStream;
        }
        String logRootPath = this.slaveConfig.getLogRootPath();
        LocalDiskLogStream localDiskLogStream = new LocalDiskLogStream(StringUtils.isEmpty(logRootPath) ? RelayJobSlaveConfig.DEFAULT_LOG_ROOT_PATH : logRootPath);
        localDiskLogStream.init();

        this.logStream = localDiskLogStream;
        return logStream;
    }

    @Override
    protected void autoConfig() {
        RelayJobLoggerFactory.init(this.logStream(), this.slaveConfig.getLogPattern());
    }

    @Override
    protected String componentName() {
        return "Relay job execute log service";
    }

}

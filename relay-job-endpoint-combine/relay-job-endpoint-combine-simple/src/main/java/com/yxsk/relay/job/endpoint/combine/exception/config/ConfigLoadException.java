package com.yxsk.relay.job.endpoint.combine.exception.config;

import com.yxsk.relay.job.endpoint.combine.exception.EndpointCombineException;

/**
 * @Description
 * @Author 11376
 * @CreateTime 2019/9/10 21:30
 */
public class ConfigLoadException extends EndpointCombineException {
    public ConfigLoadException(String cause) {
        super(cause);
    }

    public ConfigLoadException(Throwable e) {
        super(e);
    }

    public ConfigLoadException(String cause, Throwable e) {
        super(cause, e);
    }
}

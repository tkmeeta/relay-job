package com.yxsk.relay.job.endpoint.combine.config;

import com.yxsk.relay.job.component.common.utils.DateUtils;
import lombok.extern.slf4j.Slf4j;

/**
 * @Description
 * @Author 11376
 * @CreateTime 2019/9/9 15:49
 */
@Slf4j
public abstract class EndpointCombineAutoConfig {

    public void startConfig() {
        long start = DateUtils.getCurrentTimeMillis();
        if (log.isDebugEnabled()) {
            log.debug("{} initialization...", componentName());
        }
        this.autoConfig();
        if (log.isDebugEnabled()) {
            log.debug("{} started in {}ms", componentName(), DateUtils.getCurrentTimeMillis() - start);
        }
    }

    protected abstract void autoConfig();

    protected abstract String componentName();

}

package com.yxsk.relay.job.endpoint.combine.config;

import com.yxsk.relay.job.component.endpoint.config.RelayJobAdminConfig;
import com.yxsk.relay.job.component.endpoint.config.RelayJobSlaveConfig;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * @Description
 * @Author 11376
 * @CreateTime 2019/9/10 20:21
 */
@Getter
@Setter
@ToString
@ConfigPropertiesPrefix("relay.job")
public class EndpointConfigCombine {

    private RelayJobAdminConfig admin;

    private RelayJobSlaveConfig slave;

}

package com.yxsk.relay.job.endpoint.combine.exception.resource;

import com.yxsk.relay.job.component.common.exception.RelayJobRuntimeException;

/**
 * @Description
 * @Author 11376
 * @CreateTime 2019/9/9 16:56
 */
public class ResourceLoadException extends RelayJobRuntimeException {
    public ResourceLoadException(String cause) {
        super(cause);
    }

    public ResourceLoadException(Throwable e) {
        super(e);
    }

    public ResourceLoadException(String cause, Throwable e) {
        super(cause, e);
    }
}

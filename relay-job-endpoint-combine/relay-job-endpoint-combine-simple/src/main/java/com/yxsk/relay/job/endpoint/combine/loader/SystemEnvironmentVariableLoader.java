package com.yxsk.relay.job.endpoint.combine.loader;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

/**
 * @Description
 * @Author 11376
 * @CreateTime 2019/9/10 20:56
 */
public class SystemEnvironmentVariableLoader {

    public String getProperty(String key, String defaultValue) {
        // 环境变量
        return System.getProperty(key, defaultValue);
    }

    public Map<String, String> getAllProperties() {
        Properties properties = System.getProperties();
        HashMap<String, String> variables = new HashMap<>();
        variables.putAll(System.getenv());
        properties.stringPropertyNames().stream().forEach(key -> variables.put(key, properties.getProperty(key)));
        return variables;
}

}

package com.yxsk.relay.job.endpoint.combine.exception;

import com.yxsk.relay.job.component.common.exception.RelayJobRuntimeException;

/**
 * @Description
 * @Author 11376
 * @CreateTime 2019/9/9 16:55
 */
public class EndpointCombineException extends RelayJobRuntimeException {
    public EndpointCombineException(String cause) {
        super(cause);
    }

    public EndpointCombineException(Throwable e) {
        super(e);
    }

    public EndpointCombineException(String cause, Throwable e) {
        super(cause, e);
    }
}

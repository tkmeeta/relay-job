package com.yxsk.relay.job.endpoint.combine.config;

import java.lang.annotation.*;

/**
 * @Description
 * @Author 11376
 * @CreateTime 2019/9/10 20:50
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE, ElementType.FIELD})
@Documented
public @interface ConfigPropertiesPrefix {

    String value() default "";

}

package com.yxsk.relay.job.endpoint.combine.config;

import com.yxsk.relay.job.component.common.net.handler.convert.CompressAbleMessageConvert;
import com.yxsk.relay.job.component.common.net.handler.convert.NetMessageConvert;
import com.yxsk.relay.job.component.common.net.serialization.JsonMessageEntitySerializer;
import com.yxsk.relay.job.component.common.net.serialization.MessageEntitySerializer;
import com.yxsk.relay.job.component.common.protocol.caller.DefaultHttpClientFactory;
import com.yxsk.relay.job.component.common.protocol.caller.HttpClientFactory;
import com.yxsk.relay.job.component.common.protocol.caller.HttpRestfulRemoteCaller;
import com.yxsk.relay.job.component.common.protocol.caller.RemoteCaller;
import com.yxsk.relay.job.component.common.thread.lifecycle.ThreadLifecycle;
import com.yxsk.relay.job.component.common.thread.observe.ObservableThread;
import com.yxsk.relay.job.component.endpoint.config.RelayJobAdminConfig;
import com.yxsk.relay.job.component.endpoint.config.RelayJobSlaveConfig;
import com.yxsk.relay.job.component.endpoint.registry.SigningTask;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * @Description
 * @Author 11376
 * @CreateTime 2019/9/9 15:38
 */
@AllArgsConstructor
public class AdminRegistryConfig extends EndpointCombineAutoConfig {

    private RelayJobAdminConfig adminConfig;
    private RelayJobSlaveConfig slaveConfig;

    protected void autoConfig() {
        ObservableThread signThread = this.signThread();
        signThread.start();
    }

    @Override
    protected String componentName() {
        return "Admin registry task";
    }

    private RemoteCaller remoteCaller() {
        return HttpRestfulRemoteCaller.builder()
                .httpClientFactory(httpClientFactory())
                .serializer(serializer())
                .messageConvert(messageConvert())
                .build();
    }

    private HttpClientFactory httpClientFactory() {
        return new DefaultHttpClientFactory();
    }

    private NetMessageConvert messageConvert() {
        return new CompressAbleMessageConvert();
    }

    private MessageEntitySerializer serializer() {
        return new JsonMessageEntitySerializer();
    }

    private SigningTask signingTask() {
        return new SigningTask(adminConfig, slaveConfig, remoteCaller());
    }

    private SignTaskLifecycle signTaskLifecycle() {
        return new SignTaskLifecycle();
    }

    private ObservableThread signThread() {
        return new ObservableThread(signTaskLifecycle(), signingTask());
    }

    @Slf4j
    private static class SignTaskLifecycle implements ThreadLifecycle {

        @Override
        public void onNew() {
            if (log.isDebugEnabled()) {
                log.debug("Relay job sign task init.");
            }
        }

        @Override
        public void onStart(Thread thread) {
            if (log.isDebugEnabled()) {
                log.debug("Relay job sign task started.");
            }
        }

        @Override
        public void onRunning(Thread thread) {
            if (log.isDebugEnabled()) {
                log.debug("Relay job sign task running...");
            }
        }

        @Override
        public void onFinish(Thread thread, Object o) {
            if (log.isDebugEnabled()) {
                log.debug("Relay job sign task shutdown.");
            }
        }

        @Override
        public void onError(Thread thread, Exception e) {
            log.error("Relay job error.", e);
        }
    }

}

package com.yxsk.relay.job.endpoint.combine.loader;

/**
 * @Description
 * @Author 11376
 * @CreateTime 2019/9/9 15:58
 */
public interface ResourceLoader {

    <T> T load(String path, Class<T> resourceType);

}

package com.yxsk.relay.job.endpoint.combine.exception.resource;

import com.yxsk.relay.job.endpoint.combine.exception.EndpointCombineException;

/**
 * @Description
 * @Author 11376
 * @CreateTime 2019/9/9 16:51
 */
public class ResourceNotFoundException extends ResourceLoadException {
    public ResourceNotFoundException(String cause) {
        super(cause);
    }

    public ResourceNotFoundException(Throwable e) {
        super(e);
    }

    public ResourceNotFoundException(String cause, Throwable e) {
        super(cause, e);
    }
}

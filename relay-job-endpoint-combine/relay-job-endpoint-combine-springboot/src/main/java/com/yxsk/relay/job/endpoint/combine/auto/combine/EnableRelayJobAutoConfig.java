package com.yxsk.relay.job.endpoint.combine.auto.combine;

import com.yxsk.relay.job.component.endpoint.handle.JobExecutor;
import com.yxsk.relay.job.endpoint.combine.auto.config.RelayJobEndpointComponentScan;
import com.yxsk.relay.job.endpoint.combine.auto.config.components.JobExecutorContextConfig;
import org.springframework.context.annotation.Import;
import org.springframework.core.annotation.AliasFor;

import java.lang.annotation.*;

/**
 * @Description
 * @Author 11376
 * @CreateTime 2019/9/14 23:25
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@Documented
@JobExecutorContextConfig.RelayJobHandlerAutoScan
@Import(RelayJobEndpointComponentScan.class)
public @interface EnableRelayJobAutoConfig {

    @AliasFor(value = "basePackages", annotation = JobExecutorContextConfig.RelayJobHandlerAutoScan.class)
    String[] basePackages();

    @AliasFor(value = "classes", annotation = JobExecutorContextConfig.RelayJobHandlerAutoScan.class)
    Class<? extends JobExecutor>[] classes() default {};

}

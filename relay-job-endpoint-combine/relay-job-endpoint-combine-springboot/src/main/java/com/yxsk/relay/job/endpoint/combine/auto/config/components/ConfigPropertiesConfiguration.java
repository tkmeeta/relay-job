package com.yxsk.relay.job.endpoint.combine.auto.config.components;

import com.yxsk.relay.job.endpoint.combine.auto.config.properties.RelayJobAdminConfigProperties;
import com.yxsk.relay.job.endpoint.combine.auto.config.properties.RelayJobSlaveConfigProperties;
import org.springframework.boot.autoconfigure.AutoConfigureOrder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @Description
 * @Author 11376
 * @CreateTime 2019/9/14 22:47
 */
@Configuration
@AutoConfigureOrder(1)
public class ConfigPropertiesConfiguration {

    @Bean
    public RelayJobAdminConfigProperties adminConfigProperties() {
        return new RelayJobAdminConfigProperties();
    }

    @Bean
    public RelayJobSlaveConfigProperties slaveConfigProperties() {
        return new RelayJobSlaveConfigProperties();
    }

}

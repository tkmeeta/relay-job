package com.yxsk.relay.job.endpoint.combine.auto.config.properties;

import com.yxsk.relay.job.component.endpoint.config.RelayJobAdminConfig;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @Description
 * @Author 11376
 * @CreateTime 2019/9/14 22:35
 */
@ConfigurationProperties("relay.job.admin")
public class RelayJobAdminConfigProperties extends RelayJobAdminConfig {
}

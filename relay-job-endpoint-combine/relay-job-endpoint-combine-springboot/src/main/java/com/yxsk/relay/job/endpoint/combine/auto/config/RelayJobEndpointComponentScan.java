package com.yxsk.relay.job.endpoint.combine.auto.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * @Description
 * @Author 11376
 * @CreateTime 2019/9/16 21:06
 */
@Configuration
@ComponentScan(basePackages = "com.yxsk.relay.job.endpoint.combine.auto.config.components")
public class RelayJobEndpointComponentScan {
}

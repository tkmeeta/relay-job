package com.yxsk.relay.job.endpoint.combine.auto.config.components;

import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.AutoConfigureBefore;
import org.springframework.boot.autoconfigure.AutoConfigureOrder;
import org.springframework.context.annotation.Configuration;

/**
 * @Description
 * @Author 11376
 * @CreateTime 2019/9/14 23:35
 */
@Configuration
@AutoConfigureOrder(6)
public class LogoPrintConfig implements ApplicationRunner {

    @Override
    public void run(ApplicationArguments args) {
        this.printBanner();
    }

    private void printBanner() {
        System.out.println("    #############################################################");
        System.out.println("   #                                                             #");
        System.out.println("  #                           RELAY-JOB                           #");
        System.out.println("   #                                                             #");
        System.out.println("    #############################################################");
    }

}

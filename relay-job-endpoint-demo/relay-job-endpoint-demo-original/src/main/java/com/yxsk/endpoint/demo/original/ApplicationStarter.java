package com.yxsk.endpoint.demo.original;

import com.yxsk.relay.job.endpoint.combine.auto.RelayJobEndpointAutoCombine;

/**
 * @Description
 * @Author 11376
 * @CreateTime 2019/9/10 22:21
 */
public class ApplicationStarter extends RelayJobEndpointAutoCombine {
    
    public static void main(String[] args) {
        new ApplicationStarter().start();
        System.out.println("启动成功");
    }
    
}

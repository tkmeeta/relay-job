package com.yxsk.endpoint.demo.springboot.job;

import com.yxsk.relay.job.component.endpoint.handle.JobExecutor;
import com.yxsk.relay.job.component.endpoint.handle.anno.JobHandler;

import java.util.Random;

/**
 * @Author 11376
 * @CreaTime 2019/6/16 10:40
 * @Description
 */
@JobHandler("testJob")
public class SimpleJob implements JobExecutor {

    @Override
    public String execute(String param) {
        System.out.println("接收到请求，do work....");
        try {
            Thread.sleep(new Random().nextInt(5) * 1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return "任务执行成功";
    }

}

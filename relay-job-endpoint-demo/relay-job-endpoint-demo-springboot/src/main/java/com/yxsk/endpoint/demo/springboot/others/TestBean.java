package com.yxsk.endpoint.demo.springboot.others;

import com.yxsk.relay.job.component.endpoint.log.logger.RelayJobLoggerFactory;
import org.slf4j.Logger;
import org.springframework.stereotype.Component;

/**
 * @Description
 * @Author 11376
 * @CreateTime 2019/9/16 16:02
 */
@Component
public class TestBean {

    private static final Logger logger = RelayJobLoggerFactory.getLogger(TestBean.class);

    public void call() {
        logger.debug("执行业务代码");
    }

}

package com.yxsk.endpoint.demo.springboot.job;

import com.yxsk.relay.job.component.common.vo.Endpoint;
import com.yxsk.relay.job.component.common.vo.PartitionInfo;
import com.yxsk.relay.job.component.endpoint.handle.JobExecutor;
import com.yxsk.relay.job.component.endpoint.handle.Partitionable;
import com.yxsk.relay.job.component.endpoint.handle.anno.JobHandler;
import com.yxsk.relay.job.component.endpoint.log.logger.RelayJobLoggerFactory;
import org.slf4j.Logger;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * @Author 11376
 * @CreaTime 2019/7/19 17:50
 * @Description
 */
@JobHandler("partitionJob")
public class PartitionHandleJob implements JobExecutor, Partitionable {

    private static final Logger logger = RelayJobLoggerFactory.getLogger(PartitionHandleJob.class);

    @Override
    public String execute(String param) {
        logger.info("开始执行任务, 执行参数: {}", param);

        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        logger.info("执行任务完成");

        return "成功, ok";
    }

    @Override
    public List<PartitionInfo> partition(List<Endpoint> availableEndpoints, String executeParam) {
        logger.info("收到分片请求, 端点列表：" + availableEndpoints);
        List<PartitionInfo> partitionInfos = new ArrayList<>();
        availableEndpoints.stream().forEach(endpoint -> {
            PartitionInfo info = new PartitionInfo();
            info.setEndpoint(endpoint);
            info.setExecuteParam(UUID.randomUUID().toString());
            partitionInfos.add(info);
        });
        logger.debug("分片结果：{}", partitionInfos);
        return partitionInfos;
    }
}

package com.yxsk.endpoint.demo.springboot.job;

import com.yxsk.endpoint.demo.springboot.others.TestBean;
import com.yxsk.relay.job.component.endpoint.async.RelayJobAsyncWorker;
import com.yxsk.relay.job.component.endpoint.handle.JobExecutor;
import com.yxsk.relay.job.component.endpoint.handle.anno.JobHandler;
import com.yxsk.relay.job.component.endpoint.log.logger.RelayJobLoggerFactory;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @Author 11376
 * @CreaTime 2019/6/30 11:56
 * @Description
 */
@JobHandler("asyncHandler")
public class AsyncHandleJob implements JobExecutor {

    private static final Logger logger = RelayJobLoggerFactory.getLogger(AsyncHandleJob.class);

    @Autowired
    private TestBean testBean;

    @Override
    public String execute(String param) {
        logger.debug("执行异步线程处理任务");

        logger.info("任务执行中");

        this.testBean.call();

        new Thread(new AsyncHandle()).start();

        return "执行子任务成功";
    }

    private class AsyncHandle extends RelayJobAsyncWorker implements Runnable {

        @Override
        protected Object work() {
            logger.debug("开始处理异步任务");

            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            logger.info("异步任务处理完成");

            return null;
        }

        @Override
        public void run() {
            try {
                this.call();
            } catch (Exception e) {
                logger.error("任务执行错误", e);
            }
        }
    }

}

package com.yxsk.endpoint.demo.springboot.job;

import com.yxsk.relay.job.component.common.exception.RelayJobException;
import com.yxsk.relay.job.component.endpoint.handle.JobExecutor;
import com.yxsk.relay.job.component.endpoint.handle.anno.JobHandler;
import com.yxsk.relay.job.component.endpoint.log.logger.RelayJobLoggerFactory;
import org.slf4j.Logger;

/**
 * @Author 11376
 * @CreaTime 2019/6/23 23:14
 * @Description
 */
@JobHandler("testChild")
public class ChildJob implements JobExecutor {

    private static final Logger logger = RelayJobLoggerFactory.getLogger(ChildJob.class);

    @Override
    public String execute(String param) throws RelayJobException {
        logger.debug("执行子任务");

        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        logger.info("任务执行中");

        try {
            simulateException();
        } catch (Exception e) {
            logger.error("任务执行错误", e);
            throw new RelayJobException(e);
        }

        return "执行子任务成功";
    }

    private void simulateException() {
        throw new RuntimeException("执行异常");
    }

}

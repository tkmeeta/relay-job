package com.yxsk.endpoint.demo.springboot;

import com.yxsk.relay.job.endpoint.combine.auto.combine.EnableRelayJobAutoConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @Description
 * @Author 11376
 * @CreateTime 2019/9/14 23:47
 */
@SpringBootApplication
@EnableRelayJobAutoConfig(basePackages = "com.yxsk.endpoint.demo.springboot.job")
public class ApplicationStarter {

    public static void main(String[] args) {
        SpringApplication.run(ApplicationStarter.class, args);
    }

}

package com.yxsk.endpoint.demo.springboot.job;

import com.yxsk.relay.job.component.common.exception.RelayJobException;
import com.yxsk.relay.job.component.common.vo.EndpointExecuteResult;
import com.yxsk.relay.job.component.endpoint.handle.Collectable;
import com.yxsk.relay.job.component.endpoint.handle.JobExecutor;
import com.yxsk.relay.job.component.endpoint.handle.anno.JobHandler;
import com.yxsk.relay.job.component.endpoint.log.logger.RelayJobLoggerFactory;
import org.slf4j.Logger;

import java.util.List;

/**
 * @Author 11376
 * @CreaTime 2019/8/6 16:34
 * @Description
 */
@JobHandler("collectAbleHandleJob")
public class CollectAbleHandleJob implements JobExecutor, Collectable {

    private static final Logger logger = RelayJobLoggerFactory.getLogger(CollectAbleHandleJob.class);

    @Override
    public String collect(List<EndpointExecuteResult> executeResults) throws RelayJobException {

        logger.debug("开始收集任务结果");

        logger.debug("执行结果列表：{}", executeResults);

        return "ok";
    }

    @Override
    public String execute(String param) throws RelayJobException {
        logger.debug("开始执行任务, 执行参数: {}", param);

        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        logger.debug("任务执行完成");
        return "success > " + Thread.currentThread().getName();
    }

}

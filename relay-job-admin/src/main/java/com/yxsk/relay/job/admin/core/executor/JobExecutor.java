package com.yxsk.relay.job.admin.core.executor;

import com.yxsk.relay.job.admin.core.executor.context.JobExecuteContext;

/**
 * @Author 11376
 * @CreaTime 2019/6/28 9:28
 * @Description
 */
public interface JobExecutor <T extends JobExecuteContext>{

    void execute(T context);

}

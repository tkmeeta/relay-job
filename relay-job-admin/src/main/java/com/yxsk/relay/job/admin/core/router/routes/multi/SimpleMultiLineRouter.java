package com.yxsk.relay.job.admin.core.router.routes.multi;

import com.yxsk.relay.job.admin.core.router.AutoUpdateRouter;
import com.yxsk.relay.job.admin.core.router.endpoint.EndpointRouterInfo;
import com.yxsk.relay.job.admin.core.schedule.quartz.context.TriggerContext;
import com.yxsk.relay.job.component.admin.registry.Steward;

import java.util.List;

/**
 * @Author 11376
 * @CreaTime 2019/7/19 10:32
 * @Description
 */
public class SimpleMultiLineRouter extends AutoUpdateRouter {

    public SimpleMultiLineRouter(Steward steward) {
        super(steward);
    }

    @Override
    public List<EndpointRouterInfo> route(TriggerContext context) {
        return super.getEndpointList(context.getJobConfig().getAppName());
    }

}

package com.yxsk.relay.job.admin.core.trigger.quartz;

import com.yxsk.relay.job.admin.core.router.selector.RouterDecisionMaker;
import com.yxsk.relay.job.admin.core.schedule.JobTriggerInfo;
import com.yxsk.relay.job.admin.core.schedule.constant.JobDataConstant;
import com.yxsk.relay.job.admin.core.schedule.lifecycle.manage.TaskTriggerManager;
import com.yxsk.relay.job.admin.core.schedule.quartz.context.TriggerContext;
import com.yxsk.relay.job.admin.core.trigger.JobTrigger;
import com.yxsk.relay.job.admin.data.entity.vo.QuartzJobInfo;
import com.yxsk.relay.job.admin.data.service.JobDetailService;
import com.yxsk.relay.job.admin.utils.IdUtils;
import com.yxsk.relay.job.component.admin.utils.SpringBeanUtils;
import com.yxsk.relay.job.component.common.utils.DateUtils;
import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobKey;
import org.springframework.util.Assert;
import org.springframework.util.CollectionUtils;

/**
 * @Description
 * @Author 11376
 * @CreateTime 2019/8/26 9:42
 */
public abstract class QuartzJobAdapter implements Job {

    protected JobTrigger jobTrigger;

    protected JobExecutionContext quartzExecutionContext;

    protected abstract void setJobTrigger();

    public void execute(JobExecutionContext jobExecutionContext) {
        this.quartzExecutionContext = jobExecutionContext;
        this.setJobTrigger();
        Assert.notNull(this.jobTrigger, "Job trigger is null.");

        // 触发任务
        this.jobTrigger.trigger();
    }

    protected TaskTriggerManager createTriggerManager() {
        return (TaskTriggerManager) this.quartzExecutionContext.getMergedJobDataMap().get(JobDataConstant.TASK_EXECUTE_MANAGER);
    }

    protected TriggerContext createTriggerContext() {
        // 初始化任务执行上下文信息
        JobKey jobKey = this.quartzExecutionContext.getJobDetail().getKey();
        JobDetailService jobDetailService = SpringBeanUtils.getBean(JobDetailService.class);

        // 初始化触发信息
        QuartzJobInfo jobConfig = jobDetailService.getJobConfig(jobKey.getName());
        JobTriggerInfo triggerInfo = getTriggerInfo(jobConfig);
        return new TriggerContext(jobConfig, triggerInfo);
    }

    private JobTriggerInfo getTriggerInfo(QuartzJobInfo jobConfig) {
        JobTriggerInfo triggerInfo = new JobTriggerInfo();
        triggerInfo.setTriggerId(IdUtils.nextId());
        triggerInfo.setJobId(jobConfig.getId());
        triggerInfo.setBeginTime(DateUtils.getCurrentDate());
        JobDataMap dataMap = this.quartzExecutionContext.getMergedJobDataMap();
        Object param = null;
        String childTriggerId = null;
        if (!CollectionUtils.isEmpty(dataMap)) {
            param = dataMap.get(JobDataConstant.CUSTOM_TRIGGER_PARAM_KEY);
            Object o = dataMap.get(JobDataConstant.EXECUTE_LOG_CHILD_TRIGGER_ID);
            if (o != null) {
                childTriggerId = o.toString();
            }
        }
        // 优先使用执行上下文的执行参数
        triggerInfo.setTriggerParam(param == null ? jobConfig.getExecuteParam() : param.toString());
        triggerInfo.setExecuteLogId(childTriggerId);
        return triggerInfo;
    }

    protected RouterDecisionMaker createRouterDecisionMaker() {
        return SpringBeanUtils.getBean(RouterDecisionMaker.class);
    }

}

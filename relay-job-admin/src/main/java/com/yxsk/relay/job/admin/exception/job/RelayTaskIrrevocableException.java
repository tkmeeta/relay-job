package com.yxsk.relay.job.admin.exception.job;

import com.yxsk.relay.job.component.admin.exception.RelayJobAdminException;

/**
 * @Author 11376
 * @CreaTime 2019/6/6 20:20
 * @Description 不可取消任务异常
 */
public class RelayTaskIrrevocableException extends RelayJobAdminException {

    public RelayTaskIrrevocableException(String message) {
        super(message);
    }

}

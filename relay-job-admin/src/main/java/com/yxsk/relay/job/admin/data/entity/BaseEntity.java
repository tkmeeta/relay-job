package com.yxsk.relay.job.admin.data.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import java.io.Serializable;
import java.util.Date;

/**
 * @Author 11376
 * @CreaTime 2019/6/6 20:32
 * @Description
 */
@Getter
@Setter
@ToString
@MappedSuperclass
public abstract class BaseEntity implements Serializable {

    @Id
    @Column(name = "ID")
    protected String id;

    // 创建时间
    @Column(name = "CREATE_TIME")
    protected Date createTime;

    // 创建人
    @Column(name = "CREATE_BY")
    protected String createBy;

    // 更新时间
    @Column(name = "UPDATE_TIME")
    protected Date updateTime;

    // 更新人
    @Column(name = "UPDATE_BY")
    protected String updateBy;

    // 删除标记
    @Column(name = "DEL_FLAG")
    protected String delFlag;

    @AllArgsConstructor
    @Getter
    public enum DelFlagEnum {

        NORMAL("0"),
        DELETED("1");

        private String flag;

        public boolean isDelete(String flag) {
            if (DelFlagEnum.NORMAL.flag.equals(flag)) {
                return false;
            } else if (DelFlagEnum.DELETED.flag.equals(flag)) {
                return true;
            } else {
                throw new IllegalArgumentException("删除标记错误: " + flag);
            }
        }

    }

}

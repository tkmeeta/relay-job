package com.yxsk.relay.job.admin.core.executor.context;

import com.yxsk.relay.job.admin.core.schedule.JobTriggerInfo;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * @Author 11376
 * @CreaTime 2019/6/23 0:01
 * @Description
 */
@Getter
@Setter
@ToString
public abstract class JobExecuteContext {

    protected JobTriggerInfo triggerInfo;

}

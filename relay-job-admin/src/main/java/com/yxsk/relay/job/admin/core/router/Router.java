package com.yxsk.relay.job.admin.core.router;

import com.yxsk.relay.job.admin.core.router.endpoint.EndpointRouterInfo;
import com.yxsk.relay.job.admin.core.schedule.quartz.context.TriggerContext;

import java.util.List;

/**
 * @Author 11376
 * @CreaTime 2019/6/7 21:39
 * @Description 路由器
 */
public interface Router {

    /**
     * @param context
     * @Author 11376
     * @Description 路由选择执行任务的端点列表
     * @CreateTime 2019/6/7 21:59
     * @Return
     */
    List<EndpointRouterInfo> route(TriggerContext context);

}

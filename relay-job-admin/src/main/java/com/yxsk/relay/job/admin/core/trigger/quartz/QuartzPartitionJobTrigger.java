package com.yxsk.relay.job.admin.core.trigger.quartz;

import com.yxsk.relay.job.admin.core.trigger.PartitionDynamicTrigger;

/**
 * @Description
 * @Author 11376
 * @CreateTime 2019/8/27 8:54
 */
public class QuartzPartitionJobTrigger extends QuartzJobAdapter {

    @Override
    protected void setJobTrigger() {
        this.jobTrigger = new PartitionDynamicTrigger(this.createTriggerManager(), this.createTriggerContext(), this.createRouterDecisionMaker());
    }

}

package com.yxsk.relay.job.admin.data.service;

import com.yxsk.relay.job.admin.data.entity.JobExecuteLog;
import com.yxsk.relay.job.admin.data.repository.JobExecuteLogRepository;
import com.yxsk.relay.job.component.common.vo.ExecuteResult;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @Author 11376
 * @CreaTime 2019/6/22 11:42
 * @Description
 */
@Service
@Transactional
public class JobExecuteLogService extends AbstractService<JobExecuteLog, JobExecuteLogRepository> {

    @Transactional(propagation = Propagation.SUPPORTS)
    public List<JobExecuteLog> findAllExecutingLog() {
        return this.repository.findByExecuteStatus(ExecuteResult.ExecuteResultCode.EXECUTING.getCode());
    }

    @Transactional(propagation = Propagation.SUPPORTS)
    public List<JobExecuteLog> findExecutingLog(String jobId) {
        List<String> status = Arrays.asList(ExecuteResult.ExecuteResultCode.EXECUTING.getCode());
        return this.repository.findExecuteLogWithStatus(jobId, status);
    }

    @Transactional(propagation = Propagation.SUPPORTS)
    public List<JobExecuteLog> findByTriggerId(String triggerId) {
        return this.repository.findByTriggerLogId(triggerId);
    }

    @Transactional(propagation = Propagation.SUPPORTS)
    public List<JobExecuteLog> findData(Date startTime, Date endTime, List<String> jobIds) {
        return this.repository.findData(startTime, endTime, jobIds);
    }

    @Transactional(propagation = Propagation.SUPPORTS)
    public Page<Map> findPageByCondition(String appId, String jobId, Date startTime, Date endTime, PageRequest page) {
        return this.repository.findPageByCondition(appId, jobId, startTime, endTime, page);
    }

    public void updateByIdAndExecuteStatus(JobExecuteLog executeLog, String code) {
        this.repository.updateByIdAndExecuteStatus(executeLog, code);
    }
}

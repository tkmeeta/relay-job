package com.yxsk.relay.job.admin.config.registry;

import com.yxsk.relay.job.admin.aop.CompressMessageAspectAware;
import com.yxsk.relay.job.admin.config.EnableCompressMessage;
import com.yxsk.relay.job.component.admin.registry.Steward;
import com.yxsk.relay.job.component.admin.registry.StewardConfig;
import com.yxsk.relay.job.component.common.thread.observe.ObservableThread;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * @Author 11376
 * @CreaTime 2019/6/15 10:54
 * @Description 注册中心自动配置
 */
@Configuration
@ComponentScan(basePackages = {"com.yxsk.relay.job.component.admin.facade.restful"})
@EnableCompressMessage({"com.yxsk.relay.job.component.admin.facade.restful"})
public class StewardAutoConfiguration extends CompressMessageAspectAware {

    @ConditionalOnMissingBean(StewardConfig.class)
    @Bean
    public StewardConfig stewardConfig() {
        return new StewardConfig();
    }

    @ConditionalOnMissingBean(Steward.class)
    @Bean(initMethod = "init", destroyMethod = "shutdown")
    public Steward steward() {
        return new Steward(stewardConfig());
    }

    @ConditionalOnBean(name = "steward")
    @Bean(initMethod = "start")
    public Thread stewardTask() {
        return new ObservableThread<>(steward());
    }

}

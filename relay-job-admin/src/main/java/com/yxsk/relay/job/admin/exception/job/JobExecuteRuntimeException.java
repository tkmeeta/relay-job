package com.yxsk.relay.job.admin.exception.job;

import com.yxsk.relay.job.component.admin.exception.RelayJobAdminRuntimeException;

/**
 * @Description
 * @Author 11376
 * @CreateTime 2019/9/11 9:17
 */
public class JobExecuteRuntimeException extends RelayJobAdminRuntimeException {
    public JobExecuteRuntimeException(String cause) {
        super(cause);
    }

    public JobExecuteRuntimeException(Throwable e) {
        super(e);
    }

    public JobExecuteRuntimeException(String cause, Throwable e) {
        super(cause, e);
    }
}

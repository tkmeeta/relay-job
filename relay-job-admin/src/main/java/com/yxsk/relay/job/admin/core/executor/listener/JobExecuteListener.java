package com.yxsk.relay.job.admin.core.executor.listener;

import com.yxsk.relay.job.admin.core.executor.context.JobExecuteContext;

/**
 * @Author 11376
 * @CreaTime 2019/6/22 23:13
 * @Description 任务执行监听
 */
public interface JobExecuteListener<T extends JobExecuteContext> {

    void executeBefore(T context);

    void onFinish(T context);

    void onError(T context, Throwable e);

    void executeAfter(T context);

    class DefaultJobExecuteListener implements JobExecuteListener {
        @Override
        public void executeBefore(JobExecuteContext context) {

        }

        @Override
        public void onFinish(JobExecuteContext context) {

        }

        @Override
        public void onError(JobExecuteContext context, Throwable e) {

        }

        @Override
        public void executeAfter(JobExecuteContext context) {

        }
    }

}

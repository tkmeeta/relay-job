package com.yxsk.relay.job.admin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RelayJobAdminApplication {

    public static void main(String[] args) {
        SpringApplication.run(RelayJobAdminApplication.class, args);
    }

}

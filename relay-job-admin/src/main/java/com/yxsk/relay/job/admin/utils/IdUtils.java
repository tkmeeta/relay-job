package com.yxsk.relay.job.admin.utils;

import lombok.experimental.UtilityClass;

import java.util.UUID;

/**
 * @Author 11376
 * @CreaTime 2019/6/22 11:48
 * @Description
 */
@UtilityClass
public class IdUtils {

    public String nextId() {
        return UUID.randomUUID().toString().replace("-", "");
    }

}

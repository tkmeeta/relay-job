package com.yxsk.relay.job.admin.controller.dto.app;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.NotEmpty;
import java.io.Serializable;

/**
 * @Author 11376
 * @CreaTime 2019/6/22 15:54
 * @Description
 */
@Getter
@Setter
@ToString
public class AppAddRequestDto implements Serializable {

    // 应用名称
    @NotEmpty(message = "应用名称不能为空")
    private String appName;
    // 描述
    private String description;
    /**
     * 认证方式 {@link com.yxsk.relay.job.admin.data.entity.JobApps.AuthType}
     */
    @NotEmpty(message = "注册认证类型不能为空")
    private String authType;
    // 授权IP列表, 使用英文逗号分隔
    private String authIps;

}

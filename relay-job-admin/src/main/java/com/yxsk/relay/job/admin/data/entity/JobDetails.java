package com.yxsk.relay.job.admin.data.entity;

import com.yxsk.relay.job.admin.core.schedule.quartz.vo.JobConfig;
import com.yxsk.relay.job.admin.core.trigger.quartz.QuartzAsyncJobTrigger;
import com.yxsk.relay.job.admin.core.trigger.quartz.QuartzJobTrigger;
import com.yxsk.relay.job.admin.core.trigger.quartz.QuartzPartitionJobTrigger;
import com.yxsk.relay.job.component.common.exception.RelayJobRuntimeException;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.quartz.Job;
import org.springframework.util.Assert;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.text.MessageFormat;

/**
 * @Author 11376
 * @CreaTime 2019/6/6 14:34
 * @Description
 */
@Getter
@Setter
@ToString(callSuper = true)
@Entity
@Table(name = "RELAY_JOB_DETAILS")
public class JobDetails extends BaseEntity {

    // 所属应用 Id
    @Column(name = "APP_ID")
    private String appId;
    @Column(name = "JOB_NAME")
    private String jobName;
    // 任务处理器名称
    @Column(name = "HANDLER_NAME")
    private String handlerName;
    // 任务触发器类名
    @Column(name = "TRIGGER_CLASS_NAME")
    private String triggerClassName;
    // cron表达式
    @Column(name = "CRON_EXPRESSION")
    private String cronExpression;
    /**
     * 执行模式 {@link JobConfig.ExecuteModel}
     */
    @Column(name = "EXECUTE_MODEL")
    private String executeModel;
    /**
     * 执行路由器策略 {@link JobConfig.RouteStrategy}
     */
    @Column(name = "ROUTE_STRATEGY")
    private String routeStrategy;
    /**
     * 阻塞策略 {@link JobConfig.BlockStrategy}
     */
    @Column(name = "BLOCK_STRATEGY")
    private String blockStrategy;
    // 描述
    @Column(name = "DESCRIPTION")
    private String description;
    // 执行参数
    @Column(name = "TRIGGER_PARAM")
    private String triggerParam;
    // 任务执行超时时间, 单位:秒
    @Column(name = "EXECUTE_TIMEOUT")
    private Integer executeTimeout;
    // 失败重试次数
    @Column(name = "FAIL_RETRY_TIMES")
    private Integer failRetryTimes;
    /**
     * 任务状态 {@link JobConfig.JobStatus}
     */
    @Column(name = "STATUS")
    private String status;

    @AllArgsConstructor
    @Getter
    public enum TriggerClassesEnum {
        COMMON("COMMON", "同步触发器", QuartzJobTrigger.class),
        ASYNC_TRIGGER("ASYNC_TRIGGER", "异步触发器", QuartzAsyncJobTrigger.class),
        PARTITION_TASK_TRIGGER("PARTITION_TASK_TRIGGER", "分片触发器", QuartzPartitionJobTrigger.class);

        private String triggerName;
        private String name;
        private Class<? extends Job> triggerClass;

        public static TriggerClassesEnum getTrigger(String triggerName) {
            Assert.hasLength(triggerName, "Trigger name not be null");
            TriggerClassesEnum[] values = TriggerClassesEnum.values();
            for (TriggerClassesEnum value : values) {
                if (value.triggerName.equals(triggerName)) {
                    return value;
                }
            }
            throw new RelayJobRuntimeException(MessageFormat.format("Not found trigger information of trigger name is [{0}]", triggerName));
        }

        public static TriggerClassesEnum getTriggerByClassName(String triggerClassName) {
            Assert.hasLength(triggerClassName, "Trigger name not be null");
            TriggerClassesEnum[] values = TriggerClassesEnum.values();
            for (TriggerClassesEnum value : values) {
                if (value.triggerClass.getName().equals(triggerClassName)) {
                    return value;
                }
            }
            throw new RelayJobRuntimeException(MessageFormat.format("Not found trigger information of trigger class is [{0}]", triggerClassName));

        }
    }

}

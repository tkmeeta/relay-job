package com.yxsk.relay.job.admin.controller;

import com.yxsk.relay.job.admin.controller.dto.app.AppAddRequestDto;
import com.yxsk.relay.job.admin.data.entity.JobApps;
import com.yxsk.relay.job.admin.data.service.JobAppsService;
import com.yxsk.relay.job.component.common.protocol.message.base.ResultResponse;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.BeanUtils;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.Valid;

/**
 * @Author 11376
 * @CreaTime 2019/6/22 15:51
 * @Description
 */
@Api("任务应用配置接口")
@RequestMapping("/app")
@RestController
public class JobAppController {

    @Resource
    private JobAppsService appsService;

    @ApiOperation("添加应用")
    @PostMapping("/add")
    public ResultResponse addApp(@RequestBody @Valid AppAddRequestDto addRequestDto) {
        JobApps apps = new JobApps();
        BeanUtils.copyProperties(addRequestDto, apps);
        apps.setAuthType(JobApps.AuthType.getAuthType(addRequestDto.getAuthType()).getType());
        apps.setStatus(JobApps.AppStatus.NORMAL.getStatus());
        this.appsService.addEntity(apps);
        return ResultResponse.ok("success");
    }

    @ApiOperation("暂停应用")
    @ApiImplicitParam(name = "appId", value = "应用Id", required = true)
    @GetMapping("/pause")
    public ResultResponse pause(@RequestParam("appId") String appId) {
        this.appsService.pause(appId);
        return ResultResponse.ok("success");
    }

    @ApiOperation("启用应用")
    @ApiImplicitParam(name = "appId", value = "应用Id", required = true)
    @GetMapping("/resume")
    public ResultResponse resume(@RequestParam("appId") String appId) {
        this.appsService.resume(appId);
        return ResultResponse.ok("success");
    }

    @ApiOperation("删除应用")
    @ApiImplicitParam(name = "appId", value = "应用Id", required = true)
    @GetMapping("/delete")
    public ResultResponse delete(@RequestParam("appId") String appId) {
        this.appsService.deleteApp(appId);
        return ResultResponse.ok("success");
    }
}

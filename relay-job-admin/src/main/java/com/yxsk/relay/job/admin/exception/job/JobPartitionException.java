package com.yxsk.relay.job.admin.exception.job;

import com.yxsk.relay.job.component.admin.exception.RelayJobAdminRuntimeException;

/**
 * @Author 11376
 * @CreaTime 2019/6/29 22:00
 * @Description
 */
public class JobPartitionException extends RelayJobAdminRuntimeException {
    public JobPartitionException(String cause) {
        super(cause);
    }

    public JobPartitionException(Throwable e) {
        super(e);
    }

    public JobPartitionException(String cause, Throwable e) {
        super(cause, e);
    }
}

package com.yxsk.relay.job.admin.config.registry;

import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

/**
 * @Author 11376
 * @CreaTime 2019/6/15 11:32
 * @Description
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Import(StewardAutoConfiguration.class)
public @interface EnableRelayJobSteward {
}

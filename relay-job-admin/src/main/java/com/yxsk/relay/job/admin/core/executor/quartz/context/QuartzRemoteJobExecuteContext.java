package com.yxsk.relay.job.admin.core.executor.quartz.context;

import com.yxsk.relay.job.admin.core.router.endpoint.EndpointRouterInfo;
import com.yxsk.relay.job.component.common.protocol.message.base.ResultResponse;
import com.yxsk.relay.job.component.common.protocol.message.execute.JobExecuteRequest;
import com.yxsk.relay.job.component.common.protocol.message.execute.JobExecuteResponse;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * @Author 11376
 * @CreaTime 2019/6/28 9:32
 * @Description
 */
@Getter
@Setter
@ToString
public class QuartzRemoteJobExecuteContext extends QuartzJobExecuteContext {

    // 任务执行 Id
    protected String serialNo;

    protected JobExecuteRequest request;

    protected ResultResponse<JobExecuteResponse> response;

    protected EndpointRouterInfo endpointRouterInfo;

    // 重试次数
    protected Integer retryTimes;

}

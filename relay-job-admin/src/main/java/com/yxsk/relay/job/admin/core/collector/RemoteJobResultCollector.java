package com.yxsk.relay.job.admin.core.collector;

import com.yxsk.relay.job.admin.core.remote.RemoteCallerSelector;
import com.yxsk.relay.job.component.admin.utils.SpringBeanUtils;
import com.yxsk.relay.job.component.common.constant.NetProtocol;
import com.yxsk.relay.job.component.common.protocol.caller.RemoteCaller;

/**
 * @Author 11376
 * @CreaTime 2019/8/5 22:55
 * @Description
 */
public abstract class RemoteJobResultCollector implements JobResultCollector {

    protected RemoteCaller remoteCaller(NetProtocol netProtocol) {
        return SpringBeanUtils.getBean(RemoteCallerSelector.class).select(netProtocol);
    }

}

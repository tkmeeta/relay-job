package com.yxsk.relay.job.admin.core.trigger.quartz;

import com.yxsk.relay.job.admin.core.trigger.DynamicTrigger;

/**
 * @Author 11376
 * @CreaTime 2019/6/28 9:06
 * @Description
 */
public class QuartzJobTrigger extends QuartzJobAdapter {

    @Override
    protected void setJobTrigger() {
        this.jobTrigger = new DynamicTrigger(this.createTriggerManager(), this.createTriggerContext(), this.createRouterDecisionMaker());
    }

}

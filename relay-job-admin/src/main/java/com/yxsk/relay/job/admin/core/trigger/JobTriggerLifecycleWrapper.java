package com.yxsk.relay.job.admin.core.trigger;


import com.yxsk.relay.job.component.common.exception.RelayJobRuntimeException;
import com.yxsk.relay.job.component.common.vo.TriggerResult;

/**
 * @Author 11376
 * @CreaTime 2019/6/28 9:11
 * @Description
 */
public abstract class JobTriggerLifecycleWrapper<R extends TriggerResult> implements JobTrigger<R> {

    public void onStart() {
    }

    public abstract R executeJob() throws Exception;

    public void onFinish(R result) {
    }

    public void onError(Exception e) {
    }

    @Override
    public final R trigger() {
        this.onStart();
        try {
            R result = this.executeJob();
            onFinish(result);
            return result;
        } catch (Exception e) {
            this.onError(e);
            throw new RelayJobRuntimeException(e);
        }
    }

}

package com.yxsk.relay.job.admin.core.collector;

import com.yxsk.relay.job.admin.core.schedule.JobTriggerInfo;

/**
 * @Author 11376
 * @CreaTime 2019/8/5 22:53
 * @Description
 */
public interface JobResultCollector {

    void collect(JobTriggerInfo triggerInfo);

}

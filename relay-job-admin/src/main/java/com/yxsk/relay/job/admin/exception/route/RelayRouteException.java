package com.yxsk.relay.job.admin.exception.route;

import com.yxsk.relay.job.component.admin.exception.RelayJobAdminRuntimeException;

/**
 * @Author 11376
 * @CreaTime 2019/6/15 13:29
 * @Description
 */
public class RelayRouteException extends RelayJobAdminRuntimeException {
    public RelayRouteException(String cause) {
        super(cause);
    }

    public RelayRouteException(Throwable e) {
        super(e);
    }

    public RelayRouteException(String cause, Throwable e) {
        super(cause, e);
    }
}

package com.yxsk.relay.job.admin.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.task.AsyncTaskExecutor;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

/**
 * @Author 11376
 * @CreaTime 2019/6/2 20:52
 * @Description
 */
@Configuration
public class AsyncTaskConfig {

    /**
     * @param
     * @Author 11376
     * @Description 请求追踪的异步任务执行器
     * @CreateTime 2019/6/2 20:53
     * @Return
     */
    @Bean
    public AsyncTaskExecutor traceRequestAsyncTaskExecutor() {
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setThreadNamePrefix("Async-Executor[Auto Trace Request Context]");
        executor.setMaxPoolSize(Runtime.getRuntime().availableProcessors());
        return executor;
    }

}

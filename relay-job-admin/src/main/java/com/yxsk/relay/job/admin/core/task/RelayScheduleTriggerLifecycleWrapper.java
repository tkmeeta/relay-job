package com.yxsk.relay.job.admin.core.task;

import com.yxsk.relay.job.admin.core.schedule.quartz.vo.JobConfig;
import com.yxsk.relay.job.component.common.exception.RelayJobRuntimeException;
import com.yxsk.relay.job.component.common.vo.TriggerResult;

/**
 * @Author 11376
 * @CreaTime 2019/6/5 14:41
 * @Description
 */
public abstract class RelayScheduleTriggerLifecycleWrapper<R extends TriggerResult> extends RelayJobTrigger<R> {

    protected RelayScheduleTriggerLifecycleWrapper(JobConfig jobConfig) {
        super(jobConfig);
    }

    public void onStart() {
    }

    public abstract R executeTask() throws Exception;

    public void onFinish(R result) {
    }

    public void onError(Exception e) {
    }

    @Override
    public final R execute() {
        this.onStart();
        try {
            R result = this.executeTask();
            onFinish(result);
            return result;
        } catch (Exception e) {
            this.onError(e);
            throw new RelayJobRuntimeException(e);
        }
    }
}

package com.yxsk.relay.job.admin.core.schedule;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.Date;

/**
 * @Author 11376
 * @CreaTime 2019/6/22 17:31
 * @Description
 */
@Getter
@Setter
@ToString
public class JobTriggerInfo {

    private String triggerId;

    private String executeLogId;

    private String jobId;

    private String triggerParam;

    private String triggerResultCode;

    private String triggerResultMessage;

    private Date beginTime;

    private Date endTime;

}

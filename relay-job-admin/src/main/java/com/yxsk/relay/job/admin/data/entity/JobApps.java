package com.yxsk.relay.job.admin.data.entity;

import com.yxsk.relay.job.component.common.exception.RelayJobRuntimeException;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.util.Assert;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.text.MessageFormat;

/**
 * @Author 11376
 * @Description 应用
 * @CreateTime 2019/6/21 23:19
 */
@Getter
@Setter
@ToString(callSuper = true)
@Entity
@Table(name = "RELAY_JOB_APPS")
public class JobApps extends BaseEntity {

    @Column
    private String name;
    // 应用名称
    @Column(name = "APP_NAME")
    private String appName;
    // 描述
    @Column(name = "DESCRIPTION")
    private String description;
    // 认证方式, token-token认证, ip-指定IP认证, none-无需认证
    @Column(name = "AUTH_TYPE")
    private String authType;
    // 授权IP列表, 使用英文逗号分隔
    @Column(name = "AUTH_IPS")
    private String authIps;
    @Column(name = "STATUS")
    private String status;

    @AllArgsConstructor
    @Getter
    public enum AuthType {

        TOKEN("TOKEN"),
        IP("IP"),
        NONE("NONE");

        private String type;

        public static AuthType getAuthType(String type) {
            Assert.hasLength(type, "Auth type not be null.");
            AuthType[] values = AuthType.values();
            for (AuthType value : values) {
                if (value.type.equals(type)) {
                    return value;
                }
            }
            throw new RelayJobRuntimeException(MessageFormat.format("Not found [{0}] auth type.", type));
        }
    }

    @AllArgsConstructor
    @Getter
    public enum AppStatus {
        NORMAL("NORMAL"),
        PAUSE("PAUSE");

        private String status;
    }

}

package com.yxsk.relay.job.admin.core.executor.quartz.listener;

import com.yxsk.relay.job.admin.core.executor.listener.JobExecuteListener;
import com.yxsk.relay.job.admin.core.executor.quartz.context.QuartzRemoteJobExecuteContext;
import com.yxsk.relay.job.admin.data.entity.JobExecuteLog;
import com.yxsk.relay.job.admin.data.service.JobExecuteLogService;
import com.yxsk.relay.job.component.common.exception.RelayJobRuntimeException;
import com.yxsk.relay.job.component.common.protocol.message.execute.JobExecuteResponse;
import com.yxsk.relay.job.component.common.utils.DateUtils;
import com.yxsk.relay.job.component.common.utils.ExceptionUtils;
import com.yxsk.relay.job.component.common.vo.Endpoint;
import com.yxsk.relay.job.component.common.vo.ExecuteResult;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * @Author 11376
 * @CreaTime 2019/6/22 23:55
 * @Description quartz远程任务执行监听
 */
@Slf4j
@AllArgsConstructor
public class QuartzRemoteJobExecuteListener implements JobExecuteListener<QuartzRemoteJobExecuteContext> {

    protected JobExecuteLogService executeLogService;

    @Override
    public void executeBefore(QuartzRemoteJobExecuteContext context) {
        JobExecuteLog executeLog = new JobExecuteLog();
        executeLog.setId(context.getSerialNo());
        executeLog.setTriggerLogId(context.getTriggerInfo().getTriggerId());
        Endpoint endpoint = context.getEndpointRouterInfo().getEndpoint();
        executeLog.setAuthToken(endpoint.getAuthToken());
        executeLog.setBeginTime(DateUtils.getCurrentDate());
        executeLog.setEndpointIp(endpoint.getHost());
        executeLog.setEndpointPort(endpoint.getPort());
        executeLog.setNetProtocol(endpoint.getProtocol().getProtocol());
        executeLog.setExecuteStatus(ExecuteResult.ExecuteResultCode.EXECUTING.getCode());
        executeLog.setExecuteParam(context.getTriggerInfo().getTriggerParam());
        this.executeLogService.addEntity(executeLog);
    }

    @Override
    public void onFinish(QuartzRemoteJobExecuteContext context) {
        JobExecuteLog executeLog = this.executeLogService.findById(context.getRequest().getSerialNo());
        if (executeLog == null) {
            throw new RelayJobRuntimeException("未找到任务执行日志");
        }
        JobExecuteResponse executeResponse = context.getResponse().getData();
        executeLog.setEndTime(executeResponse.getRespTime());
        executeLog.setExecuteStatus(executeResponse.getExecuteStatus());
        executeLog.setExecuteResult(executeResponse.getResult());
        executeLog.setExecuteErrorMessage(executeResponse.getErrorMsg());
        this.executeLogService.updateById(executeLog);
    }

    @Override
    public void onError(QuartzRemoteJobExecuteContext context, Throwable e) {
        JobExecuteLog executeLog = this.executeLogService.findById(context.getRequest().getSerialNo());
        if (executeLog == null) {
            throw new RelayJobRuntimeException("未找到任务执行日志");
        }
        try {
            executeLog.setEndTime(context.getResponse().getData().getRespTime());
        } catch (Exception e1) {
            executeLog.setEndTime(DateUtils.getCurrentDate());
        }
        executeLog.setExecuteStatus(ExecuteResult.ExecuteResultCode.ERROR.getCode());
        executeLog.setExecuteErrorMessage(ExceptionUtils.getStackInfo(e));
        this.executeLogService.updateById(executeLog);
    }

    @Override
    public void executeAfter(QuartzRemoteJobExecuteContext context) {
        log.debug("Job execute complete.");
    }

}

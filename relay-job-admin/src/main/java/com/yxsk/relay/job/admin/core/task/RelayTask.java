package com.yxsk.relay.job.admin.core.task;

import com.yxsk.relay.job.component.common.vo.TriggerResult;

import java.util.concurrent.Callable;

/**
 * @Author 11376
 * @CreaTime 2019/6/5 14:04
 * @Description
 */
public abstract class RelayTask<R extends TriggerResult> implements Runnable, Callable<R> {

    /**
     * @Author 11376
     * @Description 子类必须实现自己的无参构造方法
     * @CreateTime 2019/6/5 14:22
     */
    protected RelayTask() {
    }

    /**
     * @Author 11376
     * @Description 执行任务
     * @CreateTime 2019/6/5 14:09
     * @Return
     */
    public abstract R execute();

    @Override
    public void run() {
        // 执行任务
        this.execute();
    }

    @Override
    public R call() throws Exception {
        return this.execute();
    }

}

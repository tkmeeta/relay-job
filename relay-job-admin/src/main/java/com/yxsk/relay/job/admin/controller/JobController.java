package com.yxsk.relay.job.admin.controller;

import com.yxsk.relay.job.admin.controller.dto.job.AddJobRequestDto;
import com.yxsk.relay.job.admin.controller.dto.job.UpdateJobRequestDto;
import com.yxsk.relay.job.admin.core.schedule.quartz.vo.JobConfig;
import com.yxsk.relay.job.admin.data.entity.JobDetails;
import com.yxsk.relay.job.admin.data.service.JobDetailService;
import com.yxsk.relay.job.component.common.protocol.message.base.ResultResponse;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * @Author 11376
 * @CreaTime 2019/6/6 14:14
 * @Description
 */
@Api("Job管理接口")
@RequestMapping("/job")
@RestController
public class JobController {

    @Autowired
    private JobDetailService jobDetailService;

    @ApiOperation("添加任务")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "appId", value = "所属应用Id", required = true),
            @ApiImplicitParam(name = "handlerName", value = "处理器名称", required = true),
            @ApiImplicitParam(name = "triggerName", value = "触发器名称", required = true),
            @ApiImplicitParam(name = "cronExpression", value = "cron表达式"),
            @ApiImplicitParam(name = "executeModel", value = "任务执行模式", required = true),
            @ApiImplicitParam(name = "routeStrategy", value = "路由策略", required = true),
            @ApiImplicitParam(name = "blockStrategy", value = "阻塞策略", required = true),
            @ApiImplicitParam(name = "description", value = "任务描述"),
            @ApiImplicitParam(name = "triggerParam", value = "执行参数"),
            @ApiImplicitParam(name = "executeTimeout", value = "执行超时时间，单位秒"),
            @ApiImplicitParam(name = "failRetryTimes", value = "失败重试次数")
    })
    @PostMapping("/add")
    public ResultResponse add(@RequestBody @Valid AddJobRequestDto requestDto) {
        JobDetails jobDetail = new JobDetails();
        BeanUtils.copyProperties(requestDto, jobDetail);
        jobDetail.setBlockStrategy(JobConfig.BlockStrategy.getBlockStrategy(requestDto.getBlockStrategy()).getStrategy());
        jobDetail.setExecuteModel(JobConfig.ExecuteModel.getExecuteModel(requestDto.getExecuteModel()).getModel());
        jobDetail.setRouteStrategy(JobConfig.RouteStrategy.getStrategy(requestDto.getRouteStrategy()).getStrategy());
        jobDetail.setTriggerClassName(JobDetails.TriggerClassesEnum.getTrigger(requestDto.getTriggerName()).getTriggerClass().getName());
        jobDetail.setStatus(JobConfig.JobStatus.NORMAL.getStatus());
        this.jobDetailService.addJob(jobDetail);
        return ResultResponse.ok("success");
    }

    @ApiOperation("添加任务")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "id", required = true),
            @ApiImplicitParam(name = "appId", value = "所属应用Id", required = true),
            @ApiImplicitParam(name = "handlerName", value = "处理器名称", required = true),
            @ApiImplicitParam(name = "triggerName", value = "触发器名称", required = true),
            @ApiImplicitParam(name = "cronExpression", value = "cron表达式"),
            @ApiImplicitParam(name = "executeModel", value = "任务执行模式", required = true),
            @ApiImplicitParam(name = "routeStrategy", value = "路由策略", required = true),
            @ApiImplicitParam(name = "blockStrategy", value = "阻塞策略", required = true),
            @ApiImplicitParam(name = "description", value = "任务描述"),
            @ApiImplicitParam(name = "triggerParam", value = "执行参数"),
            @ApiImplicitParam(name = "executeTimeout", value = "执行超时时间，单位秒"),
            @ApiImplicitParam(name = "failRetryTimes", value = "失败重试次数")
    })
    @PostMapping("/update")
    public ResultResponse update(@RequestBody @Valid UpdateJobRequestDto requestDto) {
        JobDetails jobDetail = new JobDetails();
        BeanUtils.copyProperties(requestDto, jobDetail);
        jobDetail.setBlockStrategy(JobConfig.BlockStrategy.getBlockStrategy(requestDto.getBlockStrategy()).getStrategy());
        jobDetail.setExecuteModel(JobConfig.ExecuteModel.getExecuteModel(requestDto.getExecuteModel()).getModel());
        jobDetail.setRouteStrategy(JobConfig.RouteStrategy.getStrategy(requestDto.getRouteStrategy()).getStrategy());
        jobDetail.setTriggerClassName(JobDetails.TriggerClassesEnum.getTrigger(requestDto.getTriggerName()).getTriggerClass().getName());
        jobDetail.setStatus(JobConfig.JobStatus.NORMAL.getStatus());
        this.jobDetailService.updateJob(jobDetail);
        return ResultResponse.ok("success");
    }

    @ApiOperation("添加子任务")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "childrenId", value = "子任务Id, 使用英文逗号分隔", required = true),
            @ApiImplicitParam(name = "jobId", value = "任务Id", required = true)
    })
    @PostMapping("/addChildrenJob")
    public ResultResponse addChildren(@RequestParam("childrenId") String childrenId, @RequestParam("jobId") String jobId) {
        this.jobDetailService.addChildren(jobId, childrenId);
        return ResultResponse.ok("success");
    }

    @ApiOperation("删除子任务")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "children", value = "子任务Id, 使用英文逗号分隔", required = true),
            @ApiImplicitParam(name = "jobId", value = "任务Id", required = true)
    })
    @PostMapping("/deleteChildrenJob")
    public ResultResponse deleteChildren(@RequestParam("childrenId") String childrenId, @RequestParam("jobId") String jobId) {
        this.jobDetailService.deleteChildren(jobId, childrenId);
        return ResultResponse.ok("success");
    }

    @ApiOperation("暂停任务")
    @GetMapping("/pause")
    public ResultResponse pause(@RequestParam("jobId") String jobId) {
        this.jobDetailService.pauseJob(jobId);
        return ResultResponse.ok("success");
    }

    @ApiOperation("触发任务")
    @PostMapping("/trigger/{id}")
    @ResponseBody
    public ResultResponse trigger(@PathVariable("id") String jobId, @RequestParam(value = "param", required = false) String param) {
        this.jobDetailService.triggerNow(jobId, param);
        return ResultResponse.ok("任务触发成功");
    }

}

package com.yxsk.relay.job.admin.data.service;

import com.yxsk.relay.job.admin.data.entity.JobTriggerLog;
import com.yxsk.relay.job.admin.data.repository.JobTriggerLogRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @Author 11376
 * @CreaTime 2019/6/22 11:42
 * @Description
 */
@Service
public class JobTriggerLogService extends AbstractService<JobTriggerLog, JobTriggerLogRepository> {

    public Page<Map> findPageByCondition(String appId, String jobId, Date startTime, Date endTime, PageRequest page) {
        return this.repository.findPageByCondition(appId, jobId, startTime, endTime, page);
    }

    public List<JobTriggerLog> findData(Date startTime, Date endTime, List<String> jobIds) {
        return this.repository.findData(startTime, endTime, jobIds);
    }

}

package com.yxsk.relay.job.admin.config;

import org.springframework.core.annotation.AliasFor;
import org.springframework.web.bind.annotation.ControllerAdvice;

import java.lang.annotation.*;

/**
 * @Description
 * @Author 11376
 * @CreateTime 2019/8/24 14:46
 */
@Target({ElementType.TYPE, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Inherited
@Documented
@ControllerAdvice
public @interface EnableCompressMessage {

    @AliasFor(value = "basePackages", annotation = ControllerAdvice.class)
    String[] value() default "";

    @AliasFor(value = "basePackageClasses", annotation = ControllerAdvice.class)
    Class<?>[] basePackageClasses() default {};

    @AliasFor(value = "assignableTypes", annotation = ControllerAdvice.class)
    Class<?>[] assignableTypes() default {};

}

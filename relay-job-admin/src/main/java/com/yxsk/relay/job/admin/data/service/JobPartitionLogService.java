package com.yxsk.relay.job.admin.data.service;

import com.yxsk.relay.job.admin.data.entity.JobPartitionLog;
import com.yxsk.relay.job.admin.data.repository.JobPartitionLogRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @Author 11376
 * @CreaTime 2019/6/22 11:42
 * @Description
 */
@Service
@Transactional
public class JobPartitionLogService extends AbstractService<JobPartitionLog, JobPartitionLogRepository> {

    @Transactional(propagation = Propagation.SUPPORTS)
    public List<JobPartitionLog> findByTriggerLog(String triggerId) {
        return this.repository.findByTriggerLogId(triggerId);
    }

}

package com.yxsk.relay.job.admin.data.repository;

import com.yxsk.relay.job.admin.data.entity.JobDetails;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

/**
 * @Author 11376
 * @CreaTime 2019/6/22 11:05
 * @Description
 */
public interface JobDetailsRepository extends BaseRepository<JobDetails> {

    List<JobDetails> findByAppIdAndDelFlag(String jobId, String delFlag);

    @Modifying
    @Query(value = "INSERT INTO relay_job_relation (PARENT_JOB_ID, CHILD_JOB_ID) VALUES (?1, ?2)", nativeQuery = true)
    void addJobRelation(String jobId, String childId);

    @Modifying
    @Query(value = "DELETE FROM relay_job_relation WHERE PARENT_JOB_ID=?1 AND CHILD_JOB_ID=?2", nativeQuery = true)
    void deleteChildrenJob(String jobId, String id);

    @Query(value = "SELECT dr.CHILD_JOB_ID FROM relay_job_details jd, relay_job_relation dr WHERE jd.ID=dr.CHILD_JOB_ID AND dr.PARENT_JOB_ID=?1", nativeQuery = true)
    List<String> findChildrenIds(String jobId);

    @Query(value = "SELECT jd.* FROM relay_job_details jd, relay_job_relation dr WHERE jd.ID=dr.CHILD_JOB_ID AND dr.PARENT_JOB_ID=?1", nativeQuery = true)
    List<JobDetails> findChildrenJob(String jobId);

    @Modifying
    @Query(value = "DELETE FROM relay_job_relation WHERE PARENT_JOB_ID=?1", nativeQuery = true)
    void deleteAllChildren(String id);
}

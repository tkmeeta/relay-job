package com.yxsk.relay.job.admin.data.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Date;

/**
 * @Author 11376
 * @Description 任务触发日志
 * @CreateTime 2019/6/21 23:33
 */
@Getter
@Setter
@ToString(callSuper = true)
@Entity
@Table(name = "RELAY_JOB_TRIGGER_LOG")
public class JobTriggerLog extends BaseEntity {
    // 任务 Id
    @Column(name = "JOB_ID")
    private String jobId;
    // 触发该任务的父执行 Id
    @Column(name = "EXECUTE_LOG_ID")
    private String executeLogId;
    // 触发时间
    @Column(name = "TRIGGER_TIME")
    private Date triggerTime;
    // 触发参数
    @Column(name = "TRIGGER_PARAM")
    private String triggerParam;
    /**
     * 触发任务结果 {@link com.yxsk.relay.job.component.common.vo.TriggerResult.TriggerResultCode}
     */
    @Column(name = "TRIGGER_RESULT_CODE")
    private String triggerResultCode;
    // 触发任务返回结果信息
    @Column(name = "TRIGGER_RESULT_MESSAGE")
    private String triggerResultMessage;
    // 触发结束时间
    @Column(name = "TRIGGER_END_TIME")
    private Date triggerEndTime;
}

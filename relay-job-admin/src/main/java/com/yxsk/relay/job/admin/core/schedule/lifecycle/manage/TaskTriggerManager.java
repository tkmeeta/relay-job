package com.yxsk.relay.job.admin.core.schedule.lifecycle.manage;

import com.yxsk.relay.job.admin.core.executor.listener.JobExecuteListener;
import com.yxsk.relay.job.admin.core.trigger.listener.TaskTriggerListener;
import com.yxsk.relay.job.admin.core.trigger.interceptor.TaskExecuteInterceptor;

import java.util.Collections;
import java.util.List;

/**
 * @Author 11376
 * @CreaTime 2019/6/5 18:51
 * @Description 任务执行管理器
 */
public interface TaskTriggerManager {

    List<TaskExecuteInterceptor> getInterceptors();

    TaskTriggerListener getTriggerListener();

    JobExecuteListener jobExecuteListener();

    class DefaultTaskTriggerManager implements TaskTriggerManager {
        // 任务执行拦截器
        private List<TaskExecuteInterceptor> interceptors;

        private TaskTriggerListener triggerListener;

        private JobExecuteListener executeListener;

        {
            this.triggerListener = new TaskTriggerListener.DefaultTaskTriggerListener();
            this.executeListener = new JobExecuteListener.DefaultJobExecuteListener();
        }

        public List<TaskExecuteInterceptor> getInterceptors() {
            return Collections.emptyList();
        }

        @Override
        public TaskTriggerListener getTriggerListener() {
            return this.triggerListener;
        }

        @Override
        public JobExecuteListener jobExecuteListener() {
            return this.executeListener;
        }

    }

}

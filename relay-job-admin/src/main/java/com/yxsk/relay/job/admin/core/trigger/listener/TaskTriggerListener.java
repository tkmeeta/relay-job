package com.yxsk.relay.job.admin.core.trigger.listener;

import com.yxsk.relay.job.admin.core.schedule.quartz.context.TriggerContext;
import com.yxsk.relay.job.component.common.vo.TriggerResult;

/**
 * @Author 11376
 * @CreaTime 2019/6/5 20:23
 * @Description
 */
public interface TaskTriggerListener {

    void beforeTrigger(TriggerContext context);

    void afterComplete(TriggerContext context, TriggerResult triggerResult);

    void onError(TriggerContext context, Throwable e);

    class DefaultTaskTriggerListener implements TaskTriggerListener {

        @Override
        public void beforeTrigger(TriggerContext context) {

        }

        @Override
        public void afterComplete(TriggerContext context, TriggerResult triggerResult) {

        }

        @Override
        public void onError(TriggerContext context, Throwable e) {

        }
    }

}

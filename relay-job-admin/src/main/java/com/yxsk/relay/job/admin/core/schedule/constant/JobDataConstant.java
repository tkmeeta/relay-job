package com.yxsk.relay.job.admin.core.schedule.constant;

/**
 * @Author 11376
 * @CreaTime 2019/6/5 20:07
 * @Description
 */
public final class JobDataConstant {

    public static final String TASK_EXECUTE_MANAGER = "executeManager";

    public static final String TASK_ASYNC_EXECUTE_MANAGER = "asyncExecuteManager";

    public static final String CUSTOM_TRIGGER_PARAM_KEY = "custom_trigger_param";

    public static final String EXECUTE_LOG_CHILD_TRIGGER_ID = "execute_log_child_trigger_id";

}

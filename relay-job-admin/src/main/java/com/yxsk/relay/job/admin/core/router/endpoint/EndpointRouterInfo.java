package com.yxsk.relay.job.admin.core.router.endpoint;

import com.yxsk.relay.job.component.common.vo.Endpoint;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

/**
 * @Author 11376
 * @CreaTime 2019/6/7 21:48
 * @Description 远程工作单元信息
 */
@Getter
@Setter
@ToString
public abstract class EndpointRouterInfo implements Serializable, Cloneable {

    private Endpoint endpoint;

    // cpu核心数
    private Integer cpuCores;
    // jvm内存大小, 单位：MB
    private Long totalMemory;
    // jvm可用内存大小, 单位：MB
    private Long freeMemory;

    public abstract EndpointRouterInfo clone();

}

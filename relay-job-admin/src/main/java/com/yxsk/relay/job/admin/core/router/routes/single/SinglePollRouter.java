package com.yxsk.relay.job.admin.core.router.routes.single;

import com.yxsk.relay.job.admin.core.router.AutoUpdateRouter;
import com.yxsk.relay.job.admin.core.router.container.EndpointCycleList;
import com.yxsk.relay.job.admin.core.router.endpoint.EndpointRouterInfo;
import com.yxsk.relay.job.admin.core.schedule.quartz.context.TriggerContext;
import com.yxsk.relay.job.component.admin.registry.Steward;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author 11376
 * @CreaTime 2019/6/15 11:41
 * @Description 获取单端点, 轮询路由
 */
public class SinglePollRouter extends AutoUpdateRouter {

    private static SinglePollRouter ROUTER;

    public static SinglePollRouter singlePollRouter(Steward steward) {
        if (ROUTER != null) {
            ROUTER.callSteward(steward);
            return ROUTER;
        }
        synchronized (SinglePollRouter.class) {
            if (ROUTER != null) {
                return singlePollRouter(steward);
            }
            ROUTER = new SinglePollRouter(steward);
            return ROUTER;
        }
    }

    private SinglePollRouter(Steward steward) {
        super(steward);
    }

    @Override
    public List<EndpointRouterInfo> route(TriggerContext context) {
        List<EndpointRouterInfo> result = new ArrayList<>();
        EndpointCycleList list = this.endpointInfoMap.get(context.getJobConfig().getAppName());
        if (list != null) {
            EndpointRouterInfo endpoint = list.nextEndpoint();
            if (endpoint != null) {
                result.add(endpoint);
            }
        }
        return result;
    }

}

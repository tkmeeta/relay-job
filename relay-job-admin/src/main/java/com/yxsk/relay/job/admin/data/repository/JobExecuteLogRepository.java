package com.yxsk.relay.job.admin.data.repository;

import com.yxsk.relay.job.admin.data.entity.JobExecuteLog;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @Author 11376
 * @CreaTime 2019/6/22 11:05
 * @Description
 */
public interface JobExecuteLogRepository extends BaseRepository<JobExecuteLog> {

    List<JobExecuteLog> findByExecuteStatus(String executeStatus);

    @Query("SELECT el FROM JobExecuteLog el, JobTriggerLog tl WHERE tl.id=el.triggerLogId AND tl.jobId=?1 AND el.executeStatus IN (?2)")
    List<JobExecuteLog> findExecuteLogWithStatus(String jobId, List<String> status);

    List<JobExecuteLog> findByTriggerLogId(String triggerId);

    @Query(nativeQuery = true, value = "SELECT \n" +
            "  t.* \n" +
            "FROM\n" +
            "  relay_job_execute_log t,\n" +
            "  relay_job_trigger_log tl \n" +
            "WHERE t.TRIGGER_LOG_ID = tl.ID \n" +
            "  AND IF(?1 != '', t.BEGIN_TIME >=?1, 1=1) \n" +
            "  AND IF(?2 != '', t.BEGIN_TIME <=?2, 1=1) \n" +
            "  AND IF(LENGTH(TRIM(?3)) > 0, tl.JOB_ID IN (?3), 1=1) ORDER BY t.BEGIN_TIME")
    List<JobExecuteLog> findData(Date startTime, Date endTime, List<String> jobIds);

    @Query(nativeQuery = true, value = "SELECT \n" +
            "  el.*,jd.JOB_NAME,a.NAME \n" +
            "FROM\n" +
            "  relay_job_execute_log el,\n" +
            "  relay_job_trigger_log tl,\n" +
            "  relay_job_details jd,\n" +
            "  relay_job_apps a \n" +
            "  WHERE el.TRIGGER_LOG_ID=tl.ID\n" +
            "  AND tl.JOB_ID=jd.ID\n" +
            "  AND jd.APP_ID=a.ID\n" +
            "  AND IF(?1 != '', a.id=?1, 1=1)\n" +
            "  AND IF(?2 != '', jd.id=?2, 1=1)\n" +
            "  AND IF(?3 != '', el.BEGIN_TIME>=?3, 1=1)\n" +
            "  AND IF(?4 != '', el.BEGIN_TIME<=?4, 1=1)",
            countQuery = "SELECT \n" +
                    "  COUNT(1)\n" +
                    "FROM\n" +
                    "  relay_job_execute_log el,\n" +
                    "  relay_job_trigger_log tl,\n" +
                    "  relay_job_details jd,\n" +
                    "  relay_job_apps a \n" +
                    "  WHERE el.TRIGGER_LOG_ID=tl.ID\n" +
                    "  AND tl.JOB_ID=jd.ID\n" +
                    "  AND jd.APP_ID=a.ID\n" +
                    "  AND IF(?1 != '', a.id=?1, 1=1)\n" +
                    "  AND IF(?2 != '', jd.id=?2, 1=1)\n" +
                    "  AND IF(?3 != '', el.BEGIN_TIME>=?3, 1=1)\n" +
                    "  AND IF(?4 != '', el.BEGIN_TIME<=?4, 1=1)")
    Page<Map> findPageByCondition(String appId, String jobId, Date startTime, Date endTime, PageRequest page);

    @Modifying
    @Query("update JobExecuteLog log set " +
            "log.authToken=:#{#executeLog.authToken}, " +
            "log.beginTime=:#{#executeLog.beginTime}, " +
            "log.createBy=:#{#executeLog.createBy}, " +
            "log.createTime=:#{#executeLog.createTime}, " +
            "log.delFlag=:#{#executeLog.delFlag}, " +
            "log.endpointIp=:#{#executeLog.endpointIp}, " +
            "log.endpointPort=:#{#executeLog.endpointPort}, " +
            "log.endTime=:#{#executeLog.endTime}, " +
            "log.executeErrorMessage=:#{#executeLog.executeErrorMessage}, " +
            "log.executeParam=:#{#executeLog.executeParam}, " +
            "log.executeResult=:#{#executeLog.executeResult}, " +
            "log.executeStatus=:#{#executeLog.executeStatus}, " +
            "log.netProtocol=:#{#executeLog.netProtocol}, " +
            "log.retryTimes=:#{#executeLog.retryTimes}, " +
            "log.triggerLogId=:#{#executeLog.triggerLogId}, " +
            "log.updateBy=:#{#executeLog.updateBy}, " +
            "log.updateTime=:#{#executeLog.updateTime} " +
            "where log.id = :#{#executeLog.id} and log.executeStatus=:#{#executeStatus}"
    )
    void updateByIdAndExecuteStatus(@Param("executeLog") JobExecuteLog executeLog,@Param("executeStatus") String executeStatus);
}

package com.yxsk.relay.job.admin.core.executor.quartz.context;

import com.yxsk.relay.job.admin.core.executor.context.JobExecuteContext;
import com.yxsk.relay.job.admin.core.schedule.quartz.vo.JobConfig;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * @Author 11376
 * @CreaTime 2019/6/28 9:31
 * @Description
 */
@Getter
@Setter
@ToString
public class QuartzJobExecuteContext extends JobExecuteContext {

    protected JobConfig jobConfig;

}

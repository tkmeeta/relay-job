package com.yxsk.relay.job.admin.core.router.endpoint;

import lombok.AllArgsConstructor;

/**
 * @Author 11376
 * @CreaTime 2019/6/15 12:07
 * @Description
 */
@AllArgsConstructor
public class SimpleEndpointRouterInfo extends EndpointRouterInfo {

    @Override
    public SimpleEndpointRouterInfo clone() {
        SimpleEndpointRouterInfo endpoint = new SimpleEndpointRouterInfo();
        endpoint.setEndpoint(this.getEndpoint().clone());
        endpoint.setCpuCores(this.getCpuCores());
        endpoint.setFreeMemory(this.getFreeMemory());
        endpoint.setTotalMemory(this.getTotalMemory());
        return endpoint;
    }

}

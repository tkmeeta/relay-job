package com.yxsk.relay.job.admin.core.trigger.quartz;

import com.yxsk.relay.job.admin.core.schedule.constant.JobDataConstant;
import com.yxsk.relay.job.admin.core.schedule.lifecycle.manage.TaskTriggerManager;
import com.yxsk.relay.job.admin.core.trigger.AsyncDynamicTrigger;

/**
 * @Description
 * @Author 11376
 * @CreateTime 2019/8/26 9:38
 */
public class QuartzAsyncJobTrigger extends QuartzJobAdapter {

    @Override
    protected void setJobTrigger() {
        this.jobTrigger = new AsyncDynamicTrigger(this.createTriggerManager(), this.createTriggerContext(), this.createRouterDecisionMaker());
    }

    @Override
    protected TaskTriggerManager createTriggerManager() {
        return (TaskTriggerManager) this.quartzExecutionContext.getMergedJobDataMap().get(JobDataConstant.TASK_ASYNC_EXECUTE_MANAGER);
    }
}

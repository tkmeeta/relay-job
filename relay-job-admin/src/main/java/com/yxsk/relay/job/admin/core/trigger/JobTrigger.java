package com.yxsk.relay.job.admin.core.trigger;


import com.yxsk.relay.job.component.common.vo.TriggerResult;

/**
 * @Author 11376
 * @CreaTime 2019/6/28 9:05
 * @Description 任务触发器
 */
public interface JobTrigger<R extends TriggerResult> {

    R trigger();

}

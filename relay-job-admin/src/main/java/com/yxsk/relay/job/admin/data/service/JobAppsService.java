package com.yxsk.relay.job.admin.data.service;

import com.yxsk.relay.job.admin.core.schedule.quartz.vo.JobConfig;
import com.yxsk.relay.job.admin.data.entity.BaseEntity;
import com.yxsk.relay.job.admin.data.entity.JobApps;
import com.yxsk.relay.job.admin.data.entity.JobDetails;
import com.yxsk.relay.job.admin.data.repository.JobAppsRepository;
import com.yxsk.relay.job.component.common.exception.RelayJobRuntimeException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.text.MessageFormat;
import java.util.List;
import java.util.Optional;

/**
 * @Author 11376
 * @CreaTime 2019/6/22 11:42
 * @Description
 */
@Service
@Transactional
public class JobAppsService extends AbstractService<JobApps, JobAppsRepository> {

    @Autowired
    private JobDetailService jobDetailService;

    public void pause(String appId) {
        Optional<JobApps> optional = this.repository.findById(appId);
        if (!optional.isPresent()) {
            throw new RelayJobRuntimeException("未找到应用信息");
        }
        JobApps apps = optional.get();
        apps.setStatus(JobApps.AppStatus.PAUSE.getStatus());
        this.repository.save(apps);
        // 查询应用下面的job
        List<JobDetails> jobDetailList = this.jobDetailService.findByAppId(appId);
        if (!CollectionUtils.isEmpty(jobDetailList)) {
            jobDetailList.stream().forEach(jobDetail -> {
                if (JobConfig.JobStatus.NORMAL.getStatus().equals(jobDetail.getStatus())) {
                    // job状态为正常则暂停
                    this.jobDetailService.pauseJob(jobDetail.getId());
                }
            });
        }
    }

    public void deleteApp(String appId) {
        // 查询应用
        Optional<JobApps> optional = this.repository.findById(appId);
        if (!optional.isPresent()) {
            throw new RelayJobRuntimeException(MessageFormat.format("未找到应用编号[{0}]的应用信息.", appId));
        }
        JobApps apps = optional.get();
        if (BaseEntity.DelFlagEnum.NORMAL.getFlag().equals(apps.getDelFlag())) {
            // 先删除app下的所有任务
            List<JobDetails> jobDetailList = this.jobDetailService.findByAppId(appId);
            if (!CollectionUtils.isEmpty(jobDetailList)) {
                jobDetailList.stream().forEach(jobDetail -> {
                    if (BaseEntity.DelFlagEnum.NORMAL.getFlag().equals(jobDetail.getDelFlag())) {
                        // 删除任务
                        this.jobDetailService.deleteJob(jobDetail.getId());
                    }
                });
            }
            apps.setDelFlag(BaseEntity.DelFlagEnum.DELETED.getFlag());
            this.repository.save(apps);
        }
    }


    public void resume(String appId) {
        Optional<JobApps> optional = this.repository.findById(appId);
        if (!optional.isPresent()) {
            throw new RelayJobRuntimeException("Not found application");
        }
        JobApps jobApps = optional.get();
        jobApps.setStatus(JobApps.AppStatus.NORMAL.getStatus());
        this.repository.save(jobApps);
    }

    @Transactional(propagation = Propagation.SUPPORTS)
    public JobApps findByAppName(String appName) {
        return this.repository.findByAppName(appName);
    }

}

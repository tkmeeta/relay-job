package com.yxsk.relay.job.admin.data.entity.vo;

import com.yxsk.relay.job.admin.core.schedule.quartz.vo.JobConfig;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * @Author 11376
 * @CreaTime 2019/6/6 23:23
 * @Description
 */
@Getter
@Setter
@ToString(callSuper = true)
public class QuartzJobInfo extends JobConfig {
}

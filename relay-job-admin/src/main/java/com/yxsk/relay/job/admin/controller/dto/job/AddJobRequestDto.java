package com.yxsk.relay.job.admin.controller.dto.job;

import com.yxsk.relay.job.admin.core.schedule.quartz.vo.JobConfig;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.NotEmpty;
import java.io.Serializable;

/**
 * @Author 11376
 * @CreaTime 2019/6/6 14:16
 * @Description
 */
@Getter
@Setter
@ToString
public class AddJobRequestDto implements Serializable {

    // 所属应用 Id
    @NotEmpty(message = "所属应用编号不能为空")
    protected String appId;
    // 任务名称
    @NotEmpty(message = "任务名称不能为空")
    protected String jobName;
    // 任务处理器名称
    @NotEmpty(message = "任务处理器名称不能为空")
    protected String handlerName;
    // 任务触发器类名
    @NotEmpty(message = "触发器名称不能为空")
    protected String triggerName;
    // cron表达式
  //  @NotEmpty(message = "Cron表达式不能为空")
    protected String cronExpression;
    /**
     * 执行模式 {@link JobConfig.ExecuteModel}
     */
    @NotEmpty(message = "任务执行模式不能为空")
    protected String executeModel;
    // 执行路由器类名
    @NotEmpty(message = "路由策略不能为空")
    protected String routeStrategy;
    /**
     * 阻塞策略 {@link JobConfig.BlockStrategy}
     */
    @NotEmpty(message = "阻塞策略不能为空")
    protected String blockStrategy;
    // 描述
    protected String description;
    // 执行参数
    protected String triggerParam;
    // 任务执行超时时间, 单位:秒
    protected Integer executeTimeout;
    // 失败重试次数
    protected Integer failRetryTimes;
}

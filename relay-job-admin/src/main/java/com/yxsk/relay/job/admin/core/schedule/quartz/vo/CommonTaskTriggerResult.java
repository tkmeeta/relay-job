package com.yxsk.relay.job.admin.core.schedule.quartz.vo;

import com.yxsk.relay.job.component.common.utils.ExceptionUtils;
import com.yxsk.relay.job.component.common.vo.TriggerResult;
import lombok.Getter;
import lombok.Setter;

/**
 * @Author 11376
 * @CreaTime 2019/6/5 15:01
 * @Description
 */
@Getter
@Setter
public class CommonTaskTriggerResult extends TriggerResult {


    public static CommonTaskTriggerResult ok() {
        CommonTaskTriggerResult result = new CommonTaskTriggerResult();
        result.setCode(TriggerResult.TriggerResultCode.OK.getCode());
        result.setErrorMsg(TriggerResultCode.OK.getMessage());
        return result;
    }

    public static CommonTaskTriggerResult error(String message) {
        CommonTaskTriggerResult result = new CommonTaskTriggerResult();
        result.setCode(TriggerResultCode.ERROR.getCode());
        result.setErrorMsg(message);
        return result;
    }

    public static CommonTaskTriggerResult error(Throwable e) {
        CommonTaskTriggerResult result = new CommonTaskTriggerResult();
        result.setCode(TriggerResult.TriggerResultCode.ERROR.getCode());
        result.setErrorMsg(ExceptionUtils.getStackInfo(e));
        return result;
    }

    public static CommonTaskTriggerResult intercepted(String message) {
        CommonTaskTriggerResult result = new CommonTaskTriggerResult();
        result.setCode(TriggerResult.TriggerResultCode.INTERCEPTED.getCode());
        result.setErrorMsg(message);
        return result;
    }

}

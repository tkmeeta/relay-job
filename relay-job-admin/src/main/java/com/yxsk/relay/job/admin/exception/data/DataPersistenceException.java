package com.yxsk.relay.job.admin.exception.data;

import com.yxsk.relay.job.component.admin.exception.RelayJobAdminRuntimeException;

/**
 * @Author 11376
 * @CreaTime 2019/6/22 11:21
 * @Description
 */
public class DataPersistenceException extends RelayJobAdminRuntimeException {
    public DataPersistenceException(String cause) {
        super(cause);
    }

    public DataPersistenceException(Throwable e) {
        super(e);
    }

    public DataPersistenceException(String cause, Throwable e) {
        super(cause, e);
    }
}

package com.yxsk.relay.job.admin.data.service;

import com.yxsk.relay.job.admin.data.entity.JobCollectLog;
import com.yxsk.relay.job.admin.data.repository.JobCollectLogRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @Author 11376
 * @CreaTime 2019/6/22 11:42
 * @Description
 */
@Service
@Transactional
public class JobCollectLogService extends AbstractService<JobCollectLog, JobCollectLogRepository> {

    @Transactional(propagation = Propagation.SUPPORTS)
    public List<JobCollectLog> findByTriggerLog(String triggerId) {
        return this.repository.findByTriggerLogId(triggerId);
    }
}

package com.yxsk.relay.job.admin.data.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.util.Assert;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Date;

/**
 * @Author 11376
 * @Description 任务执行日志
 * @CreateTime 2019/6/21 23:33
 */
@Getter
@Setter
@ToString(callSuper = true)
@Entity
@Table(name = "RELAY_JOB_COLLECT_LOG")
public class JobCollectLog extends BaseEntity {
    // 任务触发的 Id
    @Column(name = "TRIGGER_LOG_ID")
    private String triggerLogId;
    // 执行端点 IP
    @Column(name = "ENDPOINT_IP")
    private String endpointIp;
    // 执行端点端口
    @Column(name = "ENDPOINT_PORT")
    private Integer endpointPort;
    // 授权token
    @Column(name = "AUTH_TOKEN")
    private String authToken;
    /**
     * 网络协议 {@link com.yxsk.relay.job.component.common.constant.NetProtocol}
     */
    @Column(name = "NET_PROTOCOL")
    private String netProtocol;
    /**
     * 任务执行状态
     */
    @Column(name = "COLLECT_STATUS")
    private String collectStatus;
    // 执行参数
    @Column(name = "COLLECT_PARAM")
    private String collectParam;
    // 任务执行返回结果
    @Column(name = "RESULT")
    private String result;
    // 执行的错误信息
    @Column(name = "ERROR_MESSAGE")
    private String errorMessage;
    // 执行起始时间
    @Column(name = "BEGIN_TIME")
    private Date beginTime;
    // 执行结束时间
    @Column(name = "END_TIME")
    private Date endTime;

    @AllArgsConstructor
    @Getter
    public enum CollectStatusEnum {
        SUCCESS("SUCCESS"),
        FAILED("FAILED");

        private String status;

        public static CollectStatusEnum convert(String status) {
            Assert.hasLength(status, "Status not be empty");
            CollectStatusEnum[] statusEnums = CollectStatusEnum.values();

            for (CollectStatusEnum statusEnum : statusEnums) {
                if (statusEnum.status.equals(status)) {
                    return statusEnum;
                }
            }
            throw new IllegalArgumentException("Unknown collect status[" + status + "]");
        }
    }

}

package com.yxsk.relay.job.admin.core.router.endpoint;

import lombok.AllArgsConstructor;

/**
 * @Author 11376
 * @CreaTime 2019/6/15 12:07
 * @Description
 */
@AllArgsConstructor
public class SimpleEndpoint extends EndpointInfo {

    @Override
    public SimpleEndpoint clone() {
        SimpleEndpoint endpoint = new SimpleEndpoint();
        endpoint.setId(this.getId());
        endpoint.setHost(this.getHost());
        endpoint.setProtocol(this.getProtocol());
        return endpoint;
    }

}

package com.yxsk.relay.job.admin.exception.job;

import com.yxsk.relay.job.component.common.exception.RelayJobException;

/**
 * @Author 11376
 * @CreaTime 2019/6/22 11:21
 * @Description
 */
public class JobResultCollectException extends RelayJobException {
    public JobResultCollectException(String cause) {
        super(cause);
    }

    public JobResultCollectException(Throwable e) {
        super(e);
    }

    public JobResultCollectException(String cause, Throwable e) {
        super(cause, e);
    }
}

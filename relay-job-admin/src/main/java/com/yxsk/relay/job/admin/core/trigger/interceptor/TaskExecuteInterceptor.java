package com.yxsk.relay.job.admin.core.trigger.interceptor;

import com.yxsk.relay.job.admin.core.schedule.quartz.context.TriggerContext;

/**
 * @Author 11376
 * @CreaTime 2019/6/5 18:37
 * @Description
 */
public interface TaskExecuteInterceptor {

    /**
     * @param context
     * @Author 11376
     * @Description 拦截任务执行请求
     * @CreateTime 2019/6/5 20:30
     * @Return false：不执行此次请求
     * true：执行请求
     */
    boolean intercept(TriggerContext context);

}

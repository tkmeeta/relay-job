package com.yxsk.relay.job.admin.core.router.endpoint;

import com.yxsk.relay.job.component.common.constant.NetProtocol;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

/**
 * @Author 11376
 * @CreaTime 2019/6/7 21:48
 * @Description 远程工作单元信息
 */
@Getter
@Setter
@ToString
public abstract class EndpointInfo implements Serializable, Cloneable {

    // 端点 Id
    private String id;
    // 主机 IP
    private String host;
    // 服务端口
    private Integer port;
    // token
    private String authToken;
    // 工作单元网络协议
    private NetProtocol protocol;

    public abstract EndpointInfo clone();

}

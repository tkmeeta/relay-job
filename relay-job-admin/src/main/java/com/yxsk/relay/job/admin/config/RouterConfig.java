package com.yxsk.relay.job.admin.config;

import com.yxsk.relay.job.admin.core.router.routes.multi.SimpleMultiLineRouter;
import com.yxsk.relay.job.admin.core.router.routes.single.SinglePollRouter;
import com.yxsk.relay.job.admin.core.router.selector.ChainRouterDecisionMaker;
import com.yxsk.relay.job.admin.core.router.selector.RouterDecisionMaker;
import com.yxsk.relay.job.admin.core.schedule.quartz.vo.JobConfig;
import com.yxsk.relay.job.component.admin.registry.Steward;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;

/**
 * @Author 11376
 * @CreaTime 2019/6/15 22:34
 * @Description 执行路由配置
 */
@Configuration
public class RouterConfig {

    @Lazy(false)
    @Bean
    public RouterDecisionMaker routerDecisionMaker(Steward steward) {
        ChainRouterDecisionMaker decisionMaker = new ChainRouterDecisionMaker();
        // 单机轮询策略
        decisionMaker.addSelector((jobConfig, chain) -> {
            if (jobConfig.getRouteStrategy() == JobConfig.RouteStrategy.POLL) {
                return singlePollRouter(steward);
            }
            return chain.chain(jobConfig);
        });
        // 多实例路由
        decisionMaker.addSelector((jobConfig, chain) -> {
            if (jobConfig.getRouteStrategy() == JobConfig.RouteStrategy.MULTIPLE_INSTANCES) {
                return simpleMultiLineRouter(steward);
            }
            return chain.chain(jobConfig);
        });
        return decisionMaker;
    }

    @Lazy(false)
    @Bean
    public SinglePollRouter singlePollRouter(Steward steward) {
        return SinglePollRouter.singlePollRouter(steward);
    }

    @Lazy(false)
    @Bean
    public SimpleMultiLineRouter simpleMultiLineRouter(Steward steward) {
        return new SimpleMultiLineRouter(steward);
    }

}

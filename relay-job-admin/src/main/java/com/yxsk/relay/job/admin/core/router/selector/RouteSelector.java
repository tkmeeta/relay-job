package com.yxsk.relay.job.admin.core.router.selector;

import com.yxsk.relay.job.admin.core.router.Router;
import com.yxsk.relay.job.admin.core.schedule.quartz.vo.JobConfig;

/**
 * @Author 11376
 * @CreaTime 2019/6/15 12:58
 * @Description 路由选择器
 */
public interface RouteSelector {

    Router select(JobConfig jobConfig, Chain chain);

    interface Chain {

        Router chain(JobConfig jobConfig);

    }

}

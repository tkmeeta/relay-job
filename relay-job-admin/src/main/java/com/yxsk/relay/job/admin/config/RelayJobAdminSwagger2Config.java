package com.yxsk.relay.job.admin.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * @Author 11376
 * @Description swagger rest api文档配置
 * @CreateTime 2019/6/2 15:44
 */
@Configuration
@EnableSwagger2
public class RelayJobAdminSwagger2Config {
    @Bean
    public Docket relayJobAdminApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .groupName("Relay-Job-Admin-API")
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.yxsk.relay.job.admin.controller"))
                .paths(PathSelectors.any())
                .build()
                .apiInfo(apiInfo());
    }

    private ApiInfo apiInfo() {
        final Contact contact = new Contact("W.D. ZHOU", "www.yxsk.com", "1137661952@qq.com");
        return new ApiInfoBuilder()
                .title("RELAY JOB SERVICE REST API")
                .description("Rest api docs for relay job service of YXSK")
                .version("1.0.0")
                .license("Apache License Version 2.0")
                .licenseUrl("https://www.apache.org/licenses/LICENSE-2.0")
                .contact(contact)
                .build();
    }


}

package com.yxsk.relay.job.admin.core.task;

import com.yxsk.relay.job.admin.core.schedule.quartz.vo.JobConfig;
import com.yxsk.relay.job.component.common.vo.TriggerResult;
import lombok.Getter;

/**
 * @Author 11376
 * @CreaTime 2019/6/5 14:27
 * @Description
 */
@Getter
public abstract class RelayJobTrigger<R extends TriggerResult> extends RelayTask<R> {

    protected JobConfig jobConfig;

    protected RelayJobTrigger(JobConfig jobConfig) {
        this.jobConfig = jobConfig;
    }

}

package com.yxsk.relay.job.admin.core.router.selector;

import com.yxsk.relay.job.admin.core.router.Router;
import com.yxsk.relay.job.admin.core.schedule.quartz.vo.JobConfig;

/**
 * @Author 11376
 * @CreaTime 2019/6/15 12:57
 * @Description 路由决策者
 */
public interface RouterDecisionMaker {

    Router decision(JobConfig jobConfig);

}

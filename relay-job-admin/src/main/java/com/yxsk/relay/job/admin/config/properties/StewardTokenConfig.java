package com.yxsk.relay.job.admin.config.properties;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @Author 11376
 * @CreaTime 2019/6/9 10:52
 * @Description
 */
@Getter
@Setter
@ToString
@ConfigurationProperties(prefix = "relay.job.steward.auth")
public class StewardTokenConfig {
    // 签约令牌
    private String token;

}

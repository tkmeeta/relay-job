package com.yxsk.relay.job.admin.aop;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpInputMessage;

import java.io.IOException;
import java.io.InputStream;

/**
 * @Description
 * @Author 11376
 * @CreateTime 2019/8/24 12:02
 */
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class RelayJobHttpMessage implements HttpInputMessage {

    private InputStream is;
    private HttpHeaders headers;

    @Override
    public InputStream getBody() throws IOException {
        return this.is;
    }

    @Override
    public HttpHeaders getHeaders() {
        return this.headers;
    }

}

package com.yxsk.relay.job.admin.data.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Date;

/**
 * @Author 11376
 * @Description 任务执行日志
 * @CreateTime 2019/6/21 23:33
 */
@Getter
@Setter
@ToString(callSuper = true)
@Entity
@Table(name = "RELAY_JOB_EXECUTE_LOG")
public class JobExecuteLog extends BaseEntity {
    // 任务触发的 Id
    @Column(name = "TRIGGER_LOG_ID")
    private String triggerLogId;
    // 执行端点 IP
    @Column(name = "ENDPOINT_IP")
    private String endpointIp;
    // 执行端点端口
    @Column(name = "ENDPOINT_PORT")
    private Integer endpointPort;
    // 授权token
    @Column(name = "AUTH_TOKEN")
    private String authToken;
    /**
     * 网络协议 {@link com.yxsk.relay.job.component.common.constant.NetProtocol}
     */
    @Column(name = "NET_PROTOCOL")
    private String netProtocol;
    /**
     * 任务执行状态 {@link com.yxsk.relay.job.component.common.vo.ExecuteResult.ExecuteResultCode}
     */
    @Column(name = "EXECUTE_STATUS")
    private String executeStatus;
    // 执行参数
    @Column(name = "EXECUTE_PARAM")
    private String executeParam;
    // 任务执行返回结果
    @Column(name = "EXECUTE_RESULT")
    private String executeResult;
    // 执行的错误信息
    @Column(name = "EXECUTE_ERROR_MESSAGE")
    private String executeErrorMessage;
    // 重试次数
    @Column(name = "RETRY_TIMES")
    private Integer retryTimes;
    // 执行起始时间
    @Column(name = "BEGIN_TIME")
    private Date beginTime;
    // 执行结束时间
    @Column(name = "END_TIME")
    private Date endTime;
}

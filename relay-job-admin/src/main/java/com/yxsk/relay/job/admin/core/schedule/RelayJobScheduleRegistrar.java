package com.yxsk.relay.job.admin.core.schedule;

import com.yxsk.relay.job.admin.core.schedule.quartz.vo.JobConfig;
import com.yxsk.relay.job.admin.exception.job.RelayTaskIrrevocableException;

import java.util.Map;

/**
 * @Author 11376
 * @CreaTime 2019/6/5 13:52
 * @Description 任务调度注册器
 */
public interface RelayJobScheduleRegistrar<T extends JobConfig> {

    /**
     * @param
     * @Author 11376
     * @Description 重新加载任务配置
     * @CreateTime 2019/6/5 13:53
     * @Return
     */
    void reloadJob();

    /**
     * @param jobConfig
     * @Author 11376
     * @Description 注册任务配置
     * @CreateTime 2019/6/5 13:57
     * @Return
     */
    void registerJob(T jobConfig);

    /**
     * @param jobConfig
     * @Author 11376
     * @Description 更新任务配置
     * @CreateTime 2019/6/5 13:58
     * @Return
     */
    void updateJob(T jobConfig);

    /**
     * @param jobConfig
     * @Author 11376
     * @Description 卸载任务配置
     * @CreateTime 2019/6/5 13:59
     * @Return
     */
    void unloadJob(T jobConfig);

    /**
     * @param jobConfig
     * @Author 11376
     * @Description 马上执行一个任务
     * @CreateTime 2019/6/6 20:17
     * @Return
     */
    void runNow(T jobConfig, Map<String, Object> param);

    /**
     * @param executeId
     * @Author 11376
     * @Description 取消正在执行的任务
     * @CreateTime 2019/6/6 20:18
     * @Return
     */
    void cancelExecute(String executeId) throws RelayTaskIrrevocableException;

    /**
     * @param
     * @Author 11376
     * @Description 暂停任务
     * @CreateTime 2019/6/5 18:01
     * @Return
     */
    void pauseJob(T jobConfig);

    /**
     * @param
     * @Author 11376
     * @Description 暂停所有任务
     * @CreateTime 2019/6/5 18:01
     * @Return
     */
    void pauseAllJob();

    /**
     * @param
     * @Author 11376
     * @Description 恢复任务
     * @CreateTime 2019/6/5 18:02
     * @Return
     */
    void resumeJob(JobConfig jobConfig);

    /**
     * @param
     * @Author 11376
     * @Description 恢复所有任务
     * @CreateTime 2019/6/5 18:02
     * @Return
     */
    void resumeAllJob();

}

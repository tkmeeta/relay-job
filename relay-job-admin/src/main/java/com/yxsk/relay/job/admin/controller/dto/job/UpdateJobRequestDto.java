package com.yxsk.relay.job.admin.controller.dto.job;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.NotEmpty;

/**
 * @Author 11376
 * @CreaTime 2019/6/6 14:16
 * @Description
 */
@Getter
@Setter
@ToString
public class UpdateJobRequestDto extends AddJobRequestDto {

    // id
    @NotEmpty(message = "任务编号不能为空")
    private String id;

}

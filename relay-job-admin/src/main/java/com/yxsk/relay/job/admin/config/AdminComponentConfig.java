package com.yxsk.relay.job.admin.config;

import com.yxsk.relay.job.admin.aop.RestfulServiceExceptionHandlerAdvice;
import com.yxsk.relay.job.component.admin.utils.SpringBeanUtils;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

/**
 * @Author 11376
 * @CreaTime 2019/6/2 15:33
 * @Description
 */
@Configuration
@Import({RestfulServiceExceptionHandlerAdvice.class, SpringBeanUtils.class})
@EnableJpaRepositories(basePackages = "com.yxsk.relay.job.admin.data.repository")
@ComponentScan("com.yxsk.relay.job.admin")
public class AdminComponentConfig {

}

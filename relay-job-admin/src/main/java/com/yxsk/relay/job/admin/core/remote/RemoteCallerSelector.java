package com.yxsk.relay.job.admin.core.remote;

import com.yxsk.relay.job.component.admin.utils.SpringBeanUtils;
import com.yxsk.relay.job.component.common.constant.NetProtocol;
import com.yxsk.relay.job.component.common.protocol.caller.HttpRestfulRemoteCaller;
import com.yxsk.relay.job.component.common.protocol.caller.RemoteCaller;
import org.springframework.stereotype.Component;

/**
 * @Author 11376
 * @CreaTime 2019/8/6 18:22
 * @Description 远程过程调用执行器选择器
 */
@Component
public class RemoteCallerSelector {

    public RemoteCaller select(NetProtocol protocol) {
        switch (protocol) {
            case HTTP:
                return getSpringCaller(HttpRestfulRemoteCaller.class);
            default:
                return getSpringCaller(HttpRestfulRemoteCaller.class);
        }
    }

    private <T extends RemoteCaller> T getSpringCaller(Class<T> clazz) {
        // 远程过程调用器暂时
        return SpringBeanUtils.getBean(clazz);
    }

}

package com.yxsk.relay.job.admin.data.repository;

import com.yxsk.relay.job.admin.data.entity.JobApps;

/**
 * @Author 11376
 * @CreaTime 2019/6/22 11:02
 * @Description
 */
public interface JobAppsRepository extends BaseRepository<JobApps> {

    JobApps findByAppName(String appName);

}

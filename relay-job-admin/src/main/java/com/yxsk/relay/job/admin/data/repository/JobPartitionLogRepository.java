package com.yxsk.relay.job.admin.data.repository;

import com.yxsk.relay.job.admin.data.entity.JobPartitionLog;

import java.util.List;

/**
 * @Author 11376
 * @CreaTime 2019/6/22 11:05
 * @Description
 */
public interface JobPartitionLogRepository extends BaseRepository<JobPartitionLog> {

    List<JobPartitionLog> findByTriggerLogId(String triggerId);

}

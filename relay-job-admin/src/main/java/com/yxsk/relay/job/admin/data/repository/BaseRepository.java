package com.yxsk.relay.job.admin.data.repository;

import com.yxsk.relay.job.admin.data.entity.BaseEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;

import java.util.List;

/**
 * @Author 11376
 * @CreaTime 2019/6/22 11:03
 * @Description
 */
@NoRepositoryBean
public interface BaseRepository<T extends BaseEntity> extends JpaRepository<T, String> {

    List<T> findByDelFlag(String delFlag);

}

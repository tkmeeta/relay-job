package com.yxsk.relay.job.admin.core.schedule.quartz.context;

import com.yxsk.relay.job.admin.core.schedule.JobTriggerInfo;
import com.yxsk.relay.job.admin.core.schedule.quartz.vo.JobConfig;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * @Author 11376
 * @CreaTime 2019/6/6 9:39
 * @Description
 */
@Getter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class TriggerContext {

    private JobConfig jobConfig;

    private JobTriggerInfo triggerInfo;

}

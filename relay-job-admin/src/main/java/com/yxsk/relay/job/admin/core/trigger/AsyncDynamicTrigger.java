package com.yxsk.relay.job.admin.core.trigger;

import com.yxsk.relay.job.admin.core.collector.QuartzAsyncJobResultCollector;
import com.yxsk.relay.job.admin.core.collector.QuartzRemoteJobResultCollector;
import com.yxsk.relay.job.admin.core.executor.JobExecutor;
import com.yxsk.relay.job.admin.core.executor.listener.JobExecuteListener;
import com.yxsk.relay.job.admin.core.executor.quartz.QuartzAsyncRemoteJobExecutor;
import com.yxsk.relay.job.admin.core.router.selector.RouterDecisionMaker;
import com.yxsk.relay.job.admin.core.schedule.lifecycle.manage.TaskTriggerManager;
import com.yxsk.relay.job.admin.core.schedule.quartz.context.TriggerContext;
import com.yxsk.relay.job.admin.core.schedule.quartz.vo.JobConfig;
import com.yxsk.relay.job.component.admin.monitor.AsyncJobStatusMonitor;
import com.yxsk.relay.job.component.admin.utils.SpringBeanUtils;
import lombok.extern.slf4j.Slf4j;

/**
 * @Author 11376
 * @CreaTime 2019/6/6 23:40
 * @Description 异步 Quartz 动态任务
 */
@Slf4j
public class AsyncDynamicTrigger extends DynamicTrigger {

    public AsyncDynamicTrigger(TaskTriggerManager taskTriggerManager, TriggerContext context, RouterDecisionMaker routerDecisionMaker) {
        super(taskTriggerManager, context, routerDecisionMaker);
    }

    @Override
    protected JobExecutor getJobExecutor(JobExecuteListener executeListener) {
        if (this.context.getJobConfig().getExecuteModel() == JobConfig.ExecuteModel.CLUSTER) {
            // 实例化异步任务结果处理器
            QuartzAsyncJobResultCollector collector = new QuartzAsyncJobResultCollector(new QuartzRemoteJobResultCollector(), this.context.getTriggerInfo());
            return new QuartzAsyncRemoteJobExecutor(executeListener,
                    SpringBeanUtils.getBean(AsyncJobStatusMonitor.class),
                    // 注入执行回调
                    collector.new DefaultExecuteCompleteCallback());
        }
        return new QuartzAsyncRemoteJobExecutor(executeListener, SpringBeanUtils.getBean(AsyncJobStatusMonitor.class));
    }

}

package com.yxsk.relay.job.admin.config.registry.security;

import com.yxsk.relay.job.admin.data.entity.JobApps;
import com.yxsk.relay.job.admin.data.service.JobAppsService;
import com.yxsk.relay.job.component.admin.exception.registry.RelayJobRegistryException;
import com.yxsk.relay.job.component.admin.registry.security.SecurityChecker;
import com.yxsk.relay.job.component.common.vo.SlaveInfo;
import lombok.AllArgsConstructor;
import org.apache.commons.lang3.StringUtils;

/**
 * @Description
 * @Author 11376
 * @CreateTime 2019/9/20 21:13
 */
@AllArgsConstructor
public class ApplicationChecker implements SecurityChecker {

    private JobAppsService appsService;

    @Override
    public void check(SlaveInfo slaveInfo, String token) throws RelayJobRegistryException {
        String appName = slaveInfo.getAppName();
        if (StringUtils.isEmpty(appName)) {
            throw new RelayJobRegistryException("AppName not be empty.");
        }
        JobApps apps = this.appsService.findByAppName(appName);
        if (apps == null) {
            throw new RelayJobRegistryException("Not found application of app name: " + appName);
        }
    }

}

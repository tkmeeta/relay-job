package com.yxsk.relay.job.component.admin.exception;

import com.yxsk.relay.job.component.common.exception.RelayJobRuntimeException;

/**
 * @Description
 * @Author 11376
 * @CreateTime 2019/9/8 22:24
 */
public class RelayJobAdminRuntimeException extends RelayJobRuntimeException {
    public RelayJobAdminRuntimeException(String cause) {
        super(cause);
    }

    public RelayJobAdminRuntimeException(Throwable e) {
        super(e);
    }

    public RelayJobAdminRuntimeException(String cause, Throwable e) {
        super(cause, e);
    }
}

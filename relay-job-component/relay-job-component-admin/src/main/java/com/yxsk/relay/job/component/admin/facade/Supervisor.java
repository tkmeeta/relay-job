package com.yxsk.relay.job.component.admin.facade;

import com.yxsk.relay.job.component.common.protocol.message.base.ResultResponse;
import com.yxsk.relay.job.component.common.protocol.message.execute.async.JobExecuteCallbackRequest;
import com.yxsk.relay.job.component.common.protocol.message.execute.async.JobExecuteCallbackResponse;

import javax.validation.Valid;

/**
 * @Author 11376
 * @CreaTime 2019/6/19 14:46
 * @Description 管理端的任务回调接口
 */
public interface Supervisor {

    /**
     * @param callbackRequest
     * @Author 11376
     * @Description 执行结果回调
     * @CreateTime 2019/6/19 14:48
     * @Return
     */
    ResultResponse<JobExecuteCallbackResponse> executeResultCallback(@Valid JobExecuteCallbackRequest callbackRequest);

}

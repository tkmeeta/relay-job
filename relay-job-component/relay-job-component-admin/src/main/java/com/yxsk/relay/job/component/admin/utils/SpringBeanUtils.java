package com.yxsk.relay.job.component.admin.utils;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

/**
 * @Author 11376
 * @CreaTime 2019/6/5 17:42
 * @Description
 */
@Lazy(false)
@Component
public class SpringBeanUtils implements ApplicationContextAware {

    private static ApplicationContext context;

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        SpringBeanUtils.context = applicationContext;
    }

    public static <T> T getBean(Class<T> t) {
        return context.getBean(t);
    }

    public static Object getBean(String beanName) {
        return context.getBean(beanName);
    }

    public static <T> T getBean(String beanName, Class<T> t) {
        return context.getBean(beanName, t);
    }

    public static <T> T getEnvironmentProperty(String key, Class<T> valueType) {
        return context.getEnvironment().getProperty(key, valueType);
    }

}

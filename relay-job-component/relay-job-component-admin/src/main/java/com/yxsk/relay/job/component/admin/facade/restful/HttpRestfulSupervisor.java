package com.yxsk.relay.job.component.admin.facade.restful;

import com.yxsk.relay.job.component.admin.facade.Supervisor;
import com.yxsk.relay.job.component.admin.monitor.AsyncJobStatusMonitor;
import com.yxsk.relay.job.component.admin.monitor.JobExecuteCallbackInfo;
import com.yxsk.relay.job.component.common.constant.UriConstant;
import com.yxsk.relay.job.component.common.protocol.message.base.ResultResponse;
import com.yxsk.relay.job.component.common.protocol.message.execute.async.JobExecuteCallbackRequest;
import com.yxsk.relay.job.component.common.protocol.message.execute.async.JobExecuteCallbackResponse;
import com.yxsk.relay.job.component.common.utils.DateUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

/**
 * @Author 11376
 * @CreaTime 2019/6/19 14:48
 * @Description
 */
@Slf4j
@Api(description = "Relay-job管理端任务管理回调接口")
@RestController
public class HttpRestfulSupervisor implements Supervisor {

    @Autowired(required = false)
    private AsyncJobStatusMonitor jobStatusMonitor;

    @ApiOperation("执行结果回调")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "serialNo", value = "流水号", required = true),
            @ApiImplicitParam(name = "version", value = "版本", required = true),
            @ApiImplicitParam(name = "requestTime", value = "请求时间", required = true),
            @ApiImplicitParam(name = "token", value = "token"),
            @ApiImplicitParam(name = "host", value = "主机", required = true),
            @ApiImplicitParam(name = "netProtocol", value = "业务通讯协议")
    })
    @Override
    @PostMapping(UriConstant.TASK_ASYNC_EXECUTE_RESULT_CALLBACK_URI)
    public ResultResponse<JobExecuteCallbackResponse> executeResultCallback(@RequestBody @Valid JobExecuteCallbackRequest callbackRequest) {
        if (log.isDebugEnabled()) {
            log.debug("Task execution result callback request received, request: {}", callbackRequest);
        }
        if (this.jobStatusMonitor != null) {
            JobExecuteCallbackInfo callbackInfo = new JobExecuteCallbackInfo();
            BeanUtils.copyProperties(callbackRequest, callbackInfo);
            jobStatusMonitor.doFinishCallback(callbackInfo);
            if (log.isDebugEnabled()) {
                log.debug("Task monitor processing completed, job id[{}]", callbackRequest.getJobId());
            }
        } else {
            if (log.isWarnEnabled()) {
                log.warn("Not found job monitor.");
            }
        }
        JobExecuteCallbackResponse response = new JobExecuteCallbackResponse();
        response.setRequestSerialNo(callbackRequest.getSerialNo());
        response.setRespTime(DateUtils.getCurrentDate());
        return ResultResponse.ok(response);
    }
}

package com.yxsk.relay.job.component.admin.registry.security;

import com.yxsk.relay.job.component.admin.exception.registry.RelayJobRegistryException;
import com.yxsk.relay.job.component.common.vo.SlaveInfo;

/**
 * @Author 11376
 * @CreaTime 2019/7/1 20:58
 * @Description
 */
public interface SecurityChecker {

    /**
     * @param slaveInfo
     * @param token
     * @Author 11376
     * @Description 服务注册安全检查
     * @CreateTime 2019/7/2 9:57
     * @Return if check pass return 'true', else 'false'
     */
    void check(SlaveInfo slaveInfo, String token) throws RelayJobRegistryException;

    /**
     * @param slaveInfo
     * @param token
     * @Author 11376
     * @Description 是否需要检查
     * @CreateTime 2019/7/2 10:02
     * @Return
     */
    default boolean isCheck(SlaveInfo slaveInfo, String token) {
        return true;
    }

}

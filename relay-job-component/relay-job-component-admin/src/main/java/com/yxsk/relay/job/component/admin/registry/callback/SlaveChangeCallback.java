package com.yxsk.relay.job.component.admin.registry.callback;

import com.yxsk.relay.job.component.common.vo.SlaveInfo;

/**
 * @Author 11376
 * @CreaTime 2019/6/15 11:46
 * @Description 服务列表更新回调
 */
public interface SlaveChangeCallback {

    void online(SlaveInfo slaveInfo);

    void offline(SlaveInfo slaveInfo);

}

package com.yxsk.relay.job.component.admin.exception.registry;

import com.yxsk.relay.job.component.admin.exception.RelayJobAdminException;

/**
 * @Author 11376
 * @CreaTime 2019/7/1 21:09
 * @Description
 */
public class RelayJobRegistryException extends RelayJobAdminException {
    public RelayJobRegistryException(String cause) {
        super(cause);
    }

    public RelayJobRegistryException(Throwable e) {
        super(e);
    }

    public RelayJobRegistryException(String cause, Throwable e) {
        super(cause, e);
    }
}

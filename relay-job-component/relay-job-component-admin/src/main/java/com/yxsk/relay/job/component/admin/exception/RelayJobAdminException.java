package com.yxsk.relay.job.component.admin.exception;

import com.yxsk.relay.job.component.common.exception.RelayJobException;

/**
 * @Description
 * @Author 11376
 * @CreateTime 2019/9/8 22:24
 */
public class RelayJobAdminException extends RelayJobException {
    public RelayJobAdminException(String cause) {
        super(cause);
    }

    public RelayJobAdminException(Throwable e) {
        super(e);
    }

    public RelayJobAdminException(String cause, Throwable e) {
        super(cause, e);
    }
}

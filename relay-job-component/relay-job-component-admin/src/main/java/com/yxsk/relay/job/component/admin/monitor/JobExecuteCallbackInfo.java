package com.yxsk.relay.job.component.admin.monitor;

import lombok.*;

/**
 * @Author 11376
 * @CreaTime 2019/6/19 14:40
 * @Description
 */
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class JobExecuteCallbackInfo {

    // 任务执行的 Id
    private String jobId;
    // 执行主机
    private String host;
    /**
     * 执行结果 {@link com.yxsk.relay.job.component.common.vo.ExecuteResult.ExecuteResultCode}
     */
    private String resultCode;
    // 执行结果
    private String result;

    private String errorMsg;

}

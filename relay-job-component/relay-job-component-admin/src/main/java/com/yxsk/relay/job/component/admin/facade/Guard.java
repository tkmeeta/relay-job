package com.yxsk.relay.job.component.admin.facade;

import com.yxsk.relay.job.component.common.protocol.message.base.ResultResponse;
import com.yxsk.relay.job.component.common.protocol.message.renewal.RenewalRequest;
import com.yxsk.relay.job.component.common.protocol.message.renewal.RenewalResponse;
import com.yxsk.relay.job.component.common.protocol.message.termination.TerminationRequest;
import com.yxsk.relay.job.component.common.protocol.message.termination.TerminationResponse;

/**
 * @Author 11376
 * @CreaTime 2019/6/8 22:17
 * @Description
 */
public interface Guard {

    /**
     * @param request
     * @Author 11376
     * @Description 服务续期请求
     * @CreateTime 2019/6/8 22:50
     * @Return
     */
    ResultResponse<RenewalResponse> renewal(RenewalRequest request);

    /**
     * @param request
     * @Author 11376
     * @Description 服务下线
     * @CreateTime 2019/6/8 22:53
     * @Return
     */
    ResultResponse<TerminationResponse> terminate(TerminationRequest request);

}

package com.yxsk.relay.job.component.admin.monitor.callback;

import com.yxsk.relay.job.component.common.protocol.message.execute.JobExecuteResponse;

/**
 * @Author 11376
 * @CreaTime 2019/6/17 21:30
 * @Description 监控时间回调
 */
public interface MonitorEventCallback {

    /**
     * @param jobId
     * @param executeResponse
     * @Author 11376
     * @Description 任务完成回调
     * @CreateTime 2019/6/17 22:28
     * @Return
     */
    void onFinish(String jobId, JobExecuteResponse executeResponse);

    /**
     * @param jobId
     * @param e
     * @Author 11376
     * @Description 监控异常回调
     * @CreateTime 2019/6/17 22:28
     * @Return
     */
    void onError(String jobId, Throwable e);

    /**
     * @param jobId
     * @Author 11376
     * @Description 执行超时时回调
     * @CreateTime 2019/7/1 10:42
     * @Return
     */
    void onExecuteTimeout(String jobId);

}

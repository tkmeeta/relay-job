package com.yxsk.relay.job.component.admin.registry;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.concurrent.TimeUnit;

/**
 * @Author 11376
 * @CreaTime 2019/6/9 10:52
 * @Description
 */
@Getter
@Setter
@ToString
@ConfigurationProperties(prefix = "relay.job.steward.contract")
public class StewardConfig {
    // 契约有效期
    private Long expirationTime;
    // 是否保存异常下线 slave
    private Boolean recordAnomalySlave;
    // 异常下线 slave 保存时长
    private Long anomalyKeepTime;

    {
        this.expirationTime = TimeUnit.MILLISECONDS.convert(30, TimeUnit.SECONDS);
        this.recordAnomalySlave = true;
        this.anomalyKeepTime = TimeUnit.MILLISECONDS.convert(5, TimeUnit.MINUTES);
    }

}

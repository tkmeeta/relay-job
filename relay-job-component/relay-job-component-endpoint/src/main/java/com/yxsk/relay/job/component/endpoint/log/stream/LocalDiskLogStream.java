package com.yxsk.relay.job.component.endpoint.log.stream;

import java.util.List;

/**
 * @Author 11376
 * @CreaTime 2019/6/29 21:34
 * @Description
 */
public class LocalDiskLogStream extends DiskLogStream {

    public LocalDiskLogStream(String logRootPath) {
        super(logRootPath);
    }

    @Override
    public List<String> read(String jobId) {
        return this.readLine(jobId, null, null);
    }

    @Override
    public List<String> read(String jobId, long startLine) {
        return this.readLine(jobId, startLine, null);
    }

}

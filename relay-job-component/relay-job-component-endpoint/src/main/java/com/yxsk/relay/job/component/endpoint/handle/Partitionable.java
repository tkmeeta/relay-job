package com.yxsk.relay.job.component.endpoint.handle;

import com.yxsk.relay.job.component.common.vo.Endpoint;
import com.yxsk.relay.job.component.common.vo.PartitionInfo;

import java.util.List;

/**
 * @Author 11376
 * @CreaTime 2019/7/17 11:17
 * @Description endpoint 任务执行分片
 */
public interface Partitionable {

    /**
     * 任务分片
     */
    List<PartitionInfo> partition(List<Endpoint> availableEndpoints, String executeParam);

}
package com.yxsk.relay.job.component.endpoint.handle;

import com.yxsk.relay.job.component.common.exception.RelayJobException;
import com.yxsk.relay.job.component.common.vo.EndpointExecuteResult;

import java.util.List;

/**
 * @Author 11376
 * @CreaTime 2019/7/17 11:39
 * @Description 同一触发任务执行后执行
 */
public interface Collectable {

    String collect(List<EndpointExecuteResult> executeResults) throws RelayJobException;

}

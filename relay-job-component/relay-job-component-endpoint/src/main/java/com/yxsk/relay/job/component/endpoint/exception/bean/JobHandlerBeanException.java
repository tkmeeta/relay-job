package com.yxsk.relay.job.component.endpoint.exception.bean;

import com.yxsk.relay.job.component.endpoint.exception.RelayJobEndpointException;

/**
 * @Description
 * @Author 11376
 * @CreateTime 2019/9/15 0:12
 */
public class JobHandlerBeanException extends RelayJobEndpointException {
    public JobHandlerBeanException(String cause) {
        super(cause);
    }

    public JobHandlerBeanException(Throwable e) {
        super(e);
    }

    public JobHandlerBeanException(String cause, Throwable e) {
        super(cause, e);
    }
}

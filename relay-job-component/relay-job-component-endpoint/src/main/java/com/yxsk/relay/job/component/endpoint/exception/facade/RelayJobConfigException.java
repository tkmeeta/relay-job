package com.yxsk.relay.job.component.endpoint.exception.facade;

import com.yxsk.relay.job.component.endpoint.exception.RelayJobEndpointException;

/**
 * @Description
 * @Author 11376
 * @CreateTime 2019/9/10 8:42
 */
public class RelayJobConfigException extends RelayJobEndpointException {
    public RelayJobConfigException(String cause) {
        super(cause);
    }

    public RelayJobConfigException(Throwable e) {
        super(e);
    }

    public RelayJobConfigException(String cause, Throwable e) {
        super(cause, e);
    }
}

package com.yxsk.relay.job.component.endpoint.facade;

import com.yxsk.relay.job.component.common.constant.UriConstant;
import com.yxsk.relay.job.component.common.protocol.message.base.ResultResponse;
import com.yxsk.relay.job.component.common.protocol.message.collect.ExecuteResultCollectRequest;
import com.yxsk.relay.job.component.common.protocol.message.collect.ExecuteResultCollectResponse;
import com.yxsk.relay.job.component.common.protocol.message.execute.JobExecuteRequest;
import com.yxsk.relay.job.component.common.protocol.message.execute.JobExecuteResponse;
import com.yxsk.relay.job.component.common.protocol.message.execute.async.query.AsyncJobQueryRequest;
import com.yxsk.relay.job.component.common.protocol.message.execute.async.query.AsyncJobQueryResponse;
import com.yxsk.relay.job.component.common.protocol.message.log.delete.LogDeleteRequestDto;
import com.yxsk.relay.job.component.common.protocol.message.log.delete.LogDeleteResponseDto;
import com.yxsk.relay.job.component.common.protocol.message.log.query.LogQueryRequestDto;
import com.yxsk.relay.job.component.common.protocol.message.log.query.LogQueryResponseDto;
import com.yxsk.relay.job.component.common.protocol.message.partition.JobPartitionRequest;
import com.yxsk.relay.job.component.common.protocol.message.partition.JobPartitionResponse;
import com.yxsk.relay.job.component.endpoint.facade.anno.FacadeUriMapping;

/**
 * @Author 11376
 * @CreaTime 2019/6/15 16:46
 * @Description 远程任务执行接口
 */
public interface JobHeadman {

    @FacadeUriMapping(UriConstant.TASK_EXECUTE_URI)
    ResultResponse<JobExecuteResponse> execute(JobExecuteRequest executeRequest);

    @FacadeUriMapping(UriConstant.TASK_ASYNC_EXECUTE_URI)
    ResultResponse<JobExecuteResponse> asyncExecute(JobExecuteRequest executeRequest);

    @FacadeUriMapping(UriConstant.TASK_PARTITION_URI)
    ResultResponse<JobPartitionResponse> jobPartition(JobPartitionRequest partitionRequest);

    @FacadeUriMapping(UriConstant.TASK_RESULT_COLLECT_URI)
    ResultResponse<ExecuteResultCollectResponse> resultCollect(ExecuteResultCollectRequest resultCollectRequest);

    @FacadeUriMapping(UriConstant.TASK_ASYNC_EXECUTE_RESULT_QUERY_URI)
    ResultResponse<AsyncJobQueryResponse> queryExecuteResult(AsyncJobQueryRequest queryRequest);

    @FacadeUriMapping(UriConstant.LOG_QUERY)
    ResultResponse<LogQueryResponseDto> queryLog(LogQueryRequestDto requestDto);

    @FacadeUriMapping(UriConstant.LOG_DELETE)
    ResultResponse<LogDeleteResponseDto> deleteLog(LogDeleteRequestDto deleteRequestDto);
}

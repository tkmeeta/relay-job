package com.yxsk.relay.job.component.endpoint.exception.facade;

import com.yxsk.relay.job.component.endpoint.exception.RelayJobEndpointException;

/**
 * @Author 11376
 * @CreaTime 2019/6/15 18:22
 * @Description
 */
public class JobExecutorNotFoundException extends RelayJobEndpointException {
    public JobExecutorNotFoundException(String cause) {
        super(cause);
    }

    public JobExecutorNotFoundException(Throwable e) {
        super(e);
    }

    public JobExecutorNotFoundException(String cause, Throwable e) {
        super(cause, e);
    }
}

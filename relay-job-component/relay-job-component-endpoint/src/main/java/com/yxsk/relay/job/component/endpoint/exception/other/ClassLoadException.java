package com.yxsk.relay.job.component.endpoint.exception.other;

import com.yxsk.relay.job.component.endpoint.exception.RelayJobEndpointException;

/**
 * @Description
 * @Author 11376
 * @CreateTime 2019/9/10 9:40
 */
public class ClassLoadException extends RelayJobEndpointException {
    public ClassLoadException(String cause) {
        super(cause);
    }

    public ClassLoadException(Throwable e) {
        super(e);
    }

    public ClassLoadException(String cause, Throwable e) {
        super(cause, e);
    }
}

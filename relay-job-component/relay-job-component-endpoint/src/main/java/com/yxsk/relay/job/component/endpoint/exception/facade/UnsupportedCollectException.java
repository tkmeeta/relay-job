package com.yxsk.relay.job.component.endpoint.exception.facade;

import com.yxsk.relay.job.component.endpoint.exception.RelayJobEndpointException;

/**
 * @Author 11376
 * @CreaTime 2019/7/18 17:22
 * @Description
 */
public class UnsupportedCollectException extends RelayJobEndpointException {
    public UnsupportedCollectException(String cause) {
        super(cause);
    }

    public UnsupportedCollectException(Throwable e) {
        super(e);
    }

    public UnsupportedCollectException(String cause, Throwable e) {
        super(cause, e);
    }
}

package com.yxsk.relay.job.component.endpoint.handle.context;

import com.yxsk.relay.job.component.endpoint.handle.JobExecutor;

import java.lang.annotation.*;

/**
 * @Description
 * @Author 11376
 * @CreateTime 2019/9/9 19:30
 */
@Documented
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface RelayJobHandlerScan {

    String[] basePackages() default {};

    Class<JobExecutor>[] classes() default {};

}

package com.yxsk.relay.job.component.endpoint.log.stream;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.List;

/**
 * @Author 11376
 * @CreaTime 2019/6/29 21:27
 * @Description
 */
public interface LogStream {

    Charset LOG_CHARSET = StandardCharsets.UTF_8;

    /**
     *
     * @Author 11376
     * @Description 记录日志
     * @CreateTime 2019/6/29 21:28
     * @param logId
     * @param message
     * @Return
     */
    void append(String logId, String message);

    /**
     *
     * @Author 11376
     * @Description 读取日志
     * @CreateTime 2019/6/29 21:28
     * @param logId
     * @Return
     */
    List<String> read(String logId);

    /**
     *
     * @Author 11376
     * @Description 读取日志
     * @CreateTime 2019/6/29 21:30
     * @param logId
     * @param startLine
     * @param endLine
     * @Return
     */
    List<String> read(String logId, long startLine, long endLine);

    /**
     *
     * @Author 11376
     * @Description 读取日志
     * @CreateTime 2019/6/29 21:30
     * @param logId
     * @param startLine
     * @Return
     */
    List<String> read(String logId, long startLine);

    /**
     *
     * @Author 11376
     * @Description 获取日志总行数
     * @CreateTime 2019/6/29 21:33
     * @param logId
     * @Return
     */
    long getTotalRow(String logId);

    /**
     * 
     * @Author 11376
     * @Description 删除日志
     * @CreateTime 2019/6/29 21:30
     * @param logId
     * @Return
     */
    void delete(String logId);

}

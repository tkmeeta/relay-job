package com.yxsk.relay.job.component.endpoint.exception.log;

import com.yxsk.relay.job.component.endpoint.exception.RelayJobEndpointException;

/**
 * @Description
 * @Author 11376
 * @CreateTime 2019/9/9 18:48
 */
public class RelayJobLogException extends RelayJobEndpointException {
    public RelayJobLogException(String cause) {
        super(cause);
    }

    public RelayJobLogException(Throwable e) {
        super(e);
    }

    public RelayJobLogException(String cause, Throwable e) {
        super(cause, e);
    }
}

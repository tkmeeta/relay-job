package com.yxsk.relay.job.component.endpoint.facade.anno;

import java.lang.annotation.*;

/**
 * @Description
 * @Author 11376
 * @CreateTime 2019/9/6 16:48
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE, ElementType.METHOD})
public @interface FacadeUriMapping {

    String value();

}

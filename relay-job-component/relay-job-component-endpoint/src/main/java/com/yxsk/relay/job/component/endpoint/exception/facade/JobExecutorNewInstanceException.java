package com.yxsk.relay.job.component.endpoint.exception.facade;

import com.yxsk.relay.job.component.endpoint.exception.RelayJobEndpointException;

/**
 * @Author 11376
 * @CreaTime 2019/6/15 18:22
 * @Description
 */
public class JobExecutorNewInstanceException extends RelayJobEndpointException {
    public JobExecutorNewInstanceException(String cause) {
        super(cause);
    }

    public JobExecutorNewInstanceException(Throwable e) {
        super(e);
    }

    public JobExecutorNewInstanceException(String cause, Throwable e) {
        super(cause, e);
    }
}

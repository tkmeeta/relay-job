package com.yxsk.relay.job.component.endpoint.exception.facade;

import com.yxsk.relay.job.component.endpoint.exception.RelayJobEndpointException;

/**
 * @Author 11376
 * @CreaTime 2019/7/18 17:22
 * @Description
 */
public class UnsupportedPartitionException extends RelayJobEndpointException {
    public UnsupportedPartitionException(String cause) {
        super(cause);
    }

    public UnsupportedPartitionException(Throwable e) {
        super(e);
    }

    public UnsupportedPartitionException(String cause, Throwable e) {
        super(cause, e);
    }
}

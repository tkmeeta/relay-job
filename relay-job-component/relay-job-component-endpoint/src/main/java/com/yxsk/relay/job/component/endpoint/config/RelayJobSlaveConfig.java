package com.yxsk.relay.job.component.endpoint.config;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.File;

/**
 * @Author 11376
 * @CreaTime 2019/6/15 18:25
 * @Description
 */
@Getter
@Setter
@ToString
public class RelayJobSlaveConfig {

    // 默认日志根目录
    public static final String DEFAULT_LOG_ROOT_PATH = System.getProperty("user.dir") + File.separator + "relayJobLogs";

    // 默认日志格式
    public static final String DEFAULT_LOG_PATTERN = "%date{yyyy-MM-dd HH:mm:ss} | %-5level | %thread | %logger{36}.%L - %msg%n";

    // 主机IP, 不填写则自动配置
    private String host = "";

    // 服务端口, 默认值则自动配置
    private Integer port = -1;

    // 授权 token
    private String authToken;

    // 应用名称
    private String appName;

    // 日志根目录
    private String logRootPath;

    // 日志格式, logback日志格式
    private String logPattern;
}

package com.yxsk.relay.job.component.endpoint.handle;

import com.yxsk.relay.job.component.common.exception.RelayJobException;

/**
 * @Author 11376
 * @CreaTime 2019/6/15 17:49
 * @Description
 */
public interface JobExecutor {

    String execute(String param) throws RelayJobException;

}

package com.yxsk.relay.job.component.endpoint.exception;

import com.yxsk.relay.job.component.common.exception.RelayJobException;
import com.yxsk.relay.job.component.common.exception.RelayJobRuntimeException;

/**
 * @Description
 * @Author 11376
 * @CreateTime 2019/9/8 21:58
 */
public class RelayJobEndpointException extends RelayJobRuntimeException {
    public RelayJobEndpointException(String cause) {
        super(cause);
    }

    public RelayJobEndpointException(Throwable e) {
        super(e);
    }

    public RelayJobEndpointException(String cause, Throwable e) {
        super(cause, e);
    }
}

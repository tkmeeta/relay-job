package com.yxsk.relay.job.component.endpoint.handle.context;

import com.yxsk.relay.job.component.common.utils.Assert;
import com.yxsk.relay.job.component.endpoint.handle.JobExecutor;

/**
 * @Description
 * @Author 11376
 * @CreateTime 2019/9/10 9:56
 */
public class RelayJobHandlerAnnotationDriver {

    public void drive(RelayJobHandlerScan annotation) {
        Assert.notNull(annotation, "Annotation not be null.");
        String[] basePackages = annotation.basePackages();
        Class<JobExecutor>[] classes = annotation.classes();

        if (basePackages != null && basePackages.length > 0) {
            RelayJobExecutorManager.load(basePackages);
        }

        if (classes != null && classes.length > 0) {
            RelayJobExecutorManager.load(classes);
        }

    }

}
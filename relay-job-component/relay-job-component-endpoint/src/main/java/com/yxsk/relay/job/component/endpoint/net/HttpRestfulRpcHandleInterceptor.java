package com.yxsk.relay.job.component.endpoint.net;

import com.yxsk.relay.job.component.common.exception.net.NetHandleInterceptException;
import com.yxsk.relay.job.component.common.net.handler.interceptor.RpcHandleInterceptor;
import com.yxsk.relay.job.component.common.net.message.NetRequestMessage;
import com.yxsk.relay.job.component.common.net.message.NetResponseMessage;
import com.yxsk.relay.job.component.common.utils.CollectionUtils;

import java.util.Map;
import java.util.TreeMap;

/**
 * @Description
 * @Author 11376
 * @CreateTime 2019/9/9 11:17
 */
public class HttpRestfulRpcHandleInterceptor implements RpcHandleInterceptor {

    @Override
    public void before(NetRequestMessage message) throws NetHandleInterceptException {
        // Nothing.
    }

    @Override
    public void after(NetResponseMessage message) throws NetHandleInterceptException {
        Map<String, String> headers = message.getHeaders();
        if (CollectionUtils.isEmpty(headers)) {
            headers = new TreeMap<>();
        }
        headers.put("Content-Type", "application/json");
        message.setHeaders(headers);
    }

}

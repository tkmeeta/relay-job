package com.yxsk.relay.job.component.endpoint.handle.anno;

import java.lang.annotation.*;

/**
 * @Description
 * @Author 11376
 * @CreateTime 2019/9/9 10:40
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@Documented
public @interface JobHandler {

    // handler name
    String value();

    // 对象生命周期模型
    Scope scope() default Scope.Prototype;

    enum Scope {
        Singleton,
        Prototype
    }

}

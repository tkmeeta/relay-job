package com.yxsk.relay.job.component.endpoint.config;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

/**
 * @Author 11376
 * @CreaTime 2019/6/15 18:25
 * @Description
 */
@Getter
@Setter
@ToString
public class RelayJobAdminConfig {

    private Boolean enable = true;

    private List<AddressInfo> addresses;

    @Getter
    @Setter
    @ToString
    public static class AddressInfo {

        private String host;

        private String port;

        private String token;

    }

}

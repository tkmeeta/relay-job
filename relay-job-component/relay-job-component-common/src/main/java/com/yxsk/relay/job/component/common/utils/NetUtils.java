package com.yxsk.relay.job.component.common.utils;

import lombok.experimental.UtilityClass;

import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 * @Author 11376
 * @CreaTime 2019/6/15 20:34
 * @Description
 */
@UtilityClass
public class NetUtils {

    public String getLocalIp() throws UnknownHostException {
        return InetAddress.getLocalHost().getHostAddress();
    }

}

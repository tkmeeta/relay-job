package com.yxsk.relay.job.component.common.constant;

/**
 * @Author 11376
 * @CreaTime 2019/6/8 23:44
 * @Description
 */
public final class UriConstant {

    /*********************************服务注册协议************************************/
    public static final String RENEWAL_URI = "/relay/job/renewal";

    public static final String TERMINATE_URI = "/relay/job/terminate";

    /*********************************任务执行协议************************************/
    public static final String TASK_EXECUTE_URI = "/relay/job/execute";

    // 异步执行 URI
    public static final String TASK_ASYNC_EXECUTE_URI = "/relay/job/async/execute";
    // 异步执行结果查询 URI
    public static final String TASK_ASYNC_EXECUTE_RESULT_QUERY_URI = "/relay/job/async/result/query";
    // 异步执行结果回调 URI, slave端调用
    public static final String TASK_ASYNC_EXECUTE_RESULT_CALLBACK_URI = "/relay/job/async/result/callback";

    /********************************* 任务分片、结果收集协议 ************************************/
    // 任务分片 URI
    public static final String TASK_PARTITION_URI = "/relay/job/execute/partition";
    // 任务结果收集 URI
    public static final String TASK_RESULT_COLLECT_URI = "/relay/job/result/collect";

    /**********************************日志协议********************************/
    public static final String LOG_QUERY = "/relay/log/query";

    public static final String LOG_DELETE = "/relay/log/delete";

    /**********************************  端口性能监控  ********************************/
    public static final String RESOURCE_USAGE = "/relay/job/performance/resource/usage";


    // 版本号，暂定
    public static final String VERSION_NO = "1.0.0";

    // 授权头信息 key
    public static final String AUTHORIZATION_HEADER_KEY = "Authorization";

}

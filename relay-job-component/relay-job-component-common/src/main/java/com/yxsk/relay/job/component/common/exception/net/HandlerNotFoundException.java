package com.yxsk.relay.job.component.common.exception.net;

import lombok.Getter;

import java.text.MessageFormat;

/**
 * @Description
 * @Author 11376
 * @CreateTime 2019/9/8 21:48
 */
@Getter
public class HandlerNotFoundException extends RelayJobNetException {

    private String uri;

    public HandlerNotFoundException(String uri) {
        super(MessageFormat.format("Request uri [{0}] not found message handler", uri));
        this.uri = uri;
    }

}

package com.yxsk.relay.job.component.common.utils;

import lombok.experimental.UtilityClass;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.util.Calendar;
import java.util.Date;

/**
 * @Author 11376
 * @CreaTime 2019/7/1 9:16
 * @Description
 */
@UtilityClass
public class DateUtils {

    /**
     * @param
     * @Author 11376
     * @Description 获取当前时间
     * @CreateTime 2019/7/1 9:17
     * @Return
     */
    public Date getCurrentDate() {
        return new Date(System.currentTimeMillis());
    }

    /**
     * @param
     * @Author 11376
     * @Description 获取当前整型时间
     * @CreateTime 2019/7/1 9:31
     * @Return
     */
    public long getCurrentTimeMillis() {
        return System.currentTimeMillis();
    }

    /**
     * @param date
     * @param pattern
     * @Author 11376
     * @Description 格式化长整型时间为字符串
     * @CreateTime 2019/7/1 9:34
     * @Return
     */
    public String formatDate(long date, String pattern) {
        Assert.hasLength(pattern, "The pattern string cannot be empty");
        SimpleDateFormat dateFormat = new SimpleDateFormat(pattern);
        return dateFormat.format(new Date(date));
    }

    /**
     * @param date
     * @param pattern
     * @Author 11376
     * @Description 格式化长整型时间为字符串
     * @CreateTime 2019/7/1 9:34
     * @Return
     */
    public String formatDate(Date date, String pattern) {
        Assert.notNull(date, "The date cannot be null");
        Assert.hasLength(pattern, "The pattern string cannot be empty");
        SimpleDateFormat dateFormat = new SimpleDateFormat(pattern);
        return dateFormat.format(date);
    }

    /**
     * @param date
     * @param pattern
     * @Author 11376
     * @Description 格式化时间
     * @CreateTime 2019/7/1 9:19
     * @Return
     */
    public Date parseDate(String date, String pattern) throws ParseException {
        Assert.hasLength(date, "The date string cannot be empty");
        Assert.hasLength(pattern, "The pattern string cannot be empty");
        SimpleDateFormat dateFormat = new SimpleDateFormat(pattern);
        return dateFormat.parse(date);
    }

    /**
     * @param date1
     * @param date2
     * @Author 11376
     * @Description 时间比较
     * @CreateTime 2019/7/1 9:24
     * @Return date1 > date2, return 1, date1 = date2 return 0 , otherwise return -1
     */
    public int compare(Date date1, Date date2) {
        Assert.notNull(date1, "The date one cannot be null");
        Assert.notNull(date1, "The date two cannot be null");
        return date1.compareTo(date2);
    }

    public static Date addHours(Date defaultDate, Integer laterHours) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(defaultDate);
        calendar.add(Calendar.HOUR, laterHours);
        return calendar.getTime();
    }

    public static Date subtractHours(Date defaultDate, Integer laterHours) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(defaultDate);
        calendar.add(Calendar.HOUR, 0 - laterHours);
        return calendar.getTime();
    }

    /**
     * @param startDate
     * @param endDate
     * @Author 11376
     * @Description 计算时间差, 单位：秒
     * @CreateTime 2019/7/3 11:51
     * @Return
     */
    public static long diffSeconds(Date startDate, Date endDate) {
        return Duration.between(startDate.toInstant(), endDate.toInstant()).getSeconds();
    }

    public static Date addSeconds(Date startDte, Long seconds) {
        Calendar instance = Calendar.getInstance();
        instance.setTime(startDte);
        instance.add(Calendar.SECOND, Integer.parseInt(seconds.toString()));
        return instance.getTime();
    }
}

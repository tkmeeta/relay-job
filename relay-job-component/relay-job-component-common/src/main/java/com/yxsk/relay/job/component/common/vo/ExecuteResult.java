package com.yxsk.relay.job.component.common.vo;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

/**
 * @Author 11376
 * @CreaTime 2019/6/5 14:07
 * @Description
 */
@Getter
@Setter
@ToString
public abstract class ExecuteResult implements Serializable {

    protected String code;

    protected String message;

    /**
     * @param result
     * @Author 11376
     * @Description 判断执行结果是否成功
     * @CreateTime 2019/6/5 20:38
     * @Return
     */
    public static <T extends ExecuteResult> boolean isOk(T result) {
        if (result == null) {
            throw new IllegalArgumentException("result not be null");
        }
        return ExecuteResultCode.OK.code.equals(result.code);
    }


    @AllArgsConstructor
    @Getter
    public enum ExecuteResultCode {
        OK("0000", "成功"),
        EXECUTOR_NOT_FOUND("1001", "未找到执行器"),
        EXECUTING("1002", "执行中"),
        EXECUTE_ERROR("1003", "任务执行错误"),

        JOB_NOT_FOUND("2003", "任务未找到"),
        PARTIAL_SUCCESS("2004", "部分成功"),
        EXECUTE_TIMEOUT("2005", "执行超时"),
        MANUAL_CANCEL("2006", "人工取消"),

        JOB_PARTITIONING_ERROR("3001", "任务分片错误"),

        UNSUPPORTED_COLLECT_ERROR("4001", "不支持任务结果收集处理"),
        JOB_COLLECT_ERROR("4002", "任务结果收集处理错误"),

        ERROR("9999", "任务执行异常");

        private String code;
        private String message;

    }

}

package com.yxsk.relay.job.component.common.exception.auth;

import com.yxsk.relay.job.component.common.exception.RelayJobException;

/**
 * @Author 11376
 * @CreaTime 2019/6/15 10:41
 * @Description
 */
public class TokenException extends RelayJobAuthException {

    public TokenException(String cause) {
        super(cause);
    }

    public TokenException(Throwable e) {
        super(e);
    }

    public TokenException(String cause, Throwable e) {
        super(cause, e);
    }

}

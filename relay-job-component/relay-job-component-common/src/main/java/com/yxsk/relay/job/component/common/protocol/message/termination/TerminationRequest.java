package com.yxsk.relay.job.component.common.protocol.message.termination;

import com.yxsk.relay.job.component.common.protocol.message.BaseRequest;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.NotEmpty;

/**
 * @Author 11376
 * @CreaTime 2019/6/8 22:34
 * @Description 下线请求
 */
@Getter
@Setter
@ToString
public class TerminationRequest extends BaseRequest {

    // 主机 ip:port
    @NotEmpty(message = "主机地址不能为空")
    private String host;
    @NotEmpty(message = "appName不能为空")
    private String appName;
    // 下线原因
    private String reason;

}

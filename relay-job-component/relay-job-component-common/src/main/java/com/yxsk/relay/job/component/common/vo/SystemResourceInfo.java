package com.yxsk.relay.job.component.common.vo;

import lombok.*;

/**
 * @Author 11376
 * @CreaTime 2019/7/2 15:50
 * @Description
 */
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
public class SystemResourceInfo implements Cloneable{

    private String osName;

    private String osArch;

    private String osVersion;

    private Integer coreNum;

    private Double availableMemory;

    private Double freeMemory;

    private Double totalMemory;

    public SystemResourceInfo clone() {
        return SystemResourceInfo.builder()
                .osName(this.osName)
                .osArch(this.osArch)
                .osVersion(this.osVersion)
                .coreNum(this.coreNum)
                .availableMemory(this.availableMemory)
                .freeMemory(this.freeMemory)
                .totalMemory(this.totalMemory)
                .build();
    }

}

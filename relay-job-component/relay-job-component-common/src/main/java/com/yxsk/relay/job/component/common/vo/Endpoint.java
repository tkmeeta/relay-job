package com.yxsk.relay.job.component.common.vo;

import com.yxsk.relay.job.component.common.constant.NetProtocol;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * @Author 11376
 * @CreaTime 2019/7/17 11:22
 * @Description
 */
@Getter
@Setter
@ToString
public class Endpoint implements Cloneable {

    // 主机
    private String host;
    // 端口
    private Integer port;
    // token
    private String authToken;
    // 工作单元网络协议
    private NetProtocol protocol;

    public Endpoint clone() {
        Endpoint endpoint = new Endpoint();
        endpoint.setHost(this.host);
        endpoint.setPort(this.port);
        endpoint.setAuthToken(this.authToken);
        endpoint.setProtocol(this.protocol);
        return endpoint;
    }

    public boolean equalsInstance(Endpoint endpoint) {
        return endpoint != null && this.host.equals(endpoint.getHost()) && this.host.equals(endpoint.getPort());
    }

}

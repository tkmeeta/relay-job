package com.yxsk.relay.job.component.common.protocol.message.execute.async;

import com.yxsk.relay.job.component.common.protocol.message.BaseRequest;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * @Author 11376
 * @CreaTime 2019/6/17 21:19
 * @Description 任务执行回调请求
 */
@Getter
@Setter
@ToString
public class JobExecuteCallbackRequest extends BaseRequest {

    /**
     * 任务执行的 Id
     */
    private String jobId;
    // 执行主机
    private String host;
    /**
     * 执行结果 {@link com.yxsk.relay.job.component.common.vo.ExecuteResult.ExecuteResultCode}
     */
    private String resultCode;
    /**
     * 执行结果
     */
    private String result;

    private String errorMsg;

}

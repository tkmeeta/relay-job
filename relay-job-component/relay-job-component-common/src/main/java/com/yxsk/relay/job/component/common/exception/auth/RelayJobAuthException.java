package com.yxsk.relay.job.component.common.exception.auth;

import com.yxsk.relay.job.component.common.exception.RelayJobException;

/**
 * @Description
 * @Author 11376
 * @CreateTime 2019/9/8 21:46
 */
public class RelayJobAuthException extends RelayJobException {
    public RelayJobAuthException(String cause) {
        super(cause);
    }

    public RelayJobAuthException(Throwable e) {
        super(e);
    }

    public RelayJobAuthException(String cause, Throwable e) {
        super(cause, e);
    }
}

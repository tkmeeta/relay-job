package com.yxsk.relay.job.component.common.net.handler.convert;

import com.yxsk.relay.job.component.common.exception.net.RelayJobNetException;
import com.yxsk.relay.job.component.common.net.handler.convert.NetMessageConvert;
import com.yxsk.relay.job.component.common.utils.CompressUtils;

import java.io.IOException;

/**
 * @Description
 * @Author 11376
 * @CreateTime 2019/9/9 10:57
 */
public class CompressAbleMessageConvert implements NetMessageConvert {

    @Override
    public byte[] readConvert(byte[] request) throws RelayJobNetException {
        if (request != null) {
            try {
                return CompressUtils.uncompress(request, CompressUtils.CompressType.GZIP);
            } catch (IOException e) {
                throw new RelayJobNetException(e);
            }
        }
        return null;
    }

    @Override
    public byte[] writeConvert(byte[] response) throws RelayJobNetException {
        if (response != null) {
            try {
                return CompressUtils.compress(response, CompressUtils.CompressType.GZIP);
            } catch (IOException e) {
                throw new RelayJobNetException(e);
            }
        }
        return null;
    }
}

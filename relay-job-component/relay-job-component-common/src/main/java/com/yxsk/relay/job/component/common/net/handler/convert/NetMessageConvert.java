package com.yxsk.relay.job.component.common.net.handler.convert;

import com.yxsk.relay.job.component.common.exception.net.RelayJobNetException;

import java.io.IOException;

/**
 * @Description 网络报文消息转换器
 * @Author 11376
 * @CreateTime 2019/9/6 17:13
 */
public interface NetMessageConvert {

    byte[] readConvert(byte[] request) throws RelayJobNetException;

    byte[] writeConvert(byte[] response) throws RelayJobNetException;

}

package com.yxsk.relay.job.component.common.vo;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * @Author 11376
 * @CreaTime 2019/7/17 11:23
 * @Description
 */
@Getter
@Setter
@ToString
public class PartitionInfo {

    // 执行端点
    private Endpoint endpoint;

    // 端点执行参数
    private String executeParam;

}

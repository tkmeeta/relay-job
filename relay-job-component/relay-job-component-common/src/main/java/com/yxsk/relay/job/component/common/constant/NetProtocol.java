package com.yxsk.relay.job.component.common.constant;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.apache.commons.lang3.StringUtils;

/**
 * @Author 11376
 * @CreaTime 2019/6/8 22:38
 * @Description
 */
@Getter
@AllArgsConstructor
public enum NetProtocol {

    HTTP("HTTP");

    private String protocol;

    public static NetProtocol getProtocol(String protocol) {
        if (StringUtils.isEmpty(protocol)) {
            throw new IllegalArgumentException("网络协议不能为空");
        }
        NetProtocol[] protocols = NetProtocol.values();
        for (NetProtocol netProtocol : protocols) {
            if (netProtocol.protocol.equals(protocol)) {
                return netProtocol;
            }
        }
        throw new IllegalArgumentException("未找到相应的网络协议[" + protocol + "]");
    }

}

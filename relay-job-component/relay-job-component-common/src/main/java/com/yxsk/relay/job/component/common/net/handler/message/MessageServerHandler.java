package com.yxsk.relay.job.component.common.net.handler.message;


import com.yxsk.relay.job.component.common.net.message.NetRequestMessage;

/**
 * @Description 报文服务处理器
 * @Author 11376
 * @CreateTime 2019/9/6 17:54
 */
public interface MessageServerHandler {

    byte[] handle(NetRequestMessage requestMessage) throws Exception;

}

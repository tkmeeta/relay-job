package com.yxsk.relay.job.component.common.protocol.message.execute.async.query;

import com.yxsk.relay.job.component.common.protocol.message.BaseResponse;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * @Author 11376
 * @CreaTime 2019/6/19 14:25
 * @Description 异步任务执行查询响应
 */
@Getter
@Setter
@ToString(callSuper = true)
public class AsyncJobQueryResponse extends BaseResponse {

    /**
     * 执行状态 {@link com.yxsk.relay.job.component.common.vo.ExecuteResult.ExecuteResultCode}
     */
    private String resultCode;

    private String result;

    private String errorMsg;

}

package com.yxsk.relay.job.component.common.exception.net;

import com.yxsk.relay.job.component.common.exception.RelayJobException;

/**
 * @Description
 * @Author 11376
 * @CreateTime 2019/9/6 17:29
 */
public class NetHandleInterceptException extends RelayJobNetException {
    public NetHandleInterceptException(String cause) {
        super(cause);
    }

    public NetHandleInterceptException(Throwable e) {
        super(e);
    }

    public NetHandleInterceptException(String cause, Throwable e) {
        super(cause, e);
    }
}
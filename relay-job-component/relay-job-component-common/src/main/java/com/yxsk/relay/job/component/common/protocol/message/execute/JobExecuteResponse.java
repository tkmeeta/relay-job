package com.yxsk.relay.job.component.common.protocol.message.execute;

import com.yxsk.relay.job.component.common.protocol.message.BaseResponse;
import lombok.*;

/**
 * @Author 11376
 * @CreaTime 2019/6/15 16:20
 * @Description
 */
@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class JobExecuteResponse extends BaseResponse {
    /**
     * 执行状态
     */
    private String executeStatus;
    // 执行结果
    private String result;
    // 执行异常信息
    private String errorMsg;

}

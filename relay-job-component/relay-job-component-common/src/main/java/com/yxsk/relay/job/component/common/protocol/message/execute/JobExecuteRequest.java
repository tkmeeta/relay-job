package com.yxsk.relay.job.component.common.protocol.message.execute;

import com.yxsk.relay.job.component.common.protocol.message.BaseRequest;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.NotEmpty;

/**
 * @Author 11376
 * @CreaTime 2019/6/15 16:20
 * @Description 任务执行请求
 */
@Getter
@Setter
@ToString
public class JobExecuteRequest extends BaseRequest {

    @NotEmpty(message = "任务编号不能为空")
    private String jobId;
    @NotEmpty(message = "处理器名称不能为空")
    private String handleName;
    // 执行参数
    private String param;

    // 重试标识
    private Boolean retry;

}

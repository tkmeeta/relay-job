package com.yxsk.relay.job.component.common.thread.observe;

import com.yxsk.relay.job.component.common.thread.Task;

/**
 * @Author 11376
 * @CreaTime 2019/6/2 16:39
 * @Description
 */
public interface Observable<T> {

    // 任务生命周期的枚举类型
    enum Cycle {
        NEW, STARTED, RUNNING, FINISH, ERROR
    }

    // 当前任务生命周期状态
    Cycle getCycle();

    Task<T> getTask();
}

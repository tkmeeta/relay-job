package com.yxsk.relay.job.component.common.net.handler.interceptor;

import com.yxsk.relay.job.component.common.exception.net.NetHandleInterceptException;
import com.yxsk.relay.job.component.common.net.message.NetRequestMessage;
import com.yxsk.relay.job.component.common.net.message.NetResponseMessage;
import org.jboss.netty.channel.MessageEvent;

/**
 * @Description
 * @Author 11376
 * @CreateTime 2019/9/6 17:27
 */
public interface RpcHandleInterceptor {

    void before(NetRequestMessage message) throws NetHandleInterceptException;

    void after(NetResponseMessage message) throws NetHandleInterceptException;

    default void doLast(MessageEvent event) {};

}

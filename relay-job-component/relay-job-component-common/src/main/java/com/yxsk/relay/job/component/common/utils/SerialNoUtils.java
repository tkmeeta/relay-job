package com.yxsk.relay.job.component.common.utils;

import lombok.experimental.UtilityClass;

import java.util.UUID;

/**
 * @Author 11376
 * @CreaTime 2019/6/15 21:02
 * @Description
 */
@UtilityClass
public class SerialNoUtils {

    public String nextId() {
        return UUID.randomUUID().toString().replace("-", "");
    }

}

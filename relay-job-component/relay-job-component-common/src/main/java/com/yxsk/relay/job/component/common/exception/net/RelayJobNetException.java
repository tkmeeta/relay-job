package com.yxsk.relay.job.component.common.exception.net;

import com.yxsk.relay.job.component.common.exception.RelayJobException;

/**
 * @Description
 * @Author 11376
 * @CreateTime 2019/9/8 21:44
 */
public class RelayJobNetException extends RelayJobException {
    public RelayJobNetException(String cause) {
        super(cause);
    }

    public RelayJobNetException(Throwable e) {
        super(e);
    }

    public RelayJobNetException(String cause, Throwable e) {
        super(cause, e);
    }
}
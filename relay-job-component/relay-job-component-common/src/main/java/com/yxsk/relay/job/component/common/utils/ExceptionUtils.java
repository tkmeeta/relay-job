package com.yxsk.relay.job.component.common.utils;

import lombok.experimental.UtilityClass;

import java.io.PrintWriter;
import java.io.StringWriter;

/**
 * @Author 11376
 * @CreaTime 2019/6/5 20:43
 * @Description
 */
@UtilityClass
public class ExceptionUtils {

    public String getStackInfo(Throwable e) {
        Assert.notNull(e, "throwable not be null");
        StringWriter writer = new StringWriter();
        e.printStackTrace(new PrintWriter(writer));
        return writer.toString();
    }

}

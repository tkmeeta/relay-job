package com.yxsk.relay.job.component.common.net;

/**
 * @Description
 * @Author 11376
 * @CreateTime 2019/9/6 15:50
 */
public interface NetServer {

    void start();

    void stop();

    void pause() throws UnsupportedOperationException;

}

package com.yxsk.relay.job.component.common.protocol.message.execute.async;

import com.yxsk.relay.job.component.common.protocol.message.BaseResponse;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * @Author 11376
 * @CreaTime 2019/6/17 21:19
 * @Description 任务执行回调响应
 */
@Getter
@Setter
@ToString
public class JobExecuteCallbackResponse extends BaseResponse {

}

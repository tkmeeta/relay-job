package com.yxsk.relay.job.component.common.thread;

/**
 * @Author 11376
 * @CreaTime 2019/6/15 19:45
 * @Description
 */
public abstract class WakeupAbleTask<T> implements Task<T>, WakeupAble {

    protected Thread workThread;

    private volatile boolean interrupted = false;

    @Override
    public final T execute() {
        this.workThread = Thread.currentThread();
        return doWork();
    }

    @Override
    public void wakeup() {
        if (this.workThread == null) {
            throw new IllegalStateException("Thread not started.");
        }
        this.workThread.interrupt();

        this.interrupted = true;
    }

    protected abstract T doWork();

    public boolean isInterrupted() {
        if (this.workThread == null) {
            throw new IllegalStateException("Task not started.");
        }
        return this.interrupted;
    }

}

package com.yxsk.relay.job.component.common.exception.remote;

import com.yxsk.relay.job.component.common.exception.RelayJobException;

/**
 * @Author 11376
 * @CreaTime 2019/6/17 10:03
 * @Description
 */
public class RemoteCallException extends RelayJobException {
    public RemoteCallException(String cause) {
        super(cause);
    }

    public RemoteCallException(Throwable e) {
        super(e);
    }

    public RemoteCallException(String cause, Throwable e) {
        super(cause, e);
    }
}

package com.yxsk.relay.job.component.common.protocol.message.renewal;

import com.yxsk.relay.job.component.common.protocol.message.BaseRequest;
import com.yxsk.relay.job.component.common.vo.SystemResourceInfo;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

/**
 * @Author 11376
 * @CreaTime 2019/6/8 22:34
 * @Description 注册请求
 */
@Getter
@Setter
@ToString
public class RenewalRequest extends BaseRequest {

    // 主机 ip:port
    @NotEmpty(message = "主机信息不能为空")
    private String host;
    // 服务端口
    @NotNull(message = "服务端口不能为空")
    private Integer port;
    // 应用名称
    @NotEmpty(message = "应用名称不能为空")
    private String appName;
    // 业务通讯协议
    private String netProtocol;
    // 访问slave 的 token
    private String authToken;
    // 系统资源信息
    private SystemResourceInfo resourceInfo;

}

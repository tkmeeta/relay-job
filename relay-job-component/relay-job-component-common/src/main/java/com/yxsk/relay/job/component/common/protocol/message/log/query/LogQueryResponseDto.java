package com.yxsk.relay.job.component.common.protocol.message.log.query;

import com.yxsk.relay.job.component.common.protocol.message.BaseResponse;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

/**
 * @Author 11376
 * @CreaTime 2019/6/30 13:47
 * @Description
 */
@Getter
@Setter
@ToString(callSuper = true)
public class LogQueryResponseDto extends BaseResponse {

    private Long totalLine;

    private List<String> logs;

}

package com.yxsk.relay.job.component.common.exception.serialization;

import com.yxsk.relay.job.component.common.exception.RelayJobRuntimeException;

/**
 * @Description
 * @Author 11376
 * @CreateTime 2019/9/8 21:59
 */
public class MessageSerializeException extends RelayJobRuntimeException {
    public MessageSerializeException(String cause) {
        super(cause);
    }

    public MessageSerializeException(Throwable e) {
        super(e);
    }

    public MessageSerializeException(String cause, Throwable e) {
        super(cause, e);
    }
}

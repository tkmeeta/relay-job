package com.yxsk.relay.job.component.common.protocol.message.log.delete;

import com.yxsk.relay.job.component.common.protocol.message.BaseRequest;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.NotEmpty;

/**
 * @Author 11376
 * @CreaTime 2019/6/30 13:47
 * @Description
 */
@Getter
@Setter
@ToString(callSuper = true)
public class LogDeleteRequestDto extends BaseRequest {

    @NotEmpty(message = "删除日志任务Id不能为空")
    private String logId;

}

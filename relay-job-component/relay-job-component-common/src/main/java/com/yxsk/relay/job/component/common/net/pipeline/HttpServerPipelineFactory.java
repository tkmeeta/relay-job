package com.yxsk.relay.job.component.common.net.pipeline;

import com.yxsk.relay.job.component.common.net.handler.HttpServerHandler;
import lombok.AllArgsConstructor;
import org.jboss.netty.channel.ChannelPipeline;
import org.jboss.netty.channel.ChannelPipelineFactory;
import org.jboss.netty.channel.Channels;
import org.jboss.netty.handler.codec.http.HttpRequestDecoder;
import org.jboss.netty.handler.codec.http.HttpResponseEncoder;

/**
 * @Description
 * @Author 11376
 * @CreateTime 2019/9/6 15:25
 */
@AllArgsConstructor
public class HttpServerPipelineFactory implements ChannelPipelineFactory {

    private HttpServerHandler serverHandler;

    @Override
    public ChannelPipeline getPipeline() throws Exception {
        ChannelPipeline pipeline = Channels.pipeline();
        // 解码
        pipeline.addLast("decoder", new HttpRequestDecoder());
        // 编码
        pipeline.addLast("encoder", new HttpResponseEncoder());
        // HTTP请求处理器
        pipeline.addLast("handler", serverHandler);
        return pipeline;
    }

}

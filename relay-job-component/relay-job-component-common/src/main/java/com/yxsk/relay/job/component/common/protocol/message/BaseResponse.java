package com.yxsk.relay.job.component.common.protocol.message;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.util.Date;

/**
 * @Author 11376
 * @CreaTime 2019/6/8 22:22
 * @Description
 */
@Getter
@Setter
@ToString
public class BaseResponse implements Serializable {

    // 请求流水号
    private String requestSerialNo;
    // 响应时间
    private Date respTime;

}

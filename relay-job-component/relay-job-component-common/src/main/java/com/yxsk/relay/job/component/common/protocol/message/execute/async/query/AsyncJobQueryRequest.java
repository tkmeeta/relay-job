package com.yxsk.relay.job.component.common.protocol.message.execute.async.query;

import com.yxsk.relay.job.component.common.protocol.message.BaseRequest;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * @Author 11376
 * @CreaTime 2019/6/19 14:22
 * @Description 异步执行结果查询
 */
@Getter
@Setter
@ToString(callSuper = true)
public class AsyncJobQueryRequest extends BaseRequest {

    // 任务 Id
    private String jobId;

}

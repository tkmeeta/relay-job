package com.yxsk.relay.job.component.common.exception.net;

import com.yxsk.relay.job.component.common.exception.RelayJobException;
import lombok.Getter;

/**
 * @Description
 * @Author 11376
 * @CreateTime 2019/9/8 21:35
 */
@Getter
public class RelayJobHttpNetException extends RelayJobNetException {

    private int httpResponseCode;

    public RelayJobHttpNetException(int code, String cause) {
        this(code, cause, null);
    }

    public RelayJobHttpNetException(int code, Throwable e) {
        this(code, null, e);
    }

    public RelayJobHttpNetException(int code, String cause, Throwable e) {
        super(cause, e);
        this.httpResponseCode = code;
    }

}

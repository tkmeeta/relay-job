package com.yxsk.relay.job.component.common.utils;

import lombok.experimental.UtilityClass;
import org.apache.commons.lang3.StringUtils;

import java.util.Collection;
import java.util.Map;

/**
 * @Description
 * @Author 11376
 * @CreateTime 2019/9/8 20:49
 */
@UtilityClass
public class Assert {

    public void hasLength(String string, String cause) {
        if (StringUtils.isEmpty(string)) {
            throwCause(cause);
        }
    }

    public void notNull(Object o, String cause) {
        if (o == null) {
            throwCause(cause);
        }
    }

    public void notEmpty(Collection collection, String cause) {
        if (collection == null || collection.isEmpty()) {
            throwCause(cause);
        }
    }

    public void notEmpty(Map map, String cause) {
        if (map == null || map.isEmpty()) {
            throwCause(cause);
        }
    }

    public void state(boolean state, String cause) {
        if (!state) {
            throwCause(cause);
        }
    }

    private void throwCause(String cause) {
        throw new IllegalArgumentException(cause == null ? "" : cause);
    }

}

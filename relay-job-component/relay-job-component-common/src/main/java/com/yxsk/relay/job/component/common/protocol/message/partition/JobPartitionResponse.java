package com.yxsk.relay.job.component.common.protocol.message.partition;

import com.yxsk.relay.job.component.common.protocol.message.BaseResponse;
import com.yxsk.relay.job.component.common.vo.PartitionInfo;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

/**
 * @Author 11376
 * @CreaTime 2019/7/18 17:01
 * @Description
 */
@Getter
@Setter
@ToString
public class JobPartitionResponse extends BaseResponse {

    private List<PartitionInfo> partitionInfoList;

}

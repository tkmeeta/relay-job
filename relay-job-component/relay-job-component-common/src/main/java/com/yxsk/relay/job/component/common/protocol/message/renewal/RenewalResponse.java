package com.yxsk.relay.job.component.common.protocol.message.renewal;

import com.yxsk.relay.job.component.common.protocol.message.BaseResponse;
import lombok.*;

import java.util.Date;

/**
 * @Author 11376
 * @CreaTime 2019/6/8 22:34
 * @Description
 */
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class RenewalResponse extends BaseResponse {

    private Date expirationDate;

}

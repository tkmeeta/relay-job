package com.yxsk.relay.job.component.common.protocol.caller;

import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;

/**
 * @Description
 * @Author 11376
 * @CreateTime 2019/9/11 9:29
 */
public class DefaultHttpClientFactory implements HttpClientFactory {

    private static final int MAX_CONNECTION = 10;

    private static final int DEFAULT_MAX_CONNECTION = 5;

    @Override
    public HttpClient getHttpClient() {
        PoolingHttpClientConnectionManager connectionManager = new PoolingHttpClientConnectionManager();
        connectionManager.setMaxTotal(MAX_CONNECTION);//客户端总并行链接最大数
        connectionManager.setDefaultMaxPerRoute(DEFAULT_MAX_CONNECTION);//每个主机的最大并行链接数

        HttpClientBuilder httpBuilder = HttpClients.custom();
        httpBuilder.setConnectionManager(connectionManager);

        return httpBuilder.build();
    }


}

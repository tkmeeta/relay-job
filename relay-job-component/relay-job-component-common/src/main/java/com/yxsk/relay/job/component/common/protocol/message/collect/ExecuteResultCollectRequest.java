package com.yxsk.relay.job.component.common.protocol.message.collect;

import com.yxsk.relay.job.component.common.protocol.message.BaseRequest;
import com.yxsk.relay.job.component.common.vo.EndpointExecuteResult;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @Author 11376
 * @CreaTime 2019/7/18 17:01
 * @Description
 */
@Getter
@Setter
@ToString
public class ExecuteResultCollectRequest extends BaseRequest {

    @NotEmpty(message = "任务名称不能为空")
    private String handleName;

    @NotNull(message = "执行结果集不能为空")
    private List<EndpointExecuteResult> executeResults;

}

package com.yxsk.relay.job.component.common.net.message;

import lombok.*;

import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.util.Map;

/**
 * @Description
 * @Author 11376
 * @CreateTime 2019/9/6 17:03
 */
@Getter
@Setter
@ToString
@AllArgsConstructor
@Builder
public class NetRequestMessage {

    private String uri;

    private InetSocketAddress socketAddress;

    private Map<String, String> headers;

    private byte[] message;

}

package com.yxsk.relay.job.component.common.protocol.message.base;

import lombok.*;

/**
 * @Author 11376
 * @CreaTime 2019/6/2 21:06
 * @Description
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Response{
    protected String code;
    protected String message;
}

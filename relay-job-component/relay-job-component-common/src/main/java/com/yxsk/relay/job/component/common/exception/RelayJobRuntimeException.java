package com.yxsk.relay.job.component.common.exception;

/**
 * @Description
 * @Author 11376
 * @CreateTime 2019/9/8 20:32
 */
public class RelayJobRuntimeException extends RuntimeException {

    public RelayJobRuntimeException(String cause) {
        super(cause);
    }

    public RelayJobRuntimeException(Throwable e) {
        super(e);
    }

    public RelayJobRuntimeException(String cause, Throwable e) {
        super(cause, e);
    }

}

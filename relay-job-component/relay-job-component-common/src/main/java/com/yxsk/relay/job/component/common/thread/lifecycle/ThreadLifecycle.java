package com.yxsk.relay.job.component.common.thread.lifecycle;

/**
 * @Author 11376
 * @CreaTime 2019/6/2 16:27
 * @Description
 */
public interface ThreadLifecycle<T> {

    void onNew();

    void onStart(Thread thread);

    void onRunning(Thread thread);

    void onFinish(Thread thread, T taskResult);

    void onError(Thread thread, Exception e);

    class DefaultThreadLifecycle<T> implements ThreadLifecycle<T> {

        @Override
        public void onNew() {

        }

        @Override
        public void onStart(Thread thread) {

        }

        @Override
        public void onRunning(Thread thread) {

        }

        @Override
        public void onFinish(Thread thread, T taskResult) {

        }

        @Override
        public void onError(Thread thread, Exception e) {

        }
    }
}

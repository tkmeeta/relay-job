package com.yxsk.relay.job.component.common.net.handler.exception;

import com.yxsk.relay.job.component.common.net.message.NetResponseMessage;
import com.yxsk.relay.job.component.common.utils.ExceptionUtils;
import org.jboss.netty.channel.ExceptionEvent;
import org.jboss.netty.channel.MessageEvent;
import org.jboss.netty.handler.codec.http.HttpResponseStatus;

import java.nio.charset.StandardCharsets;

/**
 * @Description 全局网络服务异常处理器
 * @Author 11376
 * @CreateTime 2019/9/6 17:58
 */
public interface GlobalNetServerExceptionHandler {

    NetResponseMessage handle(ExceptionEvent e);

    class DefaultGlobalExceptionHandler implements GlobalNetServerExceptionHandler {

        @Override
        public NetResponseMessage handle(ExceptionEvent e) {
            return NetResponseMessage.builder()
                    .status(HttpResponseStatus.BAD_REQUEST.getCode())
                    .message(ExceptionUtils.getStackInfo(e.getCause()).getBytes(StandardCharsets.UTF_8))
                    .build();
        }

    }

}

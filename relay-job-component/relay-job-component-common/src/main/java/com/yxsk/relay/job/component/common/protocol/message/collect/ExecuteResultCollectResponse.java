package com.yxsk.relay.job.component.common.protocol.message.collect;

import com.yxsk.relay.job.component.common.protocol.message.BaseResponse;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * @Author 11376
 * @CreaTime 2019/7/18 17:01
 * @Description
 */
@Getter
@Setter
@ToString
public class ExecuteResultCollectResponse extends BaseResponse {

    private String result;

}

package com.yxsk.relay.job.component.common.protocol.message.termination;

import com.yxsk.relay.job.component.common.protocol.message.BaseResponse;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * @Author 11376
 * @CreaTime 2019/6/8 22:34
 * @Description
 */
@Getter
@Setter
@ToString
public class TerminationResponse extends BaseResponse {
}

package com.yxsk.relay.job.component.common.protocol.message;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

/**
 * @Author 11376
 * @CreaTime 2019/6/8 22:22
 * @Description
 */
@Getter
@Setter
@ToString
public class BaseRequest implements Serializable {

    // 流水号
    @NotEmpty(message = "请求流水号不能为空")
    private String serialNo;
    // 版本号
    @NotEmpty(message = "版本号不能为空")
    private String version;
    // 请求时间
    @NotNull(message = "请求时间不能为空")
    private Date requestTime;

}

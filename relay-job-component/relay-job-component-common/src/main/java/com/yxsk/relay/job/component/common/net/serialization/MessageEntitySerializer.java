package com.yxsk.relay.job.component.common.net.serialization;

import com.yxsk.relay.job.component.common.exception.serialization.MessageSerializeException;

/**
 * @Description
 * @Author 11376
 * @CreateTime 2019/9/8 21:53
 */
public interface MessageEntitySerializer {

    <T> T deserialize(byte[] message, Class<T> type) throws MessageSerializeException;

    byte[] serialize(Object o) throws MessageSerializeException;

}

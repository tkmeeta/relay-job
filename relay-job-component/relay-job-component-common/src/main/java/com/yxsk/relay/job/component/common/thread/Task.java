package com.yxsk.relay.job.component.common.thread;

/**
 * @Author 11376
 * @CreaTime 2019/6/2 16:44
 * @Description
 */
public interface Task<T> {

    T execute();

}

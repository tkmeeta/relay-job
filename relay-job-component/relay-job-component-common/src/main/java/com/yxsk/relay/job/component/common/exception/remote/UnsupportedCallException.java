package com.yxsk.relay.job.component.common.exception.remote;

/**
 * @Author 11376
 * @CreaTime 2019/6/17 9:56
 * @Description
 */
public class UnsupportedCallException extends RemoteCallException {
    public UnsupportedCallException(String cause) {
        super(cause);
    }

    public UnsupportedCallException(Throwable e) {
        super(e);
    }

    public UnsupportedCallException(String cause, Throwable e) {
        super(cause, e);
    }
}

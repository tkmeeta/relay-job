package com.yxsk.relay.job.component.common.utils;

import lombok.experimental.UtilityClass;

import java.util.Collection;
import java.util.Map;

/**
 * @Description
 * @Author 11376
 * @CreateTime 2019/9/8 21:07
 */
@UtilityClass
public class CollectionUtils {

    public boolean isEmpty(Collection collection) {
        return collection == null ? true : collection.isEmpty();
    }

    public boolean isNotEmpty(Collection collection) {
        return !isEmpty(collection);
    }

    public boolean isEmpty(Map map) {
        return map == null ? true : map.isEmpty();
    }

    public boolean isNotEmpty(Map map) {
        return !isEmpty(map);
    }

}

package com.yxsk.relay.job.component.common.vo;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

/**
 * @Author 11376
 * @CreaTime 2019/6/22 18:39
 * @Description
 */
@Getter
@Setter
@ToString
public class TriggerResult implements Serializable {

    protected String code;

    protected String errorMsg;

    /**
     * @param result
     * @Author 11376
     * @Description 判断执行结果是否成功
     * @CreateTime 2019/6/5 20:38
     * @Return
     */
    public static <T extends TriggerResult> boolean isOk(T result) {
        if (result == null) {
            throw new IllegalArgumentException("result not be null");
        }
        return TriggerResult.TriggerResultCode.OK.code.equals(result.code);
    }


    @AllArgsConstructor
    @Getter
    public enum TriggerResultCode {
        TRIGGERING("T-0001", "任务触发中"),
        OK("T-0000", "触发成功"),
        INTERCEPTED("T-1000", "触发请求被拦截"),
        RESULT_COLLECT_ERROR("T-2000", "任务结果处理异常"),
        ERROR("T-9999", "任务触发异常");

        private String code;
        private String message;

    }


}

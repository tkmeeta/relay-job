package com.yxsk.relay.job.component.common.protocol.caller;

import org.apache.http.client.HttpClient;

/**
 * @Description
 * @Author 11376
 * @CreateTime 2019/9/11 9:27
 */
public interface HttpClientFactory {

    HttpClient getHttpClient();
}

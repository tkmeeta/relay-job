package com.yxsk.relay.job.component.common.protocol.message.partition;

import com.yxsk.relay.job.component.common.protocol.message.BaseRequest;
import com.yxsk.relay.job.component.common.vo.Endpoint;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @Author 11376
 * @CreaTime 2019/7/18 17:01
 * @Description
 */
@Getter
@Setter
@ToString
public class JobPartitionRequest extends BaseRequest {

    @NotEmpty(message = "任务名称不能为空")
    private String handleName;

    @NotNull(message = "端点列表不能为空")
    private List<Endpoint> endpoints;

    private String executeParam;

}

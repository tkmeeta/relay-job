package com.yxsk.relay.job.component.common.thread;

/**
 * @Author 11376
 * @CreaTime 2019/6/15 11:09
 * @Description
 */
public interface WakeupAble {

    void wakeup();

}

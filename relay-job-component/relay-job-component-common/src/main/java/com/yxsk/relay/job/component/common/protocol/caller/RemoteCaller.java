package com.yxsk.relay.job.component.common.protocol.caller;

import com.alibaba.fastjson.TypeReference;
import com.yxsk.relay.job.component.common.exception.remote.RemoteCallException;
import com.yxsk.relay.job.component.common.exception.remote.UnsupportedCallException;
import com.yxsk.relay.job.component.common.protocol.message.BaseRequest;

import java.util.Map;

/**
 * @Author 11376
 * @CreaTime 2019/6/15 16:35
 * @Description 远程任务执行器
 */
public interface RemoteCaller {

    <T> T call(BaseRequest request, String url, Class<T> responseType) throws RemoteCallException;

    <T> T call(BaseRequest request, Map<String, String> header, String url, Class<T> responseType) throws RemoteCallException;

    default <T> T call(BaseRequest request, String url, TypeReference<T> responseType) throws RemoteCallException {
        throw new UnsupportedCallException("Unsupported type reference call. class: " + this.getClass().getName());
    }

    default <T> T call(BaseRequest request, Map<String, String> header, String url, TypeReference<T> responseType) throws RemoteCallException {
        throw new UnsupportedCallException("Unsupported type reference call. class: " + this.getClass().getName());
    }

    String buildUri(String host, Integer port, String uri);

}

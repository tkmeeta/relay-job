package com.yxsk.relay.job.component.common.vo;

import lombok.*;

import java.io.Serializable;

/**
 * @Author 11376
 * @CreaTime 2019/6/8 23:55
 * @Description
 */
@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class SlaveInfo implements Serializable, Cloneable {

    private String appName;

    private Endpoint endpoint;

    private SystemResourceInfo resourceInfo;

    @Override
    public SlaveInfo clone() {
        SlaveInfo info = new SlaveInfo(appName, endpoint.clone(), resourceInfo.clone());
        return info;
    }
}

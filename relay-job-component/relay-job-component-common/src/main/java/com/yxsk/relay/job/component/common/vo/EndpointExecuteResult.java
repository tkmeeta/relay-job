package com.yxsk.relay.job.component.common.vo;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * @Author 11376
 * @CreaTime 2019/7/17 11:42
 * @Description
 */
@Getter
@Setter
@ToString
public class EndpointExecuteResult {

    // 端点
    private Endpoint endpoint;

    // 执行成功
    private Boolean success;

    // 节点执行参数
    private String executeParam;

    // 错误信息
    private String errorMsg;

    // 执行结果
    private String result;

}

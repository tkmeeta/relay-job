package com.yxsk.relay.job.component.common.protocol.message.base;

/**
 *
 * @Author 11376
 * @Description 系统返回码常量类
 * @CreateTime 2019/6/2 21:10
 */
public class ResponseCodes {
    public final static Response SUCCESS = new Response("0000", "成功");
    /**
     * 内部错误
     */
    public final static Response INTERNAL_ERROR = new Response("0001", "系统繁忙，请稍后再试");
    /**
     * 参数校验
     */
    public final static Response PARAMETER_ERROR = new Response("0002", "参数错误或缺失");
    /**
     * 框架服务熔断
     */
    public final static Response SERVICE_INVOKE_ERROR = new Response("0003", "服务调用失败");
    /**
     * 方法重复调用异常
     */
    public final static Response MULTIPLE_INVOCATION = new Response("0004", "重复调用，服务处理中");
    /**
     * Token不存在
     */
    public final static Response TOKEN_NOT_EXIST = new Response("0005", "Token不存在");
    /**
     * Token已失效
     */
    public final static Response TOKEN_EXPIRED = new Response("0006", "Token已失效");
    /**
     * token 异常
     */
    public final static Response TOKEN_EXCEPTION = new Response("0007", "Token异常");
    /**
     * 用户权限不足
     */
    public final static Response PERMISSION_DENIED = new Response("0008", "用户权限不足");
    /**
     * 非法请求
     */
    public final static Response ILLEGAL_REQUEST = new Response("0009", "非法请求");

}

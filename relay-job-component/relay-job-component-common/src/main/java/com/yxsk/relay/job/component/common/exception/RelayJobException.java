package com.yxsk.relay.job.component.common.exception;

/**
 * @Description
 * @Author 11376
 * @CreateTime 2019/9/8 20:32
 */
public class RelayJobException extends Exception {

    public RelayJobException(String cause) {
        super(cause);
    }

    public RelayJobException(Throwable e) {
        super(e);
    }

    public RelayJobException(String cause, Throwable e) {
        super(cause, e);
    }


}
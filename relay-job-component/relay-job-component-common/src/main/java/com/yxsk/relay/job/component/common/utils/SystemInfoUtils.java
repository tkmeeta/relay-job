package com.yxsk.relay.job.component.common.utils;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.experimental.UtilityClass;

import java.io.IOException;
import java.lang.management.ManagementFactory;
import java.lang.management.OperatingSystemMXBean;
import java.math.BigDecimal;

/**
 * @Author 11376
 * @CreaTime 2019/7/2 15:10
 * @Description
 */
@UtilityClass
public class SystemInfoUtils {

    /**
     * @param
     * @Author 11376
     * @Description 获取JVM可用核心数
     * @CreateTime 2019/7/2 15:30
     * @Return
     */
    public int getJvmAvailableCores() {
        return Runtime.getRuntime().availableProcessors();
    }

    /**
     * @param radix
     * @Author 11376
     * @Description 获取JVM可用最大内存
     * @CreateTime 2019/7/2 15:31
     * @Return
     */
    public double getJvmAvailableMemory(StoredRadix radix) {
        return calculate(Runtime.getRuntime().maxMemory(), radix);
    }

    /**
     * @param radix
     * @Author 11376
     * @Description 获取JVM空闲内存
     * @CreateTime 2019/7/2 15:31
     * @Return
     */
    public double getJvmFreeMemory(StoredRadix radix) {
        return calculate(Runtime.getRuntime().freeMemory(), radix);
    }

    /**
     * @param radix
     * @Author 11376
     * @Description 获取JVM总共内存
     * @CreateTime 2019/7/2 15:31
     * @Return
     */
    public double getJvmTotalMemory(StoredRadix radix) {
        return calculate(Runtime.getRuntime().totalMemory(), radix);
    }

    private double calculate(long memory, StoredRadix radix) {
        return new BigDecimal(memory).divide(new BigDecimal(radix.conversion), 1, BigDecimal.ROUND_HALF_UP).doubleValue();
    }

    /**
     * @param
     * @Author 11376
     * @Description 获取操作系统名称
     * @CreateTime 2019/7/4 11:03
     * @Return
     */
    public String getOsName() {
        return System.getProperty("os.name");
    }

    /**
     * @param
     * @Author 11376
     * @Description 获取操作系统架构
     * @CreateTime 2019/7/4 11:03
     * @Return
     */
    public String getOsArch() {
        return System.getProperty("os.arch");
    }

    /**
     * @param
     * @Author 11376
     * @Description 获取操作系统版本
     * @CreateTime 2019/7/4 11:03
     * @Return
     */
    public String getOsVersion() {
        return System.getProperty("os.version");
    }

    /**
     * @param
     * @Author 11376
     * @Description 获取jvm的线程总数
     * @CreateTime 2019/7/4 11:20
     * @Return
     */
    public Integer getThreadCount() {
        ThreadGroup parentThread;
        for (parentThread = Thread.currentThread().getThreadGroup(); parentThread.getParent() != null; parentThread = parentThread.getParent())
            ;
        return parentThread.activeCount();
    }

    public Double getCpuUsageRate() throws IOException {
        String osName = getOsName().toLowerCase();
        return null;
    }

    @Getter
    @AllArgsConstructor
    public enum StoredRadix {
        B(1),
        KB(1024),
        MB(1024 * 1024),
        GB(1024 * 1024 * 1024),
        TB(1024 * 1024 * 1024 * 1024);

        private long conversion;
    }

    public static void main(String[] args) {
        OperatingSystemMXBean osmxb = (OperatingSystemMXBean)ManagementFactory.getOperatingSystemMXBean();
        while (true) {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println();
        }

    }

}